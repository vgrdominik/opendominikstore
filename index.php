<?php
/**
 * Created by Mega Gestió.
 * User: vgrdominik
 * Date: 16/02/13
 * Time: 16:15
 */
function __autoload($className)
{
    /*$path = explode('/', $_SERVER['PHP_SELF']);
    array_pop($path);
    $path = join('/', $path);*/
    $classNameArray = explode('_', strrev($className), 2);
    $classNameArray[0] = strrev($classNameArray[0]);
    if(!empty($classNameArray[1]))
    {
        $classNameArray[1] = strrev($classNameArray[1]);
    }
    if(!empty($classNameArray[1]))
    {
        $fileName = str_replace('_', '/', $classNameArray[1]).'/'.$classNameArray[0];
    }else{
        $fileName = $classNameArray[0];
    }
    $extension = '';
    if(is_file('src/lib/' . $fileName . '.class.php'))
    {
        $extension = 'class';
    }elseif(is_file('src/lib/' . $fileName . '.interface.php'))
    {
        $extension = 'interface';
    }

    if(!empty($extension))
    {
        $path = 'src/lib/' . $fileName . '.' . $extension . '.php';
        require_once $path;
    }else{
        $path = 'src/lib/' . $fileName . '.php';
        if(is_file($path))
        {
            require_once $path;
        }
    }
}

PHPConsole_PhpConsole::start();

Twig_Autoloader::register();

$loader = new Twig_Loader_Filesystem('./');
$twig = new Twig_Environment($loader, array(
    //'debug' => true,
    //'cache' => './cache',
));
//$twig->addExtension(new Twig_Extension_Debug());
$escaper = new Twig_Extension_Escaper('html');
$twig->addExtension($escaper);

$twig->clearCacheFiles();
$twig->clearTemplateCache();

$twig->addExtension(new TemplateExtension_Project());
//$twig->addFunction('debug', new Twig_Function_Function('debug'));

if(empty($_GET['action']))
{
    $_GET['action'] = 'home';
}
if(empty($_GET['language']))
{
    $_GET['language'] = 'es';
}
$baseModule = explode('/', $_GET['action']);
$baseModule = $baseModule[0];
if($baseModule != 'tienda'&&$baseModule != 'administrador')
{
    $baseModule = 'tienda';
}

try{
    include 'src/module/general/parameters.php';
    include 'src/module/'.$baseModule.'/general/parameters.php';

    // var_dump('module/'.$parameters->action->path.'/view/'.$parameters->language.'/'.$parameters->action->nameFile.'.html.twig');
    $twig->display( 'src/module/'.$parameters->action->path.'/view/'.$parameters->language.'/'.$parameters->action->nameFile.'.html.twig', array(
        'parameters' => $parameters
    ));
}catch (Exception $e)
{

    //print_r($_GET);
    //echo $e->getMessage();
    //exit();

    if( strpos($e->getMessage(), 'Unable to find template') !== false )
    {
        if($parameters->action !== 'not-found')
        {
            header('Location: '.$parameters->path.'/'.$parameters->language.'/'.$baseModule.'/not-found.html/2');
        }else{
            echo $e->getMessage();
        }
    }else{
        echo $e->getMessage();
    }
}