# DROP DATABASE openDominikStore;
CREATE DATABASE openDominikStore
        DEFAULT CHARACTER SET utf8
        DEFAULT COLLATE utf8_general_ci;

USE openDominikStore;

# DROP FUNCTION urls_amigables_func;
# DROP FUNCTION quitar_quiones_bajos_finles;
DELIMITER //
CREATE FUNCTION quitar_quiones_bajos_finles (titol VARCHAR(100)) RETURNS VARCHAR(100)
BEGIN
    DECLARE posicion_final INTEGER(1);
    WHILE SUBSTRING(titol, -1, 1) = '_' DO
        SET titol = CONCAT(titol, '"');
        SET posicion_final = LOCATE('"', titol);
        SET titol = SUBSTRING(titol, 1, posicion_final - 2);
    END WHILE;
    RETURN titol;
END//
CREATE FUNCTION urls_amigables_func (titol VARCHAR(100)) RETURNS VARCHAR(100)
BEGIN
    DECLARE url_amigable VARCHAR(100);
    SET url_amigable = REPLACE (titol,'á','a');
    SET url_amigable = REPLACE (url_amigable,'"','_');
    SET url_amigable = REPLACE (url_amigable,'é','e');
    SET url_amigable = REPLACE (url_amigable,'í','i');
    SET url_amigable = REPLACE (url_amigable,'ó','o');
    SET url_amigable = REPLACE (url_amigable,'ú','u');
    SET url_amigable = REPLACE (url_amigable,'à','a');
    SET url_amigable = REPLACE (url_amigable,'è','e');
    SET url_amigable = REPLACE (url_amigable,'ì','i');
    SET url_amigable = REPLACE (url_amigable,'ò','o');
    SET url_amigable = REPLACE (url_amigable,'ù','u');
    SET url_amigable = REPLACE (url_amigable,'Á','a');
    SET url_amigable = REPLACE (url_amigable,'É','e');
    SET url_amigable = REPLACE (url_amigable,'Í','i');
    SET url_amigable = REPLACE (url_amigable,'Ó','o');
    SET url_amigable = REPLACE (url_amigable,'Ú','u');
    SET url_amigable = REPLACE (url_amigable,'À','a');
    SET url_amigable = REPLACE (url_amigable,'È','e');
    SET url_amigable = REPLACE (url_amigable,'Ì','i');
    SET url_amigable = REPLACE (url_amigable,'Ò','o');
    SET url_amigable = REPLACE (url_amigable,'Ù','u');
    SET url_amigable = REPLACE (url_amigable,'º','_');
    SET url_amigable = REPLACE (url_amigable,'ª','_');
    SET url_amigable = REPLACE (url_amigable,'|','_');
    SET url_amigable = REPLACE (url_amigable,'!','_');
    SET url_amigable = REPLACE (url_amigable,'@','_');
    SET url_amigable = REPLACE (url_amigable,'·','_');
    SET url_amigable = REPLACE (url_amigable,'#','_');
    SET url_amigable = REPLACE (url_amigable,'$','_');
    SET url_amigable = REPLACE (url_amigable,'%','_');
    SET url_amigable = REPLACE (url_amigable,'&','_');
    SET url_amigable = REPLACE (url_amigable,'¬','_');
    SET url_amigable = REPLACE (url_amigable,'/','_');
    SET url_amigable = REPLACE (url_amigable,'(','_');
    SET url_amigable = REPLACE (url_amigable,')','_');
    SET url_amigable = REPLACE (url_amigable,'=','_');
    SET url_amigable = REPLACE (url_amigable,'?','_');
    SET url_amigable = REPLACE (url_amigable,'\'','_');
    SET url_amigable = REPLACE (url_amigable,'\\','_');
    SET url_amigable = REPLACE (url_amigable,'¡','_');
    SET url_amigable = REPLACE (url_amigable,'`','_');
    SET url_amigable = REPLACE (url_amigable,'^','_');
    SET url_amigable = REPLACE (url_amigable,'+','_');
    SET url_amigable = REPLACE (url_amigable,'*','_');
    SET url_amigable = REPLACE (url_amigable,']','_');
    SET url_amigable = REPLACE (url_amigable,'¨','_');
    SET url_amigable = REPLACE (url_amigable,'´','_');
    SET url_amigable = REPLACE (url_amigable,'{','_');
    SET url_amigable = REPLACE (url_amigable,'ç','_');
    SET url_amigable = REPLACE (url_amigable,'}','_');
    SET url_amigable = REPLACE (url_amigable,'.','_');
    SET url_amigable = REPLACE (url_amigable,',','_');
    SET url_amigable = REPLACE (url_amigable,':','_');
    SET url_amigable = REPLACE (url_amigable,';','_');
    SET url_amigable = REPLACE (url_amigable,' ','_');
    SET url_amigable = LOWER(url_amigable);
    SET url_amigable = quitar_quiones_bajos_finles(url_amigable);
    RETURN url_amigable;
END//
DELIMITER ;

# DROP TABLE Pagina;
CREATE TABLE IF NOT EXISTS Pagina(
    id_pagina INTEGER(10) AUTO_INCREMENT PRIMARY KEY,
    titol VARCHAR(200) NOT NULL UNIQUE DEFAULT "Sín título" COMMENT 'SI;Título de la página;text',
    descripcio TEXT NOT NULL COMMENT 'SI;Descripción de la página;text',
    descripcio_amigable VARCHAR(200) NOT NULL UNIQUE COMMENT 'SI;Descripción amigable;text',
    url VARCHAR(200) NOT NULL,
	ultima_modificacio TIMESTAMP NOT NULL COMMENT 'SI;Última modificación;timestamp'
) ENGINE InnoDB;

# DROP TABLE PaginaKeywords;
CREATE TABLE IF NOT EXISTS PaginaKeywords(
    num_pagina_keywords INTEGER(11) auto_increment ,
    id_pagina INTEGER(10),
    descripcio VARCHAR(25) NOT NULL COMMENT 'SI;Metakeyword de la página;text',
    PRIMARY KEY (num_pagina_keywords, id_pagina),
    FOREIGN KEY (id_pagina) REFERENCES Pagina(id_pagina) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE InnoDB;

# DROP TRIGGER upd_pagina_ins;
DELIMITER //
CREATE TRIGGER upd_pagina_ins BEFORE INSERT ON Pagina FOR EACH ROW
BEGIN
	DECLARE existente VARCHAR(200);
    IF new.descripcio_amigable = '' THEN
        SET new.descripcio_amigable = urls_amigables_func(new.titol);
    END IF;
	SET existente = (SELECT descripcio_amigable FROM Pagina WHERE descripcio_amigable = new.descripcio_amigable LIMIT 1);
	WHILE existente <> '' DO
		SET new.descripcio_amigable = CONCAT(new.descripcio_amigable, '1');
		SET existente = (SELECT descripcio_amigable FROM Pagina WHERE descripcio_amigable = new.descripcio_amigable LIMIT 1);
	END WHILE;
END//
# DROP TRIGGER ins_pagina_ins//
CREATE TRIGGER ins_pagina_ins AFTER INSERT ON Pagina FOR EACH ROW
BEGIN
    DECLARE keyword VARCHAR(100);
    DECLARE num_keyword INTEGER(3);
    SET num_keyword = 1;
    WHILE SUBSTRING_INDEX(new.titol, ' ', num_keyword - 1) <> new.titol DO
        SET keyword = SUBSTRING_INDEX(new.titol, ' ', -num_keyword);
        IF num_keyword <> 1 THEN
            SET keyword = SUBSTRING_INDEX(keyword, ' ', 1);
        END IF;
        INSERT INTO PaginaKeywords VALUES (num_keyword, new.id_pagina, keyword);
        SET num_keyword = num_keyword + 1;
    END WHILE;
END//
# DROP TRIGGER upd_pagina_upd//
CREATE TRIGGER upd_pagina_upd BEFORE UPDATE ON Pagina FOR EACH ROW
BEGIN
	DECLARE existente VARCHAR(200);
	SET existente = (SELECT descripcio_amigable FROM Pagina WHERE descripcio_amigable = new.descripcio_amigable LIMIT 1, 1);
	WHILE existente <> '' DO
		SET new.descripcio_amigable = CONCAT(new.descripcio_amigable, '1');
		SET existente = (SELECT descripcio_amigable FROM Pagina WHERE descripcio_amigable = new.descripcio_amigable LIMIT 1, 1);
	END WHILE;
END//
# DROP TRIGGER ins_pagina_upd//
CREATE TRIGGER ins_pagina_upd AFTER UPDATE ON Pagina FOR EACH ROW
BEGIN
    DECLARE keyword VARCHAR(100);
    DECLARE num_keyword INTEGER(3);
    DELETE FROM PaginaKeywords WHERE id_pagina = new.id_pagina;
    SET num_keyword = 1;
    WHILE SUBSTRING_INDEX(new.titol, ' ', num_keyword - 1) <> new.titol DO
        SET keyword = SUBSTRING_INDEX(new.titol, ' ', -num_keyword);
        IF num_keyword <> 1 THEN
            SET keyword = SUBSTRING_INDEX(keyword, ' ', 1);
        END IF;
        INSERT INTO PaginaKeywords VALUES (num_keyword, new.id_pagina, keyword);
        SET num_keyword = num_keyword + 1;
    END WHILE;
END//
DELIMITER ;

# INSERT INTO Pagina VALUES(NULL, 'Título de pruebas', 'Descripción título de pruebas', 'hola', '', NOW());
# UPDATE Pagina SET titol = 'Títulos de pruebas antes de eso', descripcio_amigable = '' WHERE id_pagina = 14;

# DROP TABLE Contingut;
CREATE TABLE IF NOT EXISTS Contingut(
    id_contingut INTEGER(10) AUTO_INCREMENT PRIMARY KEY,
	id_pagina INTEGER(10) NOT NULL COMMENT 'NO',
	titol VARCHAR(150) UNIQUE COMMENT 'SI;Título;text',
	descripcio TEXT NOT NULL COMMENT 'SI;Descripción;textarea',
	imatge VARCHAR(150) NOT NULL COMMENT 'SI;Imagen;text',
	alta DATETIME NOT NULL COMMENT 'SI;Fecha de alta;text',
    FOREIGN KEY (id_pagina) REFERENCES Pagina(id_pagina) ON UPDATE CASCADE ON DELETE NO ACTION
) ENGINE InnoDB;

# DROP TRIGGER ins_contingut_ins;
DELIMITER //
CREATE TRIGGER ins_contingut_ins BEFORE INSERT ON Contingut FOR EACH ROW
BEGIN
    DECLARE nuevo_id_urls_amigables INTEGER(10);
    DECLARE descripcio_final VARCHAR(200) DEFAULT new.titol;
    INSERT INTO Pagina VALUES(NULL, descripcio_final, descripcio_final, '', '', NOW());
	SET new.id_pagina = LAST_INSERT_ID();
END//
# DROP TRIGGER ins_contingut_upd//
CREATE TRIGGER ins_contingut_upd AFTER UPDATE ON Contingut FOR EACH ROW
BEGIN
    DECLARE descripcio_final VARCHAR(200) DEFAULT new.titol;
    UPDATE Pagina SET titol = descripcio_final, descripcio = descripcio_final, ultima_modificacio = NOW() WHERE id_pagina = new.id_pagina;
END//
DELIMITER ;

# DROP TRIGGER upd_contingut_del;

DELIMITER //
CREATE TRIGGER upd_contingut_del AFTER DELETE ON Contingut FOR EACH ROW
BEGIN
	DELETE FROM Pagina WHERE id_pagina = old.id_pagina;
END//
DELIMITER ;

# INSERT INTO Contingut VALUES(NULL, 0, 'Contingut 1', 'Descripció del contingut 1', 'http://dev.megagestio.com/test.jpg', NOW());
# UPDATE Contingut SET titol = 'Contingut test 1' WHERE titol = 'Contingut 1';
# DELETE FROM Contingut WHERE titol = 'Contingut test 1';

# DROP TABLE Promocio;
CREATE TABLE IF NOT EXISTS Promocio(
    id_promocio INTEGER(10) AUTO_INCREMENT PRIMARY KEY,
	descripcio VARCHAR(200) NOT NULL UNIQUE COMMENT 'SI;Descripción;text',
  id_tipus INTEGER(10) NOT NULL COMMENT 'SI;Tipo;text',
	tipus VARCHAR(25) NOT NULL COMMENT 'SI;Tipo;text'
) ENGINE InnoDB;

# DROP TABLE Promocio;
CREATE TABLE IF NOT EXISTS Promocio_Pagina(
    id_promocio INTEGER(10) NOT NULL COMMENT 'NO',
	id_pagina INTEGER(10) NOT NULL COMMENT 'NO',
	PRIMARY KEY (id_promocio, id_pagina),
    FOREIGN KEY (id_pagina) REFERENCES Pagina(id_pagina) ON UPDATE CASCADE ON DELETE NO ACTION,
	FOREIGN KEY (id_promocio) REFERENCES Promocio(id_promocio) ON UPDATE CASCADE ON DELETE NO ACTION
) ENGINE InnoDB;

# DROP TABLE IndexPaginaPrincipal;
CREATE TABLE IF NOT EXISTS IndexPaginaPrincipal(
    id_indexpaginaprincipal INTEGER(10) AUTO_INCREMENT PRIMARY KEY,
	id_pagina INTEGER(10) NOT NULL COMMENT 'NO',
	descripcio VARCHAR(200) NOT NULL UNIQUE COMMENT 'SI;Descripción;text',
    FOREIGN KEY (id_pagina) REFERENCES Pagina(id_pagina) ON UPDATE CASCADE ON DELETE NO ACTION
) ENGINE InnoDB;

CREATE TABLE IF NOT EXISTS IndexPaginaPrincipal_Tipus(
  id_indexpaginaprincipal INTEGER(10) NOT NULL,
  id_tipus INTEGER(10) NOT NULL,
  tipus VARCHAR(50) NOT NULL,
  width INTEGER(4) NOT NULL COMMENT 'SI;Width;text',
  height INTEGER(4) NOT NULL COMMENT 'SI;Height;text',
  ordre VARCHAR(25) NOT NULL COMMENT 'SI;Orden;text',
  params TEXT,
  PRIMARY KEY (id_indexpaginaprincipal, id_tipus, tipus),
  FOREIGN KEY (id_indexpaginaprincipal) REFERENCES IndexPaginaPrincipal(id_indexpaginaprincipal) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE InnoDB;

# INSERT INTO IndexPaginaPrincipal VALUES(NULL, 0, 'Descripció del index 1', 100, 500, 'ordre 1');
# UPDATE IndexPaginaPrincipal SET descripcio = 'Index test 1' WHERE descripcio = 'Descripció del index 1';
# DELETE FROM IndexPaginaPrincipal WHERE descripcio = 'Index test 1';

# DROP TABLE Imatge;
CREATE TABLE IF NOT EXISTS Imatge(
    id_imatge INTEGER(10) AUTO_INCREMENT PRIMARY KEY,
	id_indexpaginaprincipal INTEGER(10) COMMENT 'NO',
	id_promocio INTEGER(10) COMMENT 'NO',
	url VARCHAR(200) NOT NULL COMMENT 'SI;Descripción;text',
    FOREIGN KEY (id_indexpaginaprincipal) REFERENCES IndexPaginaPrincipal(id_indexpaginaprincipal) ON UPDATE CASCADE ON DELETE NO ACTION,
    FOREIGN KEY (id_promocio) REFERENCES Promocio(id_promocio) ON UPDATE CASCADE ON DELETE NO ACTION
) ENGINE InnoDB;

# DROP TABLE TextBanner;
CREATE TABLE IF NOT EXISTS TextBanner(
    id_textbanner INTEGER(10) AUTO_INCREMENT PRIMARY KEY,
	id_indexpaginaprincipal INTEGER(10) COMMENT 'NO',
	id_promocio INTEGER(10) COMMENT 'NO',
	textBanner TEXT NOT NULL COMMENT 'SI;Text;textarea',
    FOREIGN KEY (id_indexpaginaprincipal) REFERENCES IndexPaginaPrincipal(id_indexpaginaprincipal) ON UPDATE CASCADE ON DELETE NO ACTION,
    FOREIGN KEY (id_promocio) REFERENCES Promocio(id_promocio) ON UPDATE CASCADE ON DELETE NO ACTION
) ENGINE InnoDB;

# DROP TABLE Slide;
CREATE TABLE IF NOT EXISTS Slide(
    id_slide INTEGER(10) AUTO_INCREMENT PRIMARY KEY,
	id_indexpaginaprincipal INTEGER(10) COMMENT 'NO',
	id_promocio INTEGER(10) COMMENT 'NO',
    FOREIGN KEY (id_indexpaginaprincipal) REFERENCES IndexPaginaPrincipal(id_indexpaginaprincipal) ON UPDATE CASCADE ON DELETE NO ACTION,
    FOREIGN KEY (id_promocio) REFERENCES Promocio(id_promocio) ON UPDATE CASCADE ON DELETE NO ACTION
) ENGINE InnoDB;

# DROP TABLE SlideImatge;
CREATE TABLE IF NOT EXISTS SlideImatge(
	id_slide INTEGER(10),
	num_slideimatge INTEGER(3) AUTO_INCREMENT,
	descripcio TEXT NOT NULL COMMENT 'SI;Descripción;textarea',
	url VARCHAR(200) NOT NULL COMMENT 'SI;URL;text',
	link VARCHAR(200),
	PRIMARY KEY(num_slideimatge, id_slide),
    FOREIGN KEY (id_slide) REFERENCES Slide(id_slide) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE InnoDB;


# DROP TABLE Familia;
CREATE TABLE IF NOT EXISTS Familia(
    id_familia INTEGER(4) AUTO_INCREMENT PRIMARY KEY,
    de_familia INTEGER(4) COMMENT 'SI;Familia raiz;select;familias_tienda;id_familias_tienda;de_familias_tienda IS NULL',
    id_pagina INTEGER(10) NOT NULL,
    descripcio VARCHAR(50) NOT NULL COMMENT 'SI;Descripción;text',
    ordre INTEGER(5) UNSIGNED COMMENT 'SI;Ordre;text',
	UNIQUE (de_familia, descripcio),
    FOREIGN KEY (de_familia) REFERENCES Familia(id_familia) ON UPDATE CASCADE ON DELETE NO ACTION,
    FOREIGN KEY (id_pagina) REFERENCES Pagina(id_pagina) ON UPDATE CASCADE ON DELETE NO ACTION
) ENGINE InnoDB;

# DROP TRIGGER ins_familia_ins;
DELIMITER //
CREATE TRIGGER ins_familia_ins BEFORE INSERT ON Familia FOR EACH ROW
BEGIN
    DECLARE descripcio_final VARCHAR(100);
    IF new.de_familia <> 0 THEN
        SET descripcio_final = CONCAT(CONCAT(new.descripcio, ' - '), (SELECT descripcio FROM Familia WHERE id_familia = new.de_familia));
        INSERT INTO Pagina VALUES(NULL, descripcio_final, descripcio_final, '', '', NOW());
    ELSE
        INSERT INTO Pagina VALUES(NULL, new.descripcio, new.descripcio, '', '', NOW());
    END IF;
    SET new.id_pagina = LAST_INSERT_ID();
END//

# DROP TRIGGER ins_familia_upd//
CREATE TRIGGER ins_familia_upd BEFORE UPDATE ON Familia FOR EACH ROW
BEGIN
    DECLARE descripcio_final VARCHAR(100);
    DECLARE id_urls_amigables_while VARCHAR(100);
    DECLARE id_urls_amigables_2_while VARCHAR(100);
    DECLARE descripcio_while VARCHAR(100);
    DECLARE accion INT DEFAULT 0;
    DECLARE recorrido_urls_amigable CURSOR FOR SELECT id_pagina FROM Familia WHERE de_familia = new.id_familia;
    DECLARE CONTINUE HANDLER FOR SQLSTATE '20000' SET accion = 1;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET accion = 1;
    IF new.de_familia <> 0 THEN
        SET descripcio_final = CONCAT(CONCAT(new.descripcio, ' - '), (SELECT descripcio FROM Familia WHERE id_familia = new.de_familia));
        UPDATE Pagina SET titol = descripcio_final, descripcio = descripcio_final, ultima_modificacio = NOW() WHERE id_pagina = new.id_pagina;
    ELSE
        OPEN recorrido_urls_amigable;
        REPEAT
            FETCH recorrido_urls_amigable INTO id_urls_amigables_while;
            IF NOT accion THEN
                UPDATE Pagina SET titol = CONCAT(CONCAT((SELECT descripcio FROM Familia WHERE id_pagina = id_urls_amigables_while), ' - '), new.descripcio), descripcio = CONCAT(CONCAT((SELECT descripcio FROM Familia WHERE id_pagina = id_urls_amigables_while), ' - '), new.descripcio), ultima_modificacio = NOW() WHERE id_pagina = id_urls_amigables_while;
            END IF;
        UNTIL accion END REPEAT;
        CLOSE recorrido_urls_amigable;
    END IF;
END//
CREATE TRIGGER upd_familia_del AFTER DELETE ON Familia FOR EACH ROW
BEGIN
    DELETE FROM Pagina WHERE id_pagina = old.id_pagina;
END//
DELIMITER ;

# INSERT INTO Familia VALUES(NULL, NULL, 0, 'Familia de prueba', 10);
# INSERT INTO Familia VALUES(NULL, (SELECT f2.id_familia FROM Familia f2 WHERE f2.descripcio = 'Familia de prueba' AND f2.titol IS NULL), 0, 'Subfamilia de prueba', 10);
# INSERT INTO Familia VALUES(NULL, (SELECT f2.id_familia FROM Familia f2 WHERE f2.descripcio = 'Familia de prueba' AND f2.titol IS NULL), 0, 'Subfamilia de prueba 2', 20);
# UPDATE Familia as f1, (SELECT f2.id_familia FROM Familia f2 WHERE f2.descripcio = 'Subfamilia de prueba' AND f2.titol = (SELECT f3.id_familia FROM Familia f3 WHERE f3.descripcio = 'Familia de prueba' AND f3.titol IS NULL)) as ft1 SET f1.descripcio = 'Subfamilias prueba test' WHERE f1.id_familia = ft1.id_familia;
# UPDATE Familia as f1, (SELECT f2.id_familia FROM Familia f2 WHERE f2.descripcio = 'Familia de prueba' AND f2.titol IS NULL) as ft1 SET f1.descripcio = 'Familias prueba test' WHERE f1.id_familia = ft1.id_familia;
# DELETE FROM Familia WHERE descripcio = 'Subfamilias prueba test' AND titol IS NOT NULL;
# DELETE FROM Familia WHERE descripcio = 'Subfamilia de prueba 2' AND titol IS NOT NULL;
# DELETE FROM Familia WHERE descripcio = 'Familias prueba test' AND titol IS NULL;


# DROP TABLE Nivell;
CREATE TABLE IF NOT EXISTS Nivell(
    id_nivell INTEGER(10) AUTO_INCREMENT PRIMARY KEY,
	descripcio VARCHAR(150) UNIQUE COMMENT 'SI;Descripcio;text',
	tipus VARCHAR(25) NOT NULL COMMENT 'SI;Tipo;text',
	adicional VARCHAR(25) NOT NULL COMMENT 'SI;Información adicional;text'
) ENGINE InnoDB;

# DROP TABLE IVA;
CREATE TABLE IF NOT EXISTS IVA(
    id_iva INTEGER(10) AUTO_INCREMENT PRIMARY KEY,
	iva INTEGER(3) NOT NULL COMMENT 'SI;IVA;text'
) ENGINE InnoDB;

INSERT INTO IVA VALUES(NULL, 21);
# DELETE FROM IVA WHERE iva = 21;

# DROP TABLE Fabricant;
CREATE TABLE IF NOT EXISTS Fabricant(
    id_fabricant INTEGER(10) AUTO_INCREMENT PRIMARY KEY,
	descripcio VARCHAR(200) COMMENT 'SI;Descripción;text',
	imatge VARCHAR(200) COMMENT 'SI;Imagen;text'
) ENGINE InnoDB;

# DROP TABLE Proveidor;
CREATE TABLE IF NOT EXISTS Proveidor(
    id_proveidor INTEGER(10) AUTO_INCREMENT PRIMARY KEY,
	descripcio VARCHAR(200) COMMENT 'SI;Descripción;text',
	plac INTEGER(4) COMMENT 'SI;Plazo de entrga por defecto;text'
) ENGINE InnoDB;

# DROP TABLE Article;
CREATE TABLE IF NOT EXISTS Article(
    id_article INTEGER(10) AUTO_INCREMENT,
    id_familia INTEGER(4) COMMENT 'NO',
	id_nivell INTEGER(4) COMMENT 'NO',
    id_pagina INTEGER(10) NOT NULL COMMENT 'NO',
	id_fabricant INTEGER(10) COMMENT 'NO',
	id_proveidor INTEGER(10) COMMENT 'NO',
	id_iva INTEGER(10) NOT NULL COMMENT 'SI;IVA;select;IVA;id_iva',
	descripcio VARCHAR(150) UNIQUE NOT NULL COMMENT 'SI;Descripcio;text',
	plac VARCHAR(25) DEFAULT NULL COMMENT 'SI;Plazo entrega;text',
	imatge VARCHAR(150) DEFAULT NULL COMMENT 'SI;Imagen;text',
    precio DOUBLE(7,2) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'SI;PVP;text',
    referencia_propia VARCHAR(25) UNIQUE COMMENT 'SI;Referencia propia;text',
    referencia_proveidor VARCHAR(25) COMMENT 'SI;Referencia proveedor;text',
    oferta_desde DATETIME COMMENT 'SI;Oferta desde;text',
    oferta_fins DATETIME COMMENT 'SI;Oferta hasta;text',
    preu_oferta DOUBLE(7,2) UNSIGNED COMMENT 'SI;PVP oferta;text',
    stock INTEGER(5) UNSIGNED COMMENT 'SI;Stock;text',
	alta DATETIME COMMENT 'SI;Fecha alta;text',
    actualitzacio DATETIME COMMENT 'SI;Última actualización;text',
    informacio TEXT COMMENT 'SI;Más información;textarea',
    PRIMARY KEY(id_article),
    UNIQUE (id_article, id_nivell),
	FOREIGN KEY (id_familia) REFERENCES Familia(id_familia) ON UPDATE CASCADE ON DELETE NO ACTION,
    FOREIGN KEY (id_nivell) REFERENCES Nivell(id_nivell) ON UPDATE CASCADE ON DELETE NO ACTION,
    FOREIGN KEY (id_pagina) REFERENCES Pagina(id_pagina) ON UPDATE CASCADE ON DELETE NO ACTION,
    FOREIGN KEY (id_iva) REFERENCES IVA(id_iva) ON UPDATE CASCADE ON DELETE NO ACTION,
    FOREIGN KEY (id_fabricant) REFERENCES Fabricant(id_fabricant) ON UPDATE CASCADE ON DELETE NO ACTION,
    FOREIGN KEY (id_proveidor) REFERENCES Proveidor(id_proveidor) ON UPDATE CASCADE ON DELETE NO ACTION
) ENGINE InnoDB;

# INSERT INTO Article SET id_article = NULL, id_familia = (SELECT f2.id_familia FROM Familia f2 WHERE f2.descripcio = 'Subfamilia de prueba' AND f2.titol IS NOT NULL), descripcio = 'Artículo de prueba 2', id_pagina = 0, id_iva = (SELECT id_iva FROM IVA WHERE iva = 21), precio = '11.10';
# UPDATE Article SET descripcio = 'test' WHERE id_article = 1;
# DELETE FROM Article WHERE id_article = 1;

# DROP TRIGGER ins_article_ins;
DELIMITER //
CREATE TRIGGER ins_article_ins BEFORE INSERT ON Article FOR EACH ROW
BEGIN
    DECLARE nuevo_id_urls_amigables INTEGER(10);
    DECLARE descripcio_final VARCHAR(200) DEFAULT new.descripcio;
    INSERT INTO Pagina VALUES(NULL, descripcio_final, descripcio_final, '', '', NOW());
	SET new.id_pagina = LAST_INSERT_ID();
END//
# DROP TRIGGER ins_article_upd//
CREATE TRIGGER ins_article_upd AFTER UPDATE ON Article FOR EACH ROW
BEGIN
    DECLARE de_familia INTEGER(4);
    UPDATE Pagina SET ultima_modificacio = NOW() WHERE id_pagina = (SELECT id_pagina FROM Article WHERE id_article = new.id_article LIMIT 1);
END//
DELIMITER ;

# DROP TRIGGER upd_article_del;

DELIMITER //
CREATE TRIGGER upd_article_del AFTER DELETE ON Article FOR EACH ROW
BEGIN
	DELETE FROM Pagina WHERE id_pagina = old.id_pagina;
END//
DELIMITER ;

# DROP TABLE ArticleEspecial;
CREATE TABLE IF NOT EXISTS Especial(
	id_especial INTEGER(10) AUTO_INCREMENT,
	descripcio VARCHAR(150) UNIQUE COMMENT 'SI;Descripcio;text',
	PRIMARY KEY(id_especial)
) ENGINE InnoDB;

# DROP TABLE ArticleEspecial;
CREATE TABLE IF NOT EXISTS ArticleEspecial(
    id_articleespecial INTEGER(3) AUTO_INCREMENT,
	id_article INTEGER(10) NOT NULL,
	id_especial INTEGER(10) NOT NULL,
	PRIMARY KEY(id_articleespecial, id_article),
	FOREIGN KEY (id_article) REFERENCES Article(id_article) ON UPDATE CASCADE ON DELETE NO ACTION,
	FOREIGN KEY (id_especial) REFERENCES Especial(id_especial) ON UPDATE CASCADE ON DELETE NO ACTION
) ENGINE InnoDB;

# DROP TABLE FitxaTecnica;
CREATE TABLE IF NOT EXISTS FitxaTecnica(
    id_fitxatecnica INTEGER(3) AUTO_INCREMENT,
	id_article INTEGER(10) NOT NULL,
	PRIMARY KEY(id_fitxatecnica, id_article),
	FOREIGN KEY (id_article) REFERENCES Article(id_article) ON UPDATE CASCADE ON DELETE NO ACTION
) ENGINE InnoDB;

# DROP TABLE FitxaTecnicaTitol;
CREATE TABLE IF NOT EXISTS FitxaTecnicaTitol(
    id_fitxatecnicatitol INTEGER(10) AUTO_INCREMENT,
	descripcio VARCHAR(150) UNIQUE COMMENT 'SI;Descripcio;text',
	PRIMARY KEY(id_fitxatecnicatitol)
) ENGINE InnoDB;

# DROP TABLE FitxaTecnicaDescripcio;
CREATE TABLE IF NOT EXISTS FitxaTecnicaDescripcio(
    id_fitxatecnicadescripcio INTEGER(10) AUTO_INCREMENT,
	id_fitxatecnicatitol INTEGER(10),
	descripcio VARCHAR(150) UNIQUE COMMENT 'SI;Descripcio;text',
	PRIMARY KEY(id_fitxatecnicadescripcio, id_fitxatecnicatitol),
	FOREIGN KEY (id_fitxatecnicatitol) REFERENCES FitxaTecnicaTitol(id_fitxatecnicatitol) ON UPDATE CASCADE ON DELETE NO ACTION
) ENGINE InnoDB;

# DROP TABLE FitxaTecnica_FitxaTecnicaDescripcio;
CREATE TABLE IF NOT EXISTS FitxaTecnica_FitxaTecnicaDescripcio(
    id_fitxatecnica INTEGER(3),
	id_fitxatecnicadescripcio INTEGER(10),
	id_fitxatecnicatitol INTEGER(10),
	PRIMARY KEY(id_fitxatecnica, id_fitxatecnicatitol, id_fitxatecnicadescripcio),
	FOREIGN KEY (id_fitxatecnica) REFERENCES FitxaTecnica(id_fitxatecnica) ON UPDATE CASCADE ON DELETE NO ACTION,
	FOREIGN KEY (id_fitxatecnicadescripcio, id_fitxatecnicatitol) REFERENCES FitxaTecnicaDescripcio(id_fitxatecnicadescripcio, id_fitxatecnicatitol) ON UPDATE CASCADE ON DELETE NO ACTION
) ENGINE InnoDB;

CREATE TABLE IF NOT EXISTS FacturesTemp(
  id_facturestemp INTEGER(10) PRIMARY KEY AUTO_INCREMENT,
  codi_sessio VARCHAR(250) NOT NULL UNIQUE,
  data TIMESTAMP NOT NULL,
  forma_pagament VARCHAR(50),
  total DOUBLE(7,2) NOT NULL
) ENGINE InnoDB;

CREATE TABLE IF NOT EXISTS FacturesTempArticle(
  id_facturestemparticle INTEGER(10) AUTO_INCREMENT,
  id_facturestemp INTEGER(10) NOT NULL,
  id_article INTEGER(10) NOT NULL,
  descripcio VARCHAR(250) NOT NULL,
  nivell VARCHAR(250),
  quantitat INTEGER(6),
  preu DOUBLE(7,2) NOT NULL,
  descompte DOUBLE(7,2),
  referencia_propia VARCHAR(250),
  referencia_proveidor VARCHAR(250),
  iva INTEGER(3) NOT NULL,
  data TIMESTAMP NOT NULL,
  PRIMARY KEY(id_facturestemparticle, id_facturestemp),
  FOREIGN KEY (id_facturestemp) REFERENCES FacturesTemp(id_facturestemp) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY (id_article) REFERENCES Article(id_article) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE InnoDB;

CREATE TABLE IF NOT EXISTS Factures(
  id_factures INTEGER(10) PRIMARY KEY AUTO_INCREMENT,
  data TIMESTAMP NOT NULL,
  forma_pagament VARCHAR(50),
  total DOUBLE(7,2) NOT NULL
) ENGINE InnoDB;

CREATE TABLE IF NOT EXISTS FacturesArticle(
  id_facturesarticle INTEGER(10) AUTO_INCREMENT,
  id_factures INTEGER(10) NOT NULL,
  id_article INTEGER(10) NOT NULL,
  descripcio VARCHAR(250) NOT NULL,
  nivell VARCHAR(250),
  quantitat INTEGER(6),
  preu DOUBLE(7,2) NOT NULL,
  descompte DOUBLE(7,2),
  referencia_propia VARCHAR(250),
  referencia_proveidor VARCHAR(250),
  iva INTEGER(3) NOT NULL,
  data TIMESTAMP NOT NULL,
  PRIMARY KEY(id_facturesarticle, id_factures),
  FOREIGN KEY (id_factures) REFERENCES Factures(id_factures) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY (id_article) REFERENCES Article(id_article) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE InnoDB;

CREATE TABLE IF NOT EXISTS FacturesEnvio(
  id_facturesenvio INTEGER(10) PRIMARY KEY AUTO_INCREMENT,
  id_factures INTEGER(10) NOT NULL,
  primer_pedido BOOLEAN NOT NULL,
  num_cliente INT(10),
  nombre_completo VARCHAR(250) NOT NULL,
  dni VARCHAR(10) NOT NULL,
  direccion VARCHAR(250) NOT NULL,
  num VARCHAR(10),
  escalera VARCHAR(10),
  piso VARCHAR(10),
  puerta VARCHAR(10),
  localidad VARCHAR(100),
  codigo_postal INTEGER(5),
  provincia VARCHAR(100),
  pais VARCHAR(100),
  email VARCHAR(250),
  telefono VARCHAR(20),
  FOREIGN KEY (id_factures) REFERENCES Factures(id_factures) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE InnoDB;

CREATE TABLE IF NOT EXISTS FacturesFacturacio(
  id_facturesfacturacio INTEGER(10) PRIMARY KEY AUTO_INCREMENT,
  id_factures INTEGER(10) NOT NULL,
  f_nombre_completo VARCHAR(250) NOT NULL,
  f_dni VARCHAR(10) NOT NULL,
  f_direccion VARCHAR(250) NOT NULL,
  f_num VARCHAR(10),
  f_escalera VARCHAR(10),
  f_piso VARCHAR(10),
  f_puerta VARCHAR(10),
  f_localidad VARCHAR(100),
  f_codigo_postal INTEGER(5),
  f_provincia VARCHAR(100),
  f_pais VARCHAR(100),
  f_email VARCHAR(250),
  f_telefono VARCHAR(20),
  FOREIGN KEY (id_factures) REFERENCES Factures(id_factures) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE InnoDB;

# DROP TABLE Rol;
CREATE TABLE IF NOT EXISTS Rol(
  id_rol INTEGER(10) PRIMARY KEY AUTO_INCREMENT,
  rol VARCHAR(50) NOT NULL UNIQUE
) ENGINE = InnoDB;

# DROP TABLE Usuario;
CREATE TABLE IF NOT EXISTS Usuario(
  id_usuario INTEGER(10) PRIMARY KEY AUTO_INCREMENT,
  id_rol INTEGER(10) NOT NULL,
  usuario VARCHAR(100) NOT NULL,
  apellidos VARCHAR(200) NOT NULL,
  email VARCHAR(255) NOT NULL UNIQUE,
  password VARCHAR(255) NOT NULL,
  cookie VARCHAR(255) DEFAULT NULL,
  activo BOOLEAN DEFAULT FALSE,
  FOREIGN KEY (id_rol) REFERENCES Rol(id_rol) ON UPDATE CASCADE ON DELETE NO ACTION
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS Usuario_Rol(
  id_usuario_rol InTEGER(10) PRIMARY KEY AUTO_INCREMENT,
  id_usuario INTEGER(10) NOT NULL,
  id_rol INTEGER(10) NOT NULL,
  FOREIGN KEY (id_rol) REFERENCES Rol(id_rol) ON UPDATE CASCADE ON DELETE NO ACTION,
  FOREIGN KEY (id_usuario) REFERENCES Usuario(id_usuario) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS Module(
  id_module INTEGER(10) PRIMARY KEY AUTO_INCREMENT,
  id_rol INTEGER(10) NOT NULL,
  id_usuario INTEGER(10) NOT NULL,
  module VARCHAR(255) NOT NULL,
  permissions INTEGER(3) NOT NULL,
  FOREIGN KEY (id_rol) REFERENCES Rol(id_rol) ON UPDATE CASCADE ON DELETE NO ACTION,
  FOREIGN KEY (id_usuario) REFERENCES Usuario(id_usuario) ON UPDATE CASCADE ON DELETE NO ACTION
) ENGINE = InnoDB;