<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 17:59
 * To change this template use File | Settings | File Templates.
 */

function objectToArray($d) {
    if (is_object($d)) {
        // Gets the properties of the given object
        // with get_object_vars function
        $d = get_object_vars($d);
    }

    if (is_array($d)) {
        /*
        * Return array converted to object
        * Using __FUNCTION__ (Magic constant)
        * for recursive call
        */
        return array_map(__FUNCTION__, $d);
    }else{
        // Return array
        return $d;
    }
}

function getAction($action) {
    $parameters = new stdClass();
    $path = $action;
    $nameFileArr = explode('/', $action);
    $nameFile = $nameFileArr[count($nameFileArr) - 1];
    $parameters->nameFile = $nameFile;
    $parameters->path = $path;
    $parameters->url = $parameters->path . '/' . $parameters->nameFile . '.php';
    $parameters->urlParameters = $parameters->path . '/' . 'parameters-' . $parameters->nameFile . '.php';

    return $parameters;
}

$parameters = new stdClass();

$parameters->languages = array('es', 'en', 'ca');