<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($parameters->subaction))
{
    $id_fitxatecnicatitol = $parameters->subaction;
}elseif($_POST['id_fitxatecnicatitol']){
    $id_fitxatecnicatitol = $_POST['id_fitxatecnicatitol'];
}else{
    throw new Exception('Missing parameter: id_fitxatecnicatitol');
}
if(!empty($parameters->group))
{
    $parameters->success = $parameters->group;
}else{
    $parameters->success = 0;
}
$paramsToFitxaTecnicaTitol = Administrador_DB::getInfo(Administrador_FitxaTecnicaTitol::TABLE, '*', array('id_fitxatecnicatitol = '.$id_fitxatecnicatitol));
$fitxaTecnicaTitol = new Administrador_FitxaTecnicaTitol($paramsToFitxaTecnicaTitol[0]);

$parameters->titol = $fitxaTecnicaTitol->getObjectAsArray();

$parameters->buttonsViewTemplate = $twig->loadTemplate('src/module/administrador/general/view/base-buttonsView.html.twig');