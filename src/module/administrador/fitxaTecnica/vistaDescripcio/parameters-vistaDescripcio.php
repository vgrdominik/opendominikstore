<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */
session_start();
$request = $_SESSION['submitted_data'];
$params = explode('&', base64_decode($request));
$request = array();
foreach($params as $param)
{
    $param = explode('=', $param);

    if(!empty($param[0])&&!empty($param[1]))
    {
        $request[$param[0]] = $param[1];
    }
}
if(!empty($_POST['id_fitxatecnicatitol'])){
    $id_fitxatecnicatitol = $_POST['id_fitxatecnicatitol'];
}elseif(!empty($request['id_fitxatecnicatitol'])){
    $id_fitxatecnicatitol = $request['id_fitxatecnicatitol'];
}else{
    throw new Exception('Missing parameter: id_fitxatecnicatitol');
}

if(!empty($_POST['id_fitxatecnicadescripcio'])){
    $id_fitxatecnicadescripcio = $_POST['id_fitxatecnicadescripcio'];
}elseif(!empty($request['id_fitxatecnicadescripcio'])){
    $id_fitxatecnicadescripcio = $request['id_fitxatecnicadescripcio'];
}else{
    throw new Exception('Missing parameter: id_fitxatecnicadescripcio');
}
if(!empty($parameters->group))
{
    $parameters->success = $parameters->group;
}else{
    $parameters->success = 0;
}
$paramsToFitxaTecnicaDescripcio = Administrador_DB::getInfo(Administrador_FitxaTecnicaDescripcio::TABLE, '*', array('id_fitxatecnicadescripcio = '.$id_fitxatecnicadescripcio, 'id_fitxatecnicatitol = '.$id_fitxatecnicatitol));
$fitxaTecnicaDescripcio = new Administrador_FitxaTecnicaDescripcio($paramsToFitxaTecnicaDescripcio[0]);

$parameters->descripcio = $fitxaTecnicaDescripcio->getObjectAsArray();
$parameters->descripcio['titol'] = $fitxaTecnicaDescripcio->getTitol();

$parameters->buttonsViewTemplate = $twig->loadTemplate('src/module/administrador/general/view/base-buttonsView.html.twig');