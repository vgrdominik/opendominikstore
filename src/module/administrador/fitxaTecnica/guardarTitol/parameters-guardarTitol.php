<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($_POST['id_fitxatecnicatitol']))
{
    if($_POST['id_fitxatecnicatitol'] == -1)
    {
        $success = '2';
    }else{
        $success = '1';
    }
}else{
    throw new Exception('Missing parameter: id_fitxatecnicatitol');
}

$textBanner = new Administrador_FitxaTecnicaTitol($_POST);
$textBanner->save();

header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/fitxaTecnica/vistaTitol.html/'.$textBanner->getIdFitxaTecnicaTitol().'/'.$success);
exit();