<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($parameters->subaction))
{
    $parameters->success = $parameters->subaction;
}

$parameters->indexTitol = Administrador_DB::getInfo(Administrador_FitxaTecnicaTitol::TABLE);

foreach($parameters->indexTitol as $texto)
{
    $descripcio = new Administrador_FitxaTecnicaTitol($texto);
    $texto->descripcio = substr(strip_tags($descripcio->getDescripcio()), 0, 100);
}

$parameters->indexDescripcio = Administrador_DB::getInfo(Administrador_FitxaTecnicaDescripcio::TABLE);

foreach($parameters->indexDescripcio as $texto)
{
    $descripcio = new Administrador_FitxaTecnicaDescripcio($texto);
    $texto->descripcio = substr(strip_tags($descripcio->getDescripcio()), 0, 50);
    $texto->titol = substr(strip_tags($descripcio->getTitol()), 0, 50);
}