<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($_POST['id_fitxatecnicatitol']))
{
    if($_POST['id_fitxatecnicatitol'] == -1)
    {
        $success = '2';
    }else{
        $success = '1';
    }
}else{
    throw new Exception('Missing parameter: id_fitxatecnicatitol');
}

if(empty($_POST['id_fitxatecnicadescripcio']))
{
    throw new Exception('Missing parameter: id_fitxatecnicadescripcio');
}

$fitxaTecnicaDescripcio = new Administrador_FitxaTecnicaDescripcio($_POST);
$fitxaTecnicaDescripcio->save();

session_start();
$_SESSION['submitted_data'] = base64_encode('id_fitxatecnicadescripcio='.$fitxaTecnicaDescripcio->getIdFitxaTecnicaDescripcio().'&id_fitxatecnicatitol='.$fitxaTecnicaDescripcio->getIdFitxaTecnicaTitol().'&success='.$success);

header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/fitxaTecnica/vistaDescripcio.html');
exit();