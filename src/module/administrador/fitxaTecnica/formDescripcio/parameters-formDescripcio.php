<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($_POST['id_fitxatecnicatitol']))
{
    $id_fitxatecnicatitol = $_POST['id_fitxatecnicatitol'];
}else{
    throw new Exception('Missing parameter: id_fitxatecnicatitol');
}
if(!empty($_POST['id_fitxatecnicadescripcio']))
{
    $id_fitxatecnicadescripcio = $_POST['id_fitxatecnicadescripcio'];
}else{
    throw new Exception('Missing parameter: id_fitxatecnicadescripcio');
}
if($id_fitxatecnicatitol != '-1'&&$id_fitxatecnicadescripcio != '-1')
{
    $paramsToFitxaTecnicaDescripcio = Administrador_DB::getInfo(Administrador_FitxaTecnicaDescripcio::TABLE, '*', array('id_fitxatecnicadescripcio = '.$id_fitxatecnicadescripcio, 'id_fitxatecnicatitol = '.$id_fitxatecnicatitol));
    $fitxaTecnicaDescripcio = new Administrador_FitxaTecnicaDescripcio($paramsToFitxaTecnicaDescripcio[0]);
    $parameters->descripcio = $fitxaTecnicaDescripcio->getObjectAsArray();
}

$parameters->titols = Administrador_DB::getInfo(Administrador_FitxaTecnicaTitol::TABLE, '*');
foreach($parameters->titols as $titulo)
{
    $titulo->descripcio = strip_tags($titulo->descripcio);
}

$parameters->buttonsFormTemplate = $twig->loadTemplate('src/module/administrador/general/view/base-buttonsForm.html.twig');