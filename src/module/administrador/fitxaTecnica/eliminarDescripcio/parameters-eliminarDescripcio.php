<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(empty($_POST['id_fitxatecnicatitol']))
{
    throw new Exception('Missing parameter: id_fitxatecnicatitol');
}else{
    $idFitxaTecnicaTitol = $_POST['id_fitxatecnicatitol'];
}

if(empty($_POST['id_fitxatecnicadescripcio']))
{
    throw new Exception('Missing parameter: id_fitxatecnicadescripcio');
}else{
    $idFitxaTecnicaDescripcio = $_POST['id_fitxatecnicadescripcio'];
}

$paramsToFitxaTecnicaDescripcio = Administrador_DB::getInfo(Administrador_FitxaTecnicaDescripcio::TABLE, '*', array(
    'id_fitxatecnicadescripcio = '.$idFitxaTecnicaDescripcio,
    'id_fitxatecnicatitol = '.$idFitxaTecnicaTitol
));
$FitxaTecnicaDescripcio = new Administrador_FitxaTecnicaDescripcio($paramsToFitxaTecnicaDescripcio[0]);
$success = $FitxaTecnicaDescripcio->delete();

$error = $success->errorInfo();

if(!empty($error[1]))
{
    $success = 2;
}else{
    $success = 1;
}

header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/fitxaTecnica/index.html/'.$success);
exit();