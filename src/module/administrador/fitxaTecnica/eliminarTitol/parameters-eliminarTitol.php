<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(empty($_POST['id_fitxatecnicatitol']))
{
    throw new Exception('Missing parameter: id_fitxatecnicatitol');
}else{
    $idFitxaTecnicaTitol = $_POST['id_fitxatecnicatitol'];
}

$paramsToFitxaTecnicaTitol = Administrador_DB::getInfo(Administrador_FitxaTecnicaTitol::TABLE, '*', array('id_fitxatecnicatitol = '.$idFitxaTecnicaTitol));
$FitxaTecnicaTitol = new Administrador_FitxaTecnicaTitol($paramsToFitxaTecnicaTitol[0]);
$success = $FitxaTecnicaTitol->delete();

$error = $success->errorInfo();

if(!empty($error[1]))
{
    $success = 2;
}else{
    $success = 1;
}

header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/fitxaTecnica/index.html/'.$success);
exit();