<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($parameters->subaction))
{
    $id_familia = $parameters->subaction;
}elseif($_POST['id_familia']){
    $id_familia = $_POST['id_familia'];
}else{
    throw new Exception('Missing parameter: id_familia');
}
if(!empty($parameters->group))
{
    $parameters->success = $parameters->group;
}else{
    $parameters->success = 0;
}
$paramsToFamilia = Administrador_DB::getInfo(Administrador_Familia::TABLE, '*', array('id_familia = '.$id_familia));
$familia = new Administrador_Familia($paramsToFamilia[0]);

$parameters->familias = Administrador_DB::getInfo(Administrador_Familia::TABLE, '*', array('de_familia is NULL'));
$parameters->familia = $familia->getObjectAsArray();

$parameters->buttonsViewTemplate = $twig->loadTemplate('src/module/administrador/general/view/base-buttonsView.html.twig');