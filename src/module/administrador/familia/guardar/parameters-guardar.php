<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($_POST['id_familia']))
{
    if($_POST['id_familia'] == -1)
    {
        $success = '2';
    }else{
        $success = '1';
    }
}else{
    throw new Exception('Missing parameter: id_familia');
}

$familia = new Administrador_Familia($_POST);
$familia->save();

header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/familia/vista.html/'.$familia->getIdFamilia().'/'.$success);
exit();