<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($parameters->subaction))
{
    $parameters->success = $parameters->subaction;
}

$parameters->index = Administrador_DB::getInfo(Administrador_Familia::TABLE.' f', array(
    'id_familia',
    'descripcio',
    'CONCAT(CONCAT((SELECT descripcio FROM '.Administrador_Familia::TABLE.' WHERE id_familia = f.de_familia), \' -> \'), descripcio) AS descripcioDe',
    'ordre'
));