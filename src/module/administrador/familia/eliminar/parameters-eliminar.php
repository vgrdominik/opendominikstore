<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(empty($_POST['id_familia']))
{
    throw new Exception('Missing parameter: id_familia');
}else{
    $idFamily = $_POST['id_familia'];
}

$paramsToFamilia = Administrador_DB::getInfo(Administrador_Familia::TABLE, '*', array('id_familia = '.$idFamily));
$familia = new Administrador_Familia($paramsToFamilia[0]);
$success = $familia->delete();

$error = $success->errorInfo();

if(!empty($error[1]))
{
    $success = 2;
}else{
    $success = 1;
}

header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/familia/index.html/'.$success);
exit();