<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($_POST['id_familia']))
{
    $id_familia = $_POST['id_familia'];
}else{
    throw new Exception('Missing parameter: id_familia');
}
if($id_familia != '-1')
{
    $paramsToFamilia = Administrador_DB::getInfo(Administrador_Familia::TABLE, '*', array('id_familia = '.$id_familia));
    $familia = new Administrador_Familia($paramsToFamilia[0]);
    $parameters->familia = $familia->getObjectAsArray();
}


$parameters->familias = Administrador_DB::getInfo(Administrador_Familia::TABLE, '*', array('de_familia is NULL'));

$parameters->buttonsFormTemplate = $twig->loadTemplate('src/module/administrador/general/view/base-buttonsForm.html.twig');