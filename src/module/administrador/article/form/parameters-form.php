<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($_POST['id_article']))
{
    $id_article = $_POST['id_article'];
}else{
    throw new Exception('Missing parameter: id_article');
}
if($id_article != '-1')
{
    $paramsToArticle = Administrador_DB::getInfo(Administrador_Article::TABLE, '*', array('id_article = '.$id_article));
    $article = new Administrador_Article($paramsToArticle[0]);
    $parameters->article = $article->getObjectAsArray();
}

$parameters->familia = Administrador_DB::getInfo(Administrador_Familia::TABLE, '*');
$parameters->nivell = Administrador_DB::getInfo(Administrador_Nivell::TABLE, '*');
$parameters->fabricant = Administrador_DB::getInfo(Administrador_Fabricant::TABLE, '*');
$parameters->proveidor = Administrador_DB::getInfo(Administrador_Proveidor::TABLE, '*');
$parameters->iva = Administrador_DB::getInfo(Administrador_IVA::TABLE, '*');

$parameters->buttonsFormTemplate = $twig->loadTemplate('src/module/administrador/general/view/base-buttonsForm.html.twig');