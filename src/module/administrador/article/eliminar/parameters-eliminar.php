<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(empty($_POST['id_article']))
{
    throw new Exception('Missing parameter: id_article');
}else{
    $idArticle = $_POST['id_article'];
}

$paramsToArticle = Administrador_DB::getInfo(Administrador_Article::TABLE, '*', array('id_article = '.$idArticle));
$article = new Administrador_Article($paramsToArticle[0]);
$success = $article->delete();

$error = $success->errorInfo();

if(!empty($error[1]))
{
    $success = 2;
}else{
    $success = 1;
}

header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/article/index.html/'.$success);
exit();