<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($parameters->subaction))
{
    $id_article = $parameters->subaction;
}elseif(!empty($_POST['id_article'])){
    $id_article = $_POST['id_article'];
}else{
    throw new Exception('Missing parameter: id_article');
}
if(!empty($parameters->group))
{
    $parameters->success = $parameters->group;
}else{
    $parameters->success = 0;
}
$paramsToArticle = Administrador_DB::getInfo(Administrador_Article::TABLE, '*', array('id_article = '.$id_article));
$article = new Administrador_Article($paramsToArticle[0]);

$parameters->article = $article->getObjectAsArray();
$parameters->article['familia'] = $article->getFamilia();
$parameters->article['nivell'] = $article->getNivell();
$parameters->article['pagina'] = $article->getPagina();
$parameters->article['fabricant'] = $article->getFabricant();
$parameters->article['proveidor'] = $article->getProveidor();
$parameters->article['iva'] = $article->getIva();

$parameters->buttonsViewTemplate = $twig->loadTemplate('src/module/administrador/general/view/base-buttonsView.html.twig');