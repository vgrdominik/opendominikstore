<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($_POST['id_article']))
{
    if($_POST['id_article'] == -1)
    {
        $success = '2';
    }else{
        $success = '1';
    }
}else{
    throw new Exception('Missing parameter: id_article');
}

$article = new Administrador_Article($_POST);
$article->save();

header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/article/vista.html/'.$article->getIdArticle().'/'.$success);
exit();