<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($parameters->subaction))
{
    $id_nivell = $parameters->subaction;
}elseif($_POST['id_nivell']){
    $id_nivell = $_POST['id_nivell'];
}else{
    throw new Exception('Missing parameter: id_nivell');
}
if(!empty($parameters->group))
{
    $parameters->success = $parameters->group;
}else{
    $parameters->success = 0;
}
$paramsToNivell = Administrador_DB::getInfo(Administrador_Nivell::TABLE, '*', array('id_nivell = '.$id_nivell));
$nivell = new Administrador_Nivell($paramsToNivell[0]);

$parameters->nivell = $nivell->getObjectAsArray();

$parameters->buttonsViewTemplate = $twig->loadTemplate('src/module/administrador/general/view/base-buttonsView.html.twig');