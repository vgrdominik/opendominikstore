<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(empty($_POST['id_nivell']))
{
    throw new Exception('Missing parameter: id_nivell');
}else{
    $idNivell = $_POST['id_nivell'];
}

$paramsToNivell = Administrador_DB::getInfo(Administrador_Nivell::TABLE, '*', array('id_nivell = '.$idNivell));
$nivell = new Administrador_Nivell($paramsToNivell[0]);
$success = $nivell->delete();

$error = $success->errorInfo();

if(!empty($error[1]))
{
    $success = 2;
}else{
    $success = 1;
}

header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/nivell/index.html/'.$success);
exit();