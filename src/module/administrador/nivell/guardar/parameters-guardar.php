<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($_POST['id_nivell']))
{
    if($_POST['id_nivell'] == -1)
    {
        $success = '2';
    }else{
        $success = '1';
    }
}else{
    throw new Exception('Missing parameter: id_nivell');
}

$nivell = new Administrador_Nivell($_POST);
$nivell->save();

header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/nivell/vista.html/'.$nivell->getIdNivell().'/'.$success);
exit();