<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

$request = json_decode(file_get_contents('php://input'));
if(!empty($request->id_promocio))
{
    if($request->id_promocio == -1)
    {
        $success = '2';
        $index = array();
    }else{
        $success = '1';
        $index = Administrador_Promocio::getIndexFromDB($request->id_promocio);
    }
}else{
    throw new Exception('Missing parameter: id_promocio');
}

$index = new Administrador_Promocio($request, $index);
$index->save();

echo $index->getIdPromocio();
//header('Location: '.$parameters->path.'/'.$parameters->language.'/paginaPrincipal/vista.html/'.$index->getIdIndexPaginaPrincipal().'/'.$success);