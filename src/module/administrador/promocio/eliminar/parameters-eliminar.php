<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(empty($_POST['id_promocio']))
{
    throw new Exception('Missing parameter: id_promocio');
}else{
    $idPromocio = $_POST['id_promocio'];
}

$paramsToPromocio = Administrador_DB::getInfo(Administrador_Promocio::TABLE, '*', array('id_promocio = '.$idPromocio));
$promocio = new Administrador_Promocio($paramsToPromocio[0], array());
$success = $promocio->delete();

$error = $success->errorInfo();

if(!empty($error[1]))
{
    $success = 2;
}else{
    $success = 1;
}

header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/promocio/index.html/'.$success);
exit();