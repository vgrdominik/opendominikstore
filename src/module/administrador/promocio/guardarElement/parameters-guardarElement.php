<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

$request = json_decode(file_get_contents('php://input'));
if(empty($request->id_promocio))
{
    throw new Exception('Missing parameter: id_promocio');
}
if(empty($request->id_pagina))
{
    throw new Exception('Missing parameter: id_pagina');
}

$element = Administrador_Promocio::saveElement(
    $request->id_promocio,
    $request->id_pagina
);

$request->info = $element;
echo json_encode($request);
//header('Location: '.$parameters->path.'/'.$parameters->language.'/paginaPrincipal/vista.html/'.$index->getIdIndexPaginaPrincipal().'/'.$success);