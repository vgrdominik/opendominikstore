<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

$request = json_decode(file_get_contents('php://input'));
if(empty($request->id_promocio))
{
    throw new Exception('Missing parameter: id_promocio');
}
if(empty($request->id_pagina))
{
    throw new Exception('Missing parameter: id_pagina');
}

$paramsToPromocio = Administrador_DB::getInfo(Administrador_Promocio::TABLE, '*', array('id_promocio = '.$request->id_promocio));
$index = Administrador_Promocio::getIndexFromDB($request->id_promocio);
$promocio = new Administrador_Promocio($paramsToPromocio[0], $index);
$promocio->deleteIndexElement($request->id_pagina);
$promocio->saveIndex();
$return = new stdClass();
$return->info = 'Elemento eliminado';

echo json_encode($return);