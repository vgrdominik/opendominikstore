<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($_POST['id_promocio']))
{
    $id_promocio = $_POST['id_promocio'];
}else{
    throw new Exception('Missing parameter: id_promocio');
}
if($id_promocio != '-1')
{
    $paramsToPromocio = Administrador_DB::getInfo(Administrador_Promocio::TABLE, '*', array('id_promocio = '.$id_promocio));
    $index = Administrador_Promocio::getIndexFromDB($id_promocio);
    $index = new Administrador_Promocio($paramsToPromocio[0], $index);
    $parameters->promocio = $index->getObjectAsArray();
}

$parameters->widgets = Administrador_Widget::getWidgetsName();

$pagines = Administrador_DB::getInfo(Administrador_Pagina::TABLE);
$parameters->pagines = array();
foreach($pagines as $pagina)
{
    $keywords = Administrador_DB::getInfo(Administrador_Pagina::TABLE_KEYWORDS, '*', array('id_pagina = '.$pagina->id_pagina));
    $paginaInstance = new Administrador_Pagina($pagina, $keywords);
    $parameters->pagines[] = $paginaInstance->getObjectAsArrayOnlyPagina();
}