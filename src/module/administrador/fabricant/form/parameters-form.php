<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($_POST['id_fabricant']))
{
    $id_fabricant = $_POST['id_fabricant'];
}else{
    throw new Exception('Missing parameter: id_fabricant');
}
if($id_fabricant != '-1')
{
    $paramsToFabricant = Administrador_DB::getInfo(Administrador_Fabricant::TABLE, '*', array('id_fabricant = '.$id_fabricant));
    $fabricant = new Administrador_Fabricant($paramsToFabricant[0]);
    $parameters->fabricant = $fabricant->getObjectAsArray();
}

$parameters->buttonsFormTemplate = $twig->loadTemplate('src/module/administrador/general/view/base-buttonsForm.html.twig');