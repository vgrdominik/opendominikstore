<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(empty($_POST['id_fabricant']))
{
    throw new Exception('Missing parameter: id_fabricant');
}else{
    $idFabricant = $_POST['id_fabricant'];
}

$paramsToFabricant = Administrador_DB::getInfo(Administrador_Fabricant::TABLE, '*', array('id_fabricant = '.$idFabricant));
$fabricant = new Administrador_Fabricant($paramsToFabricant[0]);
$success = $fabricant->delete();

$error = $success->errorInfo();

if(!empty($error[1]))
{
    $success = 2;
}else{
    $success = 1;
}

header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/fabricant/index.html/'.$success);
exit();