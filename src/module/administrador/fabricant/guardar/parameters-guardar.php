<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($_POST['id_fabricant']))
{
    if($_POST['id_fabricant'] == -1)
    {
        $success = '2';
    }else{
        $success = '1';
    }
}else{
    throw new Exception('Missing parameter: id_fabricant');
}

$fabricant = new Administrador_Fabricant($_POST);
$fabricant->save();

header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/fabricant/vista.html/'.$fabricant->getIdFabricant().'/'.$success);
exit();