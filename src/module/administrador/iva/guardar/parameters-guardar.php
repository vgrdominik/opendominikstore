<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($_POST['id_iva']))
{
    if($_POST['id_iva'] == -1)
    {
        $success = '2';
    }else{
        $success = '1';
    }
}else{
    throw new Exception('Missing parameter: id_iva');
}

$iva = new Administrador_IVA($_POST);
$iva->save();

header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/iva/vista.html/'.$iva->getIdIva().'/'.$success);
exit();