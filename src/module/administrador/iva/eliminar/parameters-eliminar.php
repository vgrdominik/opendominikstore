<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(empty($_POST['id_iva']))
{
    throw new Exception('Missing parameter: id_iva');
}else{
    $idIVA = $_POST['id_iva'];
}

$paramsToIVA = Administrador_DB::getInfo(Administrador_IVA::TABLE, '*', array('id_iva = '.$idIVA));
$iva = new Administrador_IVA($paramsToIVA[0]);
$success = $iva->delete();

$error = $success->errorInfo();

if(!empty($error[1]))
{
    $success = 2;
}else{
    $success = 1;
}

header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/iva/index.html/'.$success);
exit();