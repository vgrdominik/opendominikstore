<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($parameters->subaction))
{
    $id_iva = $parameters->subaction;
}elseif($_POST['id_iva']){
    $id_iva = $_POST['id_iva'];
}else{
    throw new Exception('Missing parameter: id_iva');
}
if(!empty($parameters->group))
{
    $parameters->success = $parameters->group;
}else{
    $parameters->success = 0;
}
$paramsToIVA = Administrador_DB::getInfo(Administrador_IVA::TABLE, '*', array('id_iva = '.$id_iva));
$iva = new Administrador_IVA($paramsToIVA[0]);

$parameters->iva = $iva->getObjectAsArray();

$parameters->buttonsViewTemplate = $twig->loadTemplate('src/module/administrador/general/view/base-buttonsView.html.twig');