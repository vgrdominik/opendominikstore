<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(empty($_POST['id_contingut']))
{
    throw new Exception('Missing parameter: id_contingut');
}else{
    $idContingut = $_POST['id_contingut'];
}

$paramsToContingut = Administrador_DB::getInfo(Administrador_Contingut::TABLE, '*', array('id_contingut = '.$idContingut));
$contingut = new Administrador_Contingut($paramsToContingut[0]);
$success = $contingut->delete();

$error = $success->errorInfo();

if(!empty($error[1]))
{
    $success = 2;
}else{
    $success = 1;
}

header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/contingut/index.html/'.$success);
exit();