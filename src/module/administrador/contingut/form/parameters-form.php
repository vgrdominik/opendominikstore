<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($_POST['id_contingut']))
{
    $id_contingut = $_POST['id_contingut'];
}else{
    throw new Exception('Missing parameter: id_contingut');
}
if($id_contingut != '-1')
{
    $paramsToContingut = Administrador_DB::getInfo(Administrador_Contingut::TABLE, '*', array('id_contingut = '.$id_contingut));
    $contingut = new Administrador_Contingut($paramsToContingut[0]);
    $parameters->contingut = $contingut->getObjectAsArray();
}

$parameters->buttonsFormTemplate = $twig->loadTemplate('src/module/administrador/general/view/base-buttonsForm.html.twig');