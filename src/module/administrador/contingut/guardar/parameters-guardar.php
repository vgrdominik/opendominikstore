<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($_POST['id_contingut']))
{
    if($_POST['id_contingut'] == -1)
    {
        $success = '2';
    }else{
        $success = '1';
    }
}else{
    throw new Exception('Missing parameter: id_contingut');
}

$contingut = new Administrador_Contingut($_POST);
$contingut->save();

header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/contingut/vista.html/'.$contingut->getIdContingut().'/'.$success);
exit();