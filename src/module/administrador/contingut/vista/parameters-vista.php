<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($parameters->subaction))
{
    $id_contingut = $parameters->subaction;
}elseif(!empty($_POST['id_contingut'])){
    $id_contingut = $_POST['id_contingut'];
}else{
    throw new Exception('Missing parameter: id_contingut');
}
if(!empty($parameters->group))
{
    $parameters->success = $parameters->group;
}else{
    $parameters->success = 0;
}
$paramsToContigut = Administrador_DB::getInfo(Administrador_Contingut::TABLE, '*', array('id_contingut = '.$id_contingut));
$contingut = new Administrador_Contingut($paramsToContigut[0]);

$parameters->contingut = $contingut->getObjectAsArray();

$parameters->buttonsViewTemplate = $twig->loadTemplate('src/module/administrador/general/view/base-buttonsView.html.twig');