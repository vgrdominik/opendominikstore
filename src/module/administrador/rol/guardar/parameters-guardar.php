<?php


if(!empty($_POST['id_rol']))
{
    if($_POST['id_rol'] == -1)
    {
        $success = '2';
    }else{
        $success = '1';
    }
}else{
    throw new Exception('Missing parameter: id_rol');
}
try{
	$rol = new Administrador_Usuario_Rol($_POST);
	$rol->save();
}catch(Exception $e){
	header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/rol/index.html/3');
	exit();
}
header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/rol/vista.html/'.$rol->getIdRol().'/'.$success);
exit();