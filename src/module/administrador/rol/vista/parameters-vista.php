<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($parameters->subaction))
{
    $id_rol = $parameters->subaction;
}elseif($_POST['id_rol']){
    $id_rol = $_POST['id_rol'];
}else{
    throw new Exception('Missing parameter: id_rol');
}
if(!empty($parameters->group))
{
    $parameters->success = $parameters->group;
}else{
    $parameters->success = 0;
}
$paramsToRol = Administrador_DB::getInfo(Administrador_Usuario_Rol::TABLE, '*', array('id_rol = '.$id_rol));
$paramsToRol[0]->usuarios = array();
$paramsUsuariosRolToRol = Administrador_DB::getInfo(Administrador_Usuario_UsuarioRol::TABLE, '*', array('id_rol = '.$id_rol));
foreach($paramsUsuariosRolToRol as $paramsUsuarioRolToRol)
{
    $paramsUsuarioRolToRol = Administrador_DB::getInfo(Administrador_Usuario_Usuario::TABLE, '*', array('id_usuario = '.$paramsUsuarioRolToUsuario->id_usuario));
    $paramsToRol[0]->usuarios[] = $paramsUsuarioRolToUsuario[0];
}
$rol = new Administrador_Usuario_Rol($paramsToRol[0]);

$parameters->rol = $rol->getObjectAsArray();

$parameters->buttonsViewTemplate = $twig->loadTemplate('src/module/administrador/general/view/base-buttonsView.html.twig');