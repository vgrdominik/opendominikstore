<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(empty($_POST['id_rol']))
{
    throw new Exception('Missing parameter: id_rol');
}else{
    $idRol= $_POST['id_rol'];
}

$paramsToRol = Administrador_DB::getInfo(Administrador_Usuario_Rol::TABLE, '*', array('id_rol = '.$idRol));
$rol = new Administrador_Usuario_Rol($paramsToRol[0]);
$success = $rol->delete();

$error = $success->errorInfo();

$success = 1;

header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/rol/index.html/'.$success);
exit();