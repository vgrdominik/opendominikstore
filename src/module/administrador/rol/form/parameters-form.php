<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($_POST['id_rol']))
{
    $id_rol = $_POST['id_rol'];
}else{
    throw new Exception('Missing parameter: id_rol');
}
if($id_rol != '-1')
{
    $paramsToRol = Administrador_DB::getInfo(Administrador_Usuario_Rol::TABLE, '*', array('id_rol = '.$id_rol));
    $rol = new Administrador_Usuario_Rol($paramsToRol[0]);
    $parameters->rol = $rol->getObjectAsArray();
}

$parameters->buttonsFormTemplate = $twig->loadTemplate('src/module/administrador/general/view/base-buttonsForm.html.twig');