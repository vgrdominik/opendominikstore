<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

$request = json_decode(file_get_contents('php://input'));
if(!empty($request->id_indexpaginaprincipal))
{
    if($request->id_indexpaginaprincipal == -1)
    {
        $success = '2';
        $index = array();
    }else{
        $success = '1';
        $index = Administrador_IndexPaginaPrincipal::getIndexFromDB($request->id_indexpaginaprincipal);
    }
}else{
    throw new Exception('Missing parameter: id_indexpaginaprincipal');
}

$index = new Administrador_IndexPaginaPrincipal($request, $index);
$index->save();

echo $index->getIdIndexPaginaPrincipal();
//header('Location: '.$parameters->path.'/'.$parameters->language.'/paginaPrincipal/vista.html/'.$index->getIdIndexPaginaPrincipal().'/'.$success);