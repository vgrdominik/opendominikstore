<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($_POST['id_indexpaginaprincipal']))
{
    $id_indexpaginaprincipal = $_POST['id_indexpaginaprincipal'];
}else{
    throw new Exception('Missing parameter: id_indexpaginaprincipal');
}
if($id_indexpaginaprincipal != '-1')
{
    $paramsToIndexPaginaPrincipal = Administrador_DB::getInfo(Administrador_IndexPaginaPrincipal::TABLE, '*', array('id_indexpaginaprincipal = '.$id_indexpaginaprincipal));
    $index = Administrador_IndexPaginaPrincipal::getIndexFromDB($id_indexpaginaprincipal);
    $index = new Administrador_IndexPaginaPrincipal($paramsToIndexPaginaPrincipal[0], $index);
    $parameters->index = $index->getObjectAsArray();
}

$parameters->widgets = Administrador_Widget::getWidgetsName();

$pagines = Administrador_DB::getInfo(Administrador_Pagina::TABLE);
$parameters->pagines = array();
foreach($pagines as $pagina)
{
    $keywords = Administrador_DB::getInfo(Administrador_Pagina::TABLE_KEYWORDS, '*', array('id_pagina = '.$pagina->id_pagina));
    $paginaInstance = new Administrador_Pagina($pagina, $keywords);
    $parameters->pagines[] = $paginaInstance->getObjectAsArrayOnlyPagina();
}