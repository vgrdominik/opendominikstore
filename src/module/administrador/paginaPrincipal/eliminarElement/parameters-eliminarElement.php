<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(empty($_POST['id_indexpaginaprincipal']))
{
    throw new Exception('Missing parameter: id_indexpaginaprincipal');
}else{
    $idIndexPaginaPrincipal = $_POST['id_indexpaginaprincipal'];
}

if(empty($_POST['tipus']))
{
    throw new Exception('Missing parameter: tipus');
}else{
    $tipus = $_POST['tipus'];
}

if(empty($_POST['id_tipus']))
{
    throw new Exception('Missing parameter: id_tipus');
}else{
    $id_tipus = $_POST['id_tipus'];
}

$paramsToPaginaPrincipal = Administrador_DB::getInfo(Administrador_IndexPaginaPrincipal::TABLE, '*', array('id_indexpaginaprincipal = '.$idIndexPaginaPrincipal));
$index = Administrador_IndexPaginaPrincipal::getIndexFromDB($idIndexPaginaPrincipal);
$paginaPrincipal = new Administrador_IndexPaginaPrincipal($paramsToPaginaPrincipal[0], $index);
$paginaPrincipal->deleteIndexElement($tipus, $id_tipus);
$paginaPrincipal->saveIndex();