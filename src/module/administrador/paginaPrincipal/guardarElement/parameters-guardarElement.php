<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

$request = json_decode(file_get_contents('php://input'));
if(empty($request->id_indexpaginaprincipal))
{
    throw new Exception('Missing parameter: id_indexpaginaprincipal');
}
if(empty($request->tipus))
{
    throw new Exception('Missing parameter: tipus');
}
if(empty($request->id_tipus))
{
    throw new Exception('Missing parameter: id_tipus');
}
if(empty($request->width))
{
    throw new Exception('Missing parameter: width');
}
if(empty($request->height))
{
    throw new Exception('Missing parameter: height');
}
if(empty($request->ordre))
{
    throw new Exception('Missing parameter: ordre');
}

$element = Administrador_IndexPaginaPrincipal::saveElement(
    $request->id_indexpaginaprincipal,
    $request->tipus,
    $request->id_tipus,
    $request->width,
    $request->height,
    $request->ordre
);

$request->info = $element;
echo json_encode($request);
//header('Location: '.$parameters->path.'/'.$parameters->language.'/paginaPrincipal/vista.html/'.$index->getIdIndexPaginaPrincipal().'/'.$success);