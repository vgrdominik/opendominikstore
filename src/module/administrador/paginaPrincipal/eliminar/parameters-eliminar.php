<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(empty($_POST['id_indexpaginaprincipal']))
{
    throw new Exception('Missing parameter: id_indexpaginaprincipal');
}else{
    $idIndexPaginaPrincipal = $_POST['id_indexpaginaprincipal'];
}

$paramsToPaginaPrincipal = Administrador_DB::getInfo(Administrador_IndexPaginaPrincipal::TABLE, '*', array('id_indexpaginaprincipal = '.$idIndexPaginaPrincipal));
$paginaPrincipal = new Administrador_IndexPaginaPrincipal($paramsToPaginaPrincipal[0], array());
$success = $paginaPrincipal->delete();

$error = $success->errorInfo();

if(!empty($error[1]))
{
    $success = 2;
}else{
    $success = 1;
}

header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/paginaPrincipal/index.html/'.$success);
exit();