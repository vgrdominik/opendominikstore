<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(empty($_POST['id_especial']))
{
    throw new Exception('Missing parameter: id_especial');
}else{
    $idEspecial = $_POST['id_especial'];
}

$paramsToEspecial = Administrador_DB::getInfo(Administrador_Especial::TABLE, '*', array('id_especial = '.$idEspecial));
$proveidor = new Administrador_Especial($paramsToEspecial[0]);
$success = $proveidor->delete();

$error = $success->errorInfo();

if(!empty($error[1]))
{
    $success = 2;
}else{
    $success = 1;
}

header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/especial/index.html/'.$success);
exit();