<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($parameters->subaction))
{
    $id_especial = $parameters->subaction;
}elseif(!empty($_POST['id_especial'])){
    $id_especial = $_POST['id_especial'];
}else{
    throw new Exception('Missing parameter: id_especial');
}
if(!empty($parameters->group))
{
    $parameters->success = $parameters->group;
}else{
    $parameters->success = 0;
}
$paramsToEspecial = Administrador_DB::getInfo(Administrador_Especial::TABLE, '*', array('id_especial = '.$id_especial));
$especial = new Administrador_Especial($paramsToEspecial[0]);

$parameters->especial = $especial->getObjectAsArray();

$parameters->buttonsViewTemplate = $twig->loadTemplate('src/module/administrador/general/view/base-buttonsView.html.twig');