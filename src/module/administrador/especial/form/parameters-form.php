<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($_POST['id_especial']))
{
    $id_especial = $_POST['id_especial'];
}else{
    throw new Exception('Missing parameter: id_especial');
}
if($id_especial != '-1')
{
    $paramsToEspecial = Administrador_DB::getInfo(Administrador_Especial::TABLE, '*', array('id_especial = '.$id_especial));
    $especial = new Administrador_Especial($paramsToEspecial[0]);
    $parameters->especial = $especial->getObjectAsArray();
}

$parameters->buttonsFormTemplate = $twig->loadTemplate('src/module/administrador/general/view/base-buttonsForm.html.twig');