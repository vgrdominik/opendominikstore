<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */
$request = json_decode(file_get_contents('php://input'));
if(!empty($parameters->subaction))
{
    $idSlide = $parameters->subaction;
}elseif(!empty($_POST['id_slide']))
{
    $idSlide = $_POST['id_slide'];
}elseif(!empty($_GET['id_slide']))
{
    $idSlide = $_GET['id_slide'];
}elseif(!empty($request->id_slide))
{
    $idSlide = $request->id_slide;
    if(!empty($request->url))
    {
        $_POST['url'] = $request->url;
    }
    if(!empty($request->link))
    {
        $_POST['link'] = $request->link;
    }
    if(!empty($request->descripcio))
    {
        $_POST['descripcio'] = $request->descripcio;
    }
    if(!empty($request->num_slideimatge))
    {
        $_POST['num_slideimatge'] = $request->num_slideimatge;
    }
    if(!empty($request->accio))
    {
        $_POST['accio'] = $request->accio;
    }
}else{
    throw new Exception('Missing parameter: id_slide');
}
if($idSlide != '-1')
{
    $paramsToSlide = Administrador_DB::getInfo(Administrador_Slide::TABLE, '*', array('id_slide = '.$idSlide));
    $imatges = Administrador_DB::getInfo(Administrador_Slide::TABLE_IMATGE, '*', array('id_slide = '.$idSlide));
}else{
    $paramsToSlide = array();
    $paramsToSlide[0] = new stdClass();
    $paramsToSlide[0]->id_slide = $idSlide;
    $imatges = array();
}

if(!empty($_POST['url'])&&!empty($_POST['descripcio']))
{
    $numImatge = count($imatges);
    $imatges[$numImatge] = array();
    $imatges[$numImatge]['url'] = $_POST['url'];
    $imatges[$numImatge]['link'] = $_POST['link'];
    $imatges[$numImatge]['descripcio'] = $_POST['descripcio'];
    $imatges[$numImatge]['num_slideimatge'] = $_POST['num_slideimatge'];
    $imatges[$numImatge]['id_slide'] = '-1';
    $slide = new Administrador_Slide($paramsToSlide[0], $imatges);
    $slide->save();
}elseif(!empty($_POST['accio'])&&$_POST['accio']='eliminar'&&!empty($_POST['num_slideimatge']))
{
    $slide = new Administrador_Slide($paramsToSlide[0], $imatges);
    $slide->deleteImatge($_POST['num_slideimatge']);
    $slide->save();
}else{
    $slide = new Administrador_Slide($paramsToSlide[0], $imatges);
}
$parameters->slide = $slide->getObjectAsArray();