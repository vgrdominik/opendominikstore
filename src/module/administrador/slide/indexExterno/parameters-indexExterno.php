<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($parameters->subaction))
{
    $parameters->success = $parameters->subaction;
}

$arrayToIndex = Administrador_DB::getInfo(Administrador_Slide::TABLE);
$parameters->index = array();

foreach($arrayToIndex as $slide)
{
    $newSlide = new stdClass();
    $imatges = Administrador_DB::getInfo(Administrador_Slide::TABLE_IMATGE, '*', array('id_slide = '.$slide->id_slide));
    $slide = new Administrador_Slide($slide, $imatges);
    $parameters->index[] = $slide->getObjectAsArray();
}