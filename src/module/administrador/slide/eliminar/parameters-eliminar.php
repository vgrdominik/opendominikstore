<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($_POST['id_slide']))
{
    $idSlide = $_POST['id_slide'];
}else{
    throw new Exception('Missing parameter: id_slide');
}

$paramsToSlide = Administrador_DB::getInfo(Administrador_Slide::TABLE, '*', array('id_slide = '.$idSlide));
$imatges = Administrador_DB::getInfo(Administrador_Slide::TABLE_IMATGE, '*', array('id_slide = '.$idSlide));
$slide = new Administrador_Slide($paramsToSlide[0], $imatges);
$success = $slide->delete();

$error = $success->errorInfo();

if(!empty($error[1]))
{
    $success = 2;
}else{
    $success = 1;
}

header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/slide/index.html/'.$success);
exit();