<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($_POST['id_slide']))
{
    $idSlide = $_POST['id_slide'];
}else{
    throw new Exception('Missing parameter: id_slide');
}
if($idSlide != '-1')
{
    $paramsToSlide = Administrador_DB::getInfo(Administrador_Slide::TABLE, '*', array('id_slide = '.$idSlide));
    $imatges = Administrador_DB::getInfo(Administrador_Slide::TABLE_IMATGE, '*', array('id_slide = '.$idSlide));
    $slide = new Administrador_Slide($paramsToSlide[0], $imatges);
    $parameters->slide = $slide->getObjectAsArray();
    $content = array();
    $content['id_slide'] = $idSlide;
    $options = array('http'=>
    array(
        'method'    => 'POST',
        'header'    => 'Content-type: application/x-www-form-urlencoded',
        'content'   => http_build_query($content)
    )
    );
    $context = stream_context_create($options);
    $slideShow = file_get_contents('http://'.$_SERVER['SERVER_NAME'].$parameters->path.'/es/administrador/slideImatge.html', false, $context);
    $parameters->slide['slideshow'] = $slideShow;
}