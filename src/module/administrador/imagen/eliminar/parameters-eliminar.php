<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(empty($_POST['id_imatge']))
{
    throw new Exception('Missing parameter: id_imatge');
}else{
    $idImatge = $_POST['id_imatge'];
}

$paramsToImatge = Administrador_DB::getInfo(Administrador_Imatge::TABLE, '*', array('id_imatge = '.$idImatge));
$imatge = new Administrador_Imatge($paramsToImatge[0]);
$success = $imatge->delete();

$error = $success->errorInfo();

if(!empty($error[1]))
{
    $success = 2;
}else{
    $success = 1;
}

header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/imagen/index.html/'.$success);
exit();