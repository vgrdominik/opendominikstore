<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($_POST['id_imatge']))
{
    $id_imatge = $_POST['id_imatge'];
}else{
    throw new Exception('Missing parameter: id_imatge');
}
if($id_imatge != '-1')
{
    $paramsToImatge = Administrador_DB::getInfo(Administrador_Imatge::TABLE, '*', array('id_imatge = '.$id_imatge));
    $imatge = new Administrador_Imatge($paramsToImatge[0]);
    $parameters->imatge = $imatge->getObjectAsArray();
}

$parameters->buttonsFormTemplate = $twig->loadTemplate('src/module/administrador/general/view/base-buttonsForm.html.twig');