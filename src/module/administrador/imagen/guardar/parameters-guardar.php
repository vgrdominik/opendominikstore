<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($_POST['id_imatge']))
{
    if($_POST['id_imatge'] == -1)
    {
        $success = '2';
    }else{
        $success = '1';
    }
}else{
    throw new Exception('Missing parameter: id_imatge');
}

$imatge = new Administrador_Imatge($_POST);
$imatge->save();

header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/imagen/vista.html/'.$imatge->getIdImatge().'/'.$success);
exit();