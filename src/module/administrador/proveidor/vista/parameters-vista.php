<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($parameters->subaction))
{
    $id_proveidor = $parameters->subaction;
}elseif($_POST['id_proveidor']){
    $id_proveidor = $_POST['id_proveidor'];
}else{
    throw new Exception('Missing parameter: id_proveidor');
}
if(!empty($parameters->group))
{
    $parameters->success = $parameters->group;
}else{
    $parameters->success = 0;
}
$paramsToProveidor = Administrador_DB::getInfo(Administrador_Proveidor::TABLE, '*', array('id_proveidor = '.$id_proveidor));
$proveidor = new Administrador_Proveidor($paramsToProveidor[0]);

$parameters->proveidor = $proveidor->getObjectAsArray();

$parameters->buttonsViewTemplate = $twig->loadTemplate('src/module/administrador/general/view/base-buttonsView.html.twig');