<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($_POST['id_proveidor']))
{
    if($_POST['id_proveidor'] == -1)
    {
        $success = '2';
    }else{
        $success = '1';
    }
}else{
    throw new Exception('Missing parameter: id_proveidor');
}

$textBanner = new Administrador_Proveidor($_POST);
$textBanner->save();

header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/proveidor/vista.html/'.$textBanner->getIdProveidor().'/'.$success);
exit();