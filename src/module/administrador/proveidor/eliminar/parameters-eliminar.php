<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(empty($_POST['id_proveidor']))
{
    throw new Exception('Missing parameter: id_proveidor');
}else{
    $idProveidor = $_POST['id_proveidor'];
}

$paramsToProveidor = Administrador_DB::getInfo(Administrador_Proveidor::TABLE, '*', array('id_proveidor = '.$idProveidor));
$proveidor = new Administrador_Proveidor($paramsToProveidor[0]);
$success = $proveidor->delete();

$error = $success->errorInfo();

if(!empty($error[1]))
{
    $success = 2;
}else{
    $success = 1;
}

header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/proveidor/index.html/'.$success);
exit();