<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 17:59
 * To change this template use File | Settings | File Templates.
 */

if(!empty($_POST['username'])&&!empty($_POST['password']))
{
    $_COOKIE['username'] = $_POST['username'];
    $_COOKIE['password'] = $_POST['password'];

    setcookie('username', $_POST['username'], null, '/');
    setcookie('password', $_POST['password'], null, '/');
}

$parameters->path = substr($_SERVER['PHP_SELF'], 0, -10);

if(!empty($_GET['language']))
{
    $parameters->language = $_GET['language'];
}else{
    $parameters->language = 'es';
}

if(!empty($_GET['action']))
{
    $parameters->action = getAction($_GET['action']);

    if(!(!empty($_COOKIE['username'])&&$_COOKIE['username']=='Administrador'&&
        !empty($_COOKIE['password'])&&$_COOKIE['password']=='123456marxa')&&
        $_GET['action'] != 'administrador/login' &&
        $_GET['action'] != 'administrador/slideImatge')
    {
        Header( "HTTP/1.1 301 Moved Permanently" );
        Header( "Location: ".$parameters->path.'/'.$parameters->language.'/administrador/login.html' );
    }
}else{
    $parameters->action = getAction('administrador/login');
}

if(!empty($_GET['subaction']))
{
    $parameters->subaction = $_GET['subaction'];
}else{
    $parameters->subaction = '';
}

if(!empty($_GET['group']))
{
    $parameters->group = $_GET['group'];
}else{
    $parameters->group = '';
}

include_once 'src/module/administrador/general/data/parameters/'.$parameters->language.'/parameters.php';

$parameters->pathModule = $parameters->path . '/src/module/' . $parameters->action->path;

if(is_file('./src/module/' . $parameters->action->urlParameters))
{
    include ('./src/module/' . $parameters->action->urlParameters);
}
$parameters->menuTemplate = $twig->loadTemplate('src/module/administrador/general/view/base-menu.html.twig');

$parameters->baseTemplateTolanguage = $twig->loadTemplate('src/module/administrador/general/view/base.html.twig');
$parameters->baseTemplate = $twig->loadTemplate('src/module/administrador/general/view/'.$parameters->language.'/base.html.twig');
if(is_file('./src/module/'.$parameters->action->path.'/view/base-'.$parameters->action->nameFile.'.html.twig'))
{
    $parameters->baseModuleTemplate = $twig->loadTemplate('src/module/'.$parameters->action->path.'/view/base-'.$parameters->action->nameFile.'.html.twig');
}

$fecha = time();
$parameters->version = $fecha;