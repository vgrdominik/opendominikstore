<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 19:42
 * To change this template use File | Settings | File Templates.
 */

if($parameters->action->path !== 'login')
{
    $parameters->menu = array(
        'Familia' => array(
            'subfamilia' => null,
            'module' => array(
                'value' => 'administrador/familia/index'
            ),
            'descripcion' => array(
                'value' => 'Família'
            )
        ),
        'Article' => array(
            'subfamilia' => array(
                'Artículo' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/article/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Article'
                    )
                ),
                'Fabricant' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/fabricant/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Fabricant'
                    )
                ),
                'IVA' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/iva/index'
                    ),
                    'descripcion' => array(
                        'value' => 'IVA'
                    )
                ),
                'Nivell' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/nivell/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Nivell'
                    )
                ),
                'Especial' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/especial/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Especial'
                    )
                ),
                'Proveidor' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/proveidor/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Proveïdor'
                    )
                ),
                'FitxaTecnica' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/fitxaTecnica/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Fitxa tècnica'
                    )
                )
            ),
            'module' => array(
                'value' => ''
            ),
            'descripcion' => array(
                'value' => 'Article'
            )
        ),
        'Continguts' => array(
            'subfamilia' => null,
            'module' => array(
                'value' => 'administrador/contingut/index'
            ),
            'descripcion' => array(
                'value' => 'Continguts'
            )
        ),
        'Pagina' => array(
            'subfamilia' => null,
            'module' => array(
                'value' => 'administrador/pagina/index'
            ),
            'descripcion' => array(
                'value' => 'Pàgina'
            )
        ),
        'Promocio' => array(
            'subfamilia' => null,
            'module' => array(
                'value' => 'administrador/promocio/index'
            ),
            'descripcion' => array(
                'value' => 'Promoció'
            )
        ),
        'PaginaPrincipal' => array(
            'subfamilia' => null,
            'module' => array(
                'value' => 'administrador/paginaPrincipal/index'
            ),
            'descripcion' => array(
                'value' => 'Pàgina Principal'
            )
        ),
        'Widgets' => array(
            'subfamilia' => array(
                'Imatge' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/imagen/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Imatge'
                    )
                ),
                'Text' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/textbanner/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Text'
                    )
                ),
                'Slide' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/slide/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Slide'
                    )
                )
            ),
            'module' => array(
                'value' => ''
            ),
            'descripcion' => array(
                'value' => 'Widgets'
            )
        ),

        'Configuracio' => array(
            'subfamilia' => array(
                'Usuari' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/usuario/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Usuari'
                    )
                ),
                'Rol' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/rol/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Rol'
                    )
                ),
                'Modul' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/module/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Mòdul'
                    )
                )
            ),
            'module' => array(
                'value' => ''
            ),
            'descripcion' => array(
                'value' => 'Configuració'
            )
        )
    );
}