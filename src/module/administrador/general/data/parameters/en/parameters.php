<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 19:42
 * To change this template use File | Settings | File Templates.
 */

if($parameters->action->path !== 'login')
{
    $parameters->menu = array(
        'Family' => array(
            'subfamilia' => null,
            'module' => array(
                'value' => 'administrador/familia/index'
            ),
            'descripcion' => array(
                'value' => 'Family'
            )
        ),
        'Item' => array(
            'subfamilia' => array(
                'Item' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/article/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Item'
                    )
                ),
                'Manufacturer' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/fabricant/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Manufacturer'
                    )
                ),
                'IVA' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/iva/index'
                    ),
                    'descripcion' => array(
                        'value' => 'IVA'
                    )
                ),
                'Level' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/nivell/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Level'
                    )
                ),
                'Special' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/especial/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Special'
                    )
                ),
                'Provider' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/proveidor/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Provider'
                    )
                ),
                'Technical' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/fitxaTecnica/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Technical'
                    )
                )
            ),
            'module' => array(
                'value' => ''
            ),
            'descripcion' => array(
                'value' => 'Item'
            )
        ),
        'Content' => array(
            'subfamilia' => null,
            'module' => array(
                'value' => 'administrador/contingut/index'
            ),
            'descripcion' => array(
                'value' => 'Content'
            )
        ),
        'Page' => array(
            'subfamilia' => null,
            'module' => array(
                'value' => 'administrador/pagina/index'
            ),
            'descripcion' => array(
                'value' => 'Page'
            )
        ),
        'Promotion' => array(
            'subfamilia' => null,
            'module' => array(
                'value' => 'administrador/promocio/index'
            ),
            'descripcion' => array(
                'value' => 'Promotion'
            )
        ),
        'HomePage' => array(
            'subfamilia' => null,
            'module' => array(
                'value' => 'administrador/paginaPrincipal/index'
            ),
            'descripcion' => array(
                'value' => 'Home Page'
            )
        ),
        'Widgets' => array(
            'subfamilia' => array(
                'Image' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/imagen/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Image'
                    )
                ),
                'Text' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/textbanner/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Text'
                    )
                ),
                'Slide' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/slide/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Slide'
                    )
                )
            ),
            'module' => array(
                'value' => ''
            ),
            'descripcion' => array(
                'value' => 'Widgets'
            )
        ),

        'Configuration' => array(
            'subfamilia' => array(
                'User' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/usuario/index'
                    ),
                    'descripcion' => array(
                        'value' => 'User'
                    )
                ),
                'Role' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/rol/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Role'
                    )
                ),
                'Module' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/module/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Module'
                    )
                )
            ),
            'module' => array(
                'value' => ''
            ),
            'descripcion' => array(
                'value' => 'Configuration'
            )
        )
    );
}