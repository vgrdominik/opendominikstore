<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 19:42
 * To change this template use File | Settings | File Templates.
 */

if($parameters->action->path !== 'login')
{
    $parameters->menu = array(
        'Familia' => array(
            'subfamilia' => null,
            'module' => array(
                'value' => 'administrador/familia/index'
            ),
            'descripcion' => array(
                'value' => 'Familia'
            )
        ),
        'Articulo' => array(
            'subfamilia' => array(
                'Artículo' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/article/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Artículo'
                    )
                ),
                'Fabricante' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/fabricant/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Fabricante'
                    )
                ),
                'IVA' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/iva/index'
                    ),
                    'descripcion' => array(
                        'value' => 'IVA'
                    )
                ),
                'Nivel' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/nivell/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Nivel'
                    )
                ),
                'Especial' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/especial/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Especial'
                    )
                ),
                'Proveedor' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/proveidor/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Proveedor'
                    )
                ),
                'FichatccnicaTitulos' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/fitxaTecnica/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Ficha técnica'
                    )
                )
            ),
            'module' => array(
                'value' => ''
            ),
            'descripcion' => array(
                'value' => 'Artículo'
            )
        ),
        'Contenidos' => array(
            'subfamilia' => null,
            'module' => array(
                'value' => 'administrador/contingut/index'
            ),
            'descripcion' => array(
                'value' => 'Contenidos'
            )
        ),
        'Pagina' => array(
            'subfamilia' => null,
            'module' => array(
                'value' => 'administrador/pagina/index'
            ),
            'descripcion' => array(
                'value' => 'Página'
            )
        ),
        'Promocion' => array(
            'subfamilia' => null,
            'module' => array(
                'value' => 'administrador/promocio/index'
            ),
            'descripcion' => array(
                'value' => 'Promoción'
            )
        ),
        'PaginaPrincipal' => array(
            'subfamilia' => null,
            'module' => array(
                'value' => 'administrador/paginaPrincipal/index'
            ),
            'descripcion' => array(
                'value' => 'Página Principal'
            )
        ),
        'Widgets' => array(
            'subfamilia' => array(
                'Imagen' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/imagen/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Imagen'
                    )
                ),
                'Texto' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/textbanner/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Texto'
                    )
                ),
                'Slide' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/slide/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Slide'
                    )
                )
            ),
            'module' => array(
                'value' => ''
            ),
            'descripcion' => array(
                'value' => 'Widgets'
            )
        ),

        'Configuracion' => array(
            'subfamilia' => array(
                'Usuario' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/usuario/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Usuario'
                    )
                ),
                'Rol' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/rol/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Rol'
                    )
                ),
                'Modulo' => array(
                    'subfamilia' => null,
                    'module' => array(
                        'value' => 'administrador/module/index'
                    ),
                    'descripcion' => array(
                        'value' => 'Módulo'
                    )
                )
            ),
            'module' => array(
                'value' => ''
            ),
            'descripcion' => array(
                'value' => 'Configuración'
            )
        )
    );
}