<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($parameters->subaction))
{
    $idPagina = $parameters->subaction;
}elseif($_POST['id_pagina']){
    $idPagina = $_POST['id_pagina'];
}else{
    throw new Exception('Missing parameter: id_pagina');
}
if(!empty($parameters->group))
{
    $parameters->success = $parameters->group;
}else{
    $parameters->success = 0;
}
$paramsToPagina = Administrador_DB::getInfo(Administrador_Pagina::TABLE, '*', array('id_pagina = '.$idPagina));
$keywords = Administrador_DB::getInfo(Administrador_Pagina::TABLE_KEYWORDS, '*', array('id_pagina = '.$idPagina));
$pagina = new Administrador_Pagina($paramsToPagina[0], $keywords);

$parameters->pagina = $pagina->getObjectAsArray();
$keywords = '';
foreach($parameters->pagina['keywords'] as $keyword)
{
    $keywords .= ', '.$keyword['descripcio'];
}
$parameters->pagina['keywords'] = trim($keywords, ',');

$parameters->buttonsViewTemplate = $twig->loadTemplate('src/module/administrador/general/view/base-buttonsView.html.twig');