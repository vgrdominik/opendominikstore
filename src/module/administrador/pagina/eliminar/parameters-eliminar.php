<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(empty($_POST['id_pagina']))
{
    throw new Exception('Missing parameter: id_pagina');
}else{
    $idPagina = $_POST['id_pagina'];
}

$paramsToPagina = Administrador_DB::getInfo(Administrador_Pagina::TABLE, '*', array('id_pagina = '.$idPagina));
$keywords = Administrador_DB::getInfo(Administrador_Pagina::TABLE_KEYWORDS, '*', array('id_pagina = '.$idPagina));
$pagina = new Administrador_Pagina($paramsToPagina[0], $keywords);
$success = $pagina->delete();

$error = $success->errorInfo();

if(!empty($error[1]))
{
    $success = 2;
}else{
    $success = 1;
}

header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/pagina/index.html/'.$success);
exit();