<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($_POST['id_pagina']))
{
    if($_POST['id_pagina'] == -1)
    {
        $success = '2';
    }else{
        $success = '1';
    }
}else{
    throw new Exception('Missing parameter: id_pagina');
}
$keywords = explode(',', $_POST['keywords']);
$keywordsToPost = array();
$numKeyword = 0;
foreach($keywords as $keyword)
{
    if(empty($keyword))
    {
        continue;
    }
    $keywordsToPost[$numKeyword] = new stdClass();
    $keywordsToPost[$numKeyword]->num_pagina_keywords = 'NULL';
    $keywordsToPost[$numKeyword]->id_pagina = $_POST['id_pagina'];
    $keywordsToPost[$numKeyword]->descripcio = $keyword;
    $numKeyword++;
}
var_dump($keywordsToPost);
$pagina = new Administrador_Pagina($_POST, $keywordsToPost);
try{
    $pagina->save();
}catch (Exception $e)
{
    header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/pagina/index.html/3');
    exit();
}

header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/pagina/vista.html/'.$pagina->getIdPagina().'/'.$success);
exit();