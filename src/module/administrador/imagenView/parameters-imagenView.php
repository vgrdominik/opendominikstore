<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($parameters->subaction))
{
    $id_imatge = $parameters->subaction;
}elseif(!empty($_POST['id_imatge'])){
    $id_imatge = $_POST['id_imatge'];
}elseif(!empty($_GET['id_imatge'])){
    $id_imatge = $_GET['id_imatge'];
}else{
    throw new Exception('Missing parameter: id_imatge');
}
if(!empty($parameters->group))
{
    $parameters->success = $parameters->group;
}else{
    $parameters->success = 0;
}
$paramsToImatge = Administrador_DB::getInfo(Administrador_Imatge::TABLE, '*', array('id_imatge = '.$id_imatge));
$imatge = new Administrador_Imatge($paramsToImatge[0]);

$parameters->imatge = $imatge->getObjectAsArray();
$parameters->imatge['size'] = getimagesize($parameters->imatge['url']);