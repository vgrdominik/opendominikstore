<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(empty($_POST['id_usuario']))
{
    throw new Exception('Missing parameter: id_usuario');
}else{
    $idUsuario = $_POST['id_usuario'];
}

$paramsToUsuario = Administrador_DB::getInfo(Administrador_Usuario_Usuario::TABLE, '*', array('id_usuario = '.$idUsuario));
$usuario = new Administrador_Usuario_Usuario($paramsToUsuario[0], true);
$success = $usuario->delete();

$error = $success->errorInfo();

$success = 1;

header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/usuario/index.html/'.$success);
exit();