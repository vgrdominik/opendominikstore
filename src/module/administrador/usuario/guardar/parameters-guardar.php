<?php


if(!empty($_POST['id_usuario']))
{
    if($_POST['id_usuario'] == -1)
    {
        $success = '2';
    }else{
        $success = '1';
    }
}else{
    throw new Exception('Missing parameter: id_usuario');
}
if(isset($_POST['activo']) && $_POST['activo']=='on')
{
    $_POST['activo'] = true;
}else{
    $_POST['activo'] = false;
}
if(!empty($_POST['password']))
{
    $_POST['password'] = hash('md5', $_POST['password']);
}
try{
    if(empty($_POST['rolesToUser']))
    {
        $rolesToUser = array();
    }else{
        $rolesToUser = $_POST['rolesToUser'];
    }
    $roles = array();
    foreach($rolesToUser as $idRol => $rolToUser)
    {
        if($rolToUser == 'on')
        {
            $roles[$idRol] = $_POST['roles'][$idRol];
        }
    }
    $_POST['roles'] = $roles;
	$usuario = new Administrador_Usuario_Usuario($_POST);
	$usuario->save();
}catch(Exception $e){
    if($e->getMessage() == 'Missing parameter: roles')
    {
        $success = 4;
    }else{
        $success = 3;
    }
	header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/usuario/index.html/'.$success);
	exit();
}
header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/usuario/vista.html/'.$usuario->getIdUsuario().'/'.$success);
exit();