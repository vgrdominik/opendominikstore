<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($parameters->subaction))
{
    $id_usuario = $parameters->subaction;
}elseif($_POST['id_usuario']){
    $id_usuario = $_POST['id_usuario'];
}else{
    throw new Exception('Missing parameter: id_usuario');
}
if(!empty($parameters->group))
{
    $parameters->success = $parameters->group;
}else{
    $parameters->success = 0;
}
$paramsToUsuario = Administrador_DB::getInfo(Administrador_Usuario_Usuario::TABLE, '*', array('id_usuario = '.$id_usuario));
$paramsToUsuario[0]->roles = array();
$paramsUsuarioRolesToUsuario = Administrador_DB::getInfo(Administrador_Usuario_UsuarioRol::TABLE, '*', array('id_usuario = '.$id_usuario));
foreach($paramsUsuarioRolesToUsuario as $paramsUsuarioRolToUsuario)
{
    $paramsUsuarioRolToUsuario = Administrador_DB::getInfo(Administrador_Usuario_Rol::TABLE, '*', array('id_rol = '.$paramsUsuarioRolToUsuario->id_rol));
    $paramsToUsuario[0]->roles[] = $paramsUsuarioRolToUsuario[0];
}
$paramsRolToUsuario = Administrador_DB::getInfo(Administrador_Usuario_Rol::TABLE, '*', array('id_rol = '.$paramsToUsuario[0]->id_rol));
$usuario = new Administrador_Usuario_Usuario($paramsToUsuario[0], false, $paramsRolToUsuario[0]->rol);

$parameters->usuario = $usuario->getObjectAsArray();

$parameters->buttonsViewTemplate = $twig->loadTemplate('src/module/administrador/general/view/base-buttonsView.html.twig');