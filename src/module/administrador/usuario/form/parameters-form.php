<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($_POST['id_usuario']))
{
    $id_usuario = $_POST['id_usuario'];
}else{
    throw new Exception('Missing parameter: id_usuario');
}
if($id_usuario != '-1')
{
    $paramsToUsuario = Administrador_DB::getInfo(Administrador_Usuario_Usuario::TABLE, '*', array('id_usuario = '.$id_usuario));
    $paramsToUsuario[0]->roles = array();
    $paramsUsuarioRolesToUsuario = Administrador_DB::getInfo(Administrador_Usuario_UsuarioRol::TABLE, '*', array('id_usuario = '.$id_usuario));
    foreach($paramsUsuarioRolesToUsuario as $paramsUsuarioRolToUsuario)
    {
        $paramsUsuarioRolToUsuario = Administrador_DB::getInfo(Administrador_Usuario_Rol::TABLE, '*', array('id_rol = '.$paramsUsuarioRolToUsuario->id_rol));
        $paramsToUsuario[0]->roles[] = $paramsUsuarioRolToUsuario[0];
    }
    $usuario = new Administrador_Usuario_Usuario($paramsToUsuario[0]);
    $parameters->usuario = $usuario->getObjectAsArray();
}

$parameters->roles = Administrador_DB::getInfo(Administrador_Usuario_Rol::TABLE);

$parameters->buttonsFormTemplate = $twig->loadTemplate('src/module/administrador/general/view/base-buttonsForm.html.twig');