<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($parameters->subaction))
{
    $id_module = $parameters->subaction;
}elseif($_POST['id_module']){
    $id_module = $_POST['id_module'];
}else{
    throw new Exception('Missing parameter: id_module');
}
if(!empty($parameters->group))
{
    $parameters->success = $parameters->group;
}else{
    $parameters->success = 0;
}
$paramsToModule = Administrador_DB::getInfo(Administrador_Module::TABLE, '*', array('id_module = '.$id_module));
$paramsRolToModule = Administrador_DB::getInfo(Administrador_Usuario_Rol::TABLE, '*', array('id_rol = '.$paramsToModule[0]->id_rol));
$paramsUsuarioToModule = Administrador_DB::getInfo(Administrador_Usuario_Usuario::TABLE, '*', array('id_usuario = '.$paramsToModule[0]->id_usuario));
$module = new Administrador_Module($paramsToModule[0], $paramsRolToModule[0]->rol, $paramsUsuarioToModule[0]->email);

$parameters->module = $module->getObjectAsArray();

$parameters->buttonsViewTemplate = $twig->loadTemplate('src/module/administrador/general/view/base-buttonsView.html.twig');