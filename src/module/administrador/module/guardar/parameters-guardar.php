<?php


if(!empty($_POST['id_module']))
{
    if($_POST['id_module'] == -1)
    {
        $success = '2';
    }else{
        $success = '1';
    }
}else{
    throw new Exception('Missing parameter: id_module');
}
try{
	$module = new Administrador_Module($_POST);
	$module->save();
}catch(Exception $e){
	header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/module/index.html/3');
	exit();
}
header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/module/vista.html/'.$module->getIdModule().'/'.$success);
exit();