<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(empty($_POST['id_module']))
{
    throw new Exception('Missing parameter: id_module');
}else{
    $idModule= $_POST['id_module'];
}

$paramsToModule = Administrador_DB::getInfo(Administrador_Module::TABLE, '*', array('id_module = '.$idModule));
$module = new Administrador_Module($paramsToModule[0]);
$success = $module->delete();

$error = $success->errorInfo();

$success = 1;

header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/module/index.html/'.$success);
exit();