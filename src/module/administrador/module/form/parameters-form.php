<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($_POST['id_module']))
{
    $id_module = $_POST['id_module'];
}else{
    throw new Exception('Missing parameter: id_module');
}
if($id_module != '-1')
{
    $paramsToModule = Administrador_DB::getInfo(Administrador_Module::TABLE, '*', array('id_module = '.$id_module));
    $module = new Administrador_Module($paramsToModule[0]);
    $parameters->module = $module->getObjectAsArray();
}

$parameters->roles = Administrador_DB::getInfo(Administrador_Usuario_Rol::TABLE, '*');
$parameters->usuarios = Administrador_DB::getInfo(Administrador_Usuario_Usuario::TABLE, '*');

$parameters->buttonsFormTemplate = $twig->loadTemplate('src/module/administrador/general/view/base-buttonsForm.html.twig');