<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($_POST['id_textbanner']))
{
    $id_textbanner = $_POST['id_textbanner'];
}else{
    throw new Exception('Missing parameter: id_textbanner');
}
if($id_textbanner != '-1')
{
    $paramsToTextBanner = Administrador_DB::getInfo(Administrador_TextBanner::TABLE, '*', array('id_textbanner = '.$id_textbanner));
    $imatge = new Administrador_TextBanner($paramsToTextBanner[0]);
    $parameters->textbanner = $imatge->getObjectAsArray();
}

$parameters->buttonsFormTemplate = $twig->loadTemplate('src/module/administrador/general/view/base-buttonsForm.html.twig');