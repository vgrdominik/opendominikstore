<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(!empty($parameters->subaction))
{
    $id_textbanner = $parameters->subaction;
}elseif($_POST['id_textbanner']){
    $id_textbanner = $_POST['id_textbanner'];
}else{
    throw new Exception('Missing parameter: id_textbanner');
}
if(!empty($parameters->group))
{
    $parameters->success = $parameters->group;
}else{
    $parameters->success = 0;
}
$paramsToTextBanner = Administrador_DB::getInfo(Administrador_TextBanner::TABLE, '*', array('id_textbanner = '.$id_textbanner));
$textBanner = new Administrador_TextBanner($paramsToTextBanner[0]);

$parameters->textbanner = $textBanner->getObjectAsArray();

$parameters->buttonsViewTemplate = $twig->loadTemplate('src/module/administrador/general/view/base-buttonsView.html.twig');