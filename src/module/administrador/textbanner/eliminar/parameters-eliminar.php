<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:01
 * To change this template use File | Settings | File Templates.
 */

if(empty($_POST['id_textbanner']))
{
    throw new Exception('Missing parameter: id_textbanner');
}else{
    $idTextBanner = $_POST['id_textbanner'];
}

$paramsToTextBanner = Administrador_DB::getInfo(Administrador_TextBanner::TABLE, '*', array('id_textbanner = '.$idTextBanner));
$textBanner = new Administrador_TextBanner($paramsToTextBanner[0]);
$success = $textBanner->delete();

$error = $success->errorInfo();

if(!empty($error[1]))
{
    $success = 2;
}else{
    $success = 1;
}

header('Location: '.$parameters->path.'/'.$parameters->language.'/administrador/textbanner/index.html/'.$success);
exit();