<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:00
 * To change this template use File | Settings | File Templates.
 */

$familia = Administrador_Familia::getByIdPagina($parameters->id_pagina);

if(!empty($parameters->params['orden'])&&$parameters->params['orden']!='descripcio-ASC')
{
    $ordenArr = explode('-', $parameters->params['orden']);
    $parameters->params['orden'] = array();
    $parameters->params['orden']['descripcion'] = $ordenArr[0];
    $parameters->params['orden']['direccion'] = $ordenArr[1];
    $orden = array(
        addslashes($parameters->params['orden']['descripcion']) =>
        addslashes($parameters->params['orden']['direccion'])
    );
}else{
    $orden = array('descripcio' => 'ASC');

}
if(empty($parameters->params)||(!empty($parameters->params)&&count($parameters->params)==1&&!empty($parameters->params['pagina'])))
{
    // Esta variable también nos dice si poner los parametros en las urls que podrían ser indexadas (Por ejemplo en las páginas)
    $parameters->noindex = false;
}else{
    $parameters->noindex = true;
}
if(empty($parameters->params['orden'])||$parameters->params['orden']=='descripcio-ASC')
{
    $parameters->params['orden'] = array();
    $parameters->params['orden']['descripcion'] = 'descripcio';
    $parameters->params['orden']['direccion'] = 'ASC';
}
if(empty($parameters->params['limite']))
{
    $limite = 16;
}else{
    $limite = $parameters->params['limite'];
}
if(empty($parameters->params['nuevo']))
{
    $parameters->params['nuevo'] = 0;
    $nuevo = 0;
}else{
    $nuevo = $parameters->params['nuevo'];
}
if(empty($parameters->params['actualizado']))
{
    $parameters->params['actualizado'] = 0;
    $actualizado = 0;
}else{
    $actualizado = $parameters->params['actualizado'];
}
if(empty($parameters->params['precioRango']))
{
    $parameters->params['precioRango'] = array();
    $precioRango = array();
}else{
    $precioRangoArr = explode('-', $parameters->params['precioRango']);
    $parameters->params['precioRango'] = array();
    $parameters->params['precioRango']['min'] = $precioRangoArr[0];
    $parameters->params['precioRango']['max'] = $precioRangoArr[1];
    $precioRango = array(
        addslashes($parameters->params['precioRango']['min']),
        addslashes($parameters->params['precioRango']['max'])
    );
}
if(empty($parameters->params['tipo']))
{
    $parameters->tipoArticulo = 'contraido';
    $parameters->maxHeightArticle = 200;
    $parameters->widthArticle = 150;
}else{
    $parameters->tipoArticulo = 'extendido';
    $parameters->maxHeightArticle = 200;
    $parameters->widthArticle = 200;
}
if(empty($parameters->params['pagina']))
{
    $pagina = 1;
}else{
    $pagina = $parameters->params['pagina'];
}
$articles = $familia->getIndexArticles(
    'http://'.$_SERVER['SERVER_NAME'].'/'.$parameters->path,
    $pagina,
    $limite,
    $orden,
    $parameters->maxHeightArticle,
    $parameters->widthArticle,
    $nuevo,
    $actualizado,
    $precioRango
);
$parameters->articles = $articles->articles;
$parameters->filtres = $articles->filtres;
if(empty($parameters->params['precioRango']))
{
    $parameters->params['precioRango']['min'] = $parameters->filtres['minPrecio'];
    $parameters->params['precioRango']['max'] = $parameters->filtres['maxPrecio'];
}
$productosInicio = $pagina * $limite - $limite;
$parameters->productosInicio = $productosInicio + 1;
$productosFin = $productosInicio + $limite;
$totalProductos = $articles->countArticles;
$parameters->productosFin = ($productosFin>$totalProductos)? $totalProductos : $productosFin;
$parameters->productosTotal = $totalProductos;

$totalPaginasArr = explode('.', $totalProductos/$limite);
$totalPaginas = $totalPaginasArr[0];
if(!empty($totalPaginasArr[1])||$totalPaginas<=0)
{
    $totalPaginas++;
}

if($pagina>$totalPaginas||
    ($limite!=16&&$limite!=32&&$limite!=52)||
    (empty($orden['descripcio'])&&empty($orden['precio']))||
    (!empty($parameters->params['pagina'])&&$parameters->params['pagina']==1)||
    (!empty($parameters->params['tipo'])&&$parameters->params['tipo']!='extendido')
)
{
    Header( "HTTP/1.1 301 Moved Permanently" );
    Header( "Location: ".$parameters->path.'/'.$parameters->language.'/'.$parameters->descripcio_amigable.'.html' );
}

if($parameters->noindex === false)
{
    $paramsUrl = '';
}else{
    $paramsUrl = 'orden/'.$parameters->params['orden']['descripcion'].'-'.$parameters->params['orden']['direccion'].'/'.
        'limite/'.$limite.'/precioRango/'.$parameters->params['precioRango']['min'].'-'.$parameters->params['precioRango']['max'].'/'.
        'nuevo/'.$nuevo.'/actualizado/'.$actualizado.'/';
    if($parameters->tipoArticulo != 'contraido')
    {
        $paramsUrl .= 'tipo/'.$parameters->tipoArticulo.'/';
    }
}

//PAGINADOR
$parameters->paginas = array();
// Cargamos la primera página (El número no hace falta porqué siempre es 1
$parameters->primeraPagina = $parameters->path.'/'.$parameters->language.'/'.$paramsUrl.$parameters->descripcio_amigable.'.html';
// Cargamos la página anterior y su número
$parameters->numeroPaginaAnterior = ($pagina == 1)? 1 : $pagina - 1;
if($parameters->numeroPaginaAnterior == 1)
{
    $parameters->paginaAnterior = $parameters->path.'/'.$parameters->language.'/'.$paramsUrl.$parameters->descripcio_amigable.'.html';
}else{
    $parameters->paginaAnterior = $parameters->path.'/'.$parameters->language.'/'.$paramsUrl.'pagina/'.$parameters->numeroPaginaAnterior.'/'.$parameters->descripcio_amigable.'.html';
}

// Cargamos las páginas a mostrar a parte de la primera y la última que siempre se muestran
$numPaginasPorBanda = 2;
if(2 > $pagina - $numPaginasPorBanda)
{
    $primeraPaginaPaginador = 2;
}else{
    $primeraPaginaPaginador = $pagina - $numPaginasPorBanda;
}
if($totalPaginas <= $pagina + $numPaginasPorBanda)
{
    $countPaginas = $totalPaginas - 1;
}else{
    $countPaginas = $pagina + $numPaginasPorBanda;
}
for($i=$primeraPaginaPaginador; $i<=$countPaginas; $i++)
{
    $parameters->paginas[] = $parameters->path.'/'.$parameters->language.'/'.$paramsUrl.'pagina/'.$i.'/'.$parameters->descripcio_amigable.'.html';
}
// Cargamos la página siguiente y su número
$parameters->numeroPaginaSiguiente = ($pagina == $totalPaginas)? $totalPaginas : $pagina + 1;
$parameters->paginaSiguiente = $parameters->path.'/'.$parameters->language.'/'.$paramsUrl.'pagina/'.$parameters->numeroPaginaSiguiente.'/'.$parameters->descripcio_amigable.'.html';
// Cargamos el número de la  página actual
$parameters->numeroPaginaActual = $pagina;
// Cargamos la última página y su número
$parameters->numeroPaginaUltima = $totalPaginas;
$parameters->ultimaPagina = $parameters->path.'/'.$parameters->language.'/'.$paramsUrl.'pagina/'.$parameters->numeroPaginaUltima.'/'.$paramsUrl.$parameters->descripcio_amigable.'.html';
//Cargamos el número de la última página del paginador
$parameters->numeroUltimaPaginaDelPaginador = $countPaginas;
//Cargamos el número de la primera página del paginador
$parameters->numeroPrimeraPaginaDelPaginador = $primeraPaginaPaginador;
// FIN PAGINDOR