<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:00
 * To change this template use File | Settings | File Templates.
 */

$paginaPrincipal = Administrador_IndexPaginaPrincipal::getByIdPagina($parameters->id_pagina);

$parameters->paginaPrincipal = $paginaPrincipal->getIndexOrdered();