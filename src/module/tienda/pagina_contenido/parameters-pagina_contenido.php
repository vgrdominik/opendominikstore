<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 08/11/13
 * Time: 09:55
 * To change this template use File | Settings | File Templates.
 */

$paramsToPaginaContenido = Administrador_DB::getInfo(Administrador_Contingut::TABLE, '*', array('id_pagina = '.$parameters->id_pagina));
$paginaContenido = new Administrador_Contingut($paramsToPaginaContenido[0]);

$parameters->paginaContenido = $paginaContenido->getObjectAsArray();