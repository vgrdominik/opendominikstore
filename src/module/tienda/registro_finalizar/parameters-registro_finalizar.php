<?php
$_POST['activo'] = false;
if(!empty($_POST['password']))
{
    $_POST['password'] = hash('md5', $_POST['password']);
}
try{
    $roles = array();
    $roles[2]['id_rol'] = 2;
    $roles[2]['rol'] = 'Cliente';
    $_POST['roles'] = $roles;
    $usuario = new Administrador_Usuario_Usuario($_POST);
    $usuario->save();
}catch(Exception $e){
    if($e->getMessage() == 'Missing parameter: roles')
    {
        $success = 4;
    }else{
        $success = 3;
    }
    header('Location: '.$parameters->path.'/'.$parameters->language.'/registro.html/'.$success.'');
    exit();
}

require '/src/lib/phpmailer/class.phpmailer.php';

try {
    $mail = new PHPMailer(true); //New instance, with exceptions enabled

    $body             = '<html><body>Haz clcik <a href="http://marxa.es/es/registro_activado.html/codigo/'.$_POST['password'].'">aquí</a> para confirmar tu alta como cliente de Marxa.</body></html>';
    $body             = preg_replace('/\\\\/','', $body); //Strip backslashes

    $mail->IsSMTP();                           // tell the class to use SMTP
    $mail->SMTPAuth   = true;                  // enable SMTP authentication
    $mail->Port       = 25;                    // set the SMTP server port
    $mail->Host       = "mail.marxa.es"; // SMTP server
    $mail->Username   = "marxa@marxa.es";     // SMTP server username
    $mail->Password   = "rzvukzz";            // SMTP server password

    $mail->IsSendmail();  // tell the class to use Sendmail

    //$mail->AddReplyTo("name@domain.com","First Last");

    $mail->From       = "marxa@marxa.es";
    $mail->FromName   = "Administración de Marxa";

    $to = $usuario->getEmail();

    $mail->AddAddress($to);

    $mail->Subject  = "Confirmación de alta como cliente de Marxa";

    //$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
    //$mail->WordWrap   = 80; // set word wrap

    $mail->MsgHTML($body);

    $mail->IsHTML(true); // send as HTML

    $mail->Send();
} catch (phpmailerException $e) {
    $success = 5;
    header('Location: '.$parameters->path.'/'.$parameters->language.'/registro.html/'.$success.'');
}

header('Location: '.$parameters->path.'/'.$parameters->language.'/home.html/'.$success.'');
exit();