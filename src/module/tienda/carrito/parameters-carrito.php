<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 18:00
 * To change this template use File | Settings | File Templates.
 */

$parameters->urlCodificada = urlencode($parameters->path.'/'.$parameters->language.'/'.$parameters->descripcio_amigable.'.html');

if(empty($_POST['id_article'])||empty($_POST['quantitat']))
{
    $id_article = null;
    $quantitat = null;
}else{
    $id_article = $_POST['id_article'];
    $quantitat = $_POST['quantitat'];

    if($facturesTemp === null)
    {
        $article = Administrador_Article::getById($id_article);
        $params = new stdClass();
        $params->id_facturestemp = -1;
        $params->codi_sessio = $idSession;
        $params->data = date('Y-m-d H:i:s');
        $params->total = $article->getPrecio() * $quantitat;
        $index = array();
        $facturesTemp = new Administrador_FacturesTemp($params, $index);
        $facturesTemp->save();
    }

    if(!empty($_POST['accion'])&&$_POST['accion']=='eliminar')
    {
        if(!empty($_POST['id_facturestemparticle']))
        {
            $facturesTemp->deleteIndexElement($_POST['id_facturestemparticle']);
            $facturesTemp->save();
        }
    }else{
        if(!empty($_POST['id_facturestemparticle']))
        {
            $facturesTemp->saveElement($facturesTemp->getIdFacturesTemp(), $_POST['id_facturestemparticle'], $quantitat, $id_article);
        }else{
            $facturesTemp->saveElement($facturesTemp->getIdFacturesTemp(), null, $quantitat, $id_article);
        }
    }


    $facturesTemp->refresh();
}

if($facturesTemp !== null)
{
    $parameters->factura = $facturesTemp->getObjectAsArray($parameters->path);
}

if(!empty($_POST['url_anterior']))
{
    $parameters->urlAnterior = array();
    $count = 0;
    foreach($_POST['url_anterior'] as $url_anterior)
    {
        $arrUrlAnteriror = explode(';', $url_anterior);

        $parameters->urlAnterior[$count] = array();
        $parameters->urlAnterior[$count]['descripcion'] = $arrUrlAnteriror[0];
        $parameters->urlAnterior[$count]['url'] = $arrUrlAnteriror[1];

        $count++;
    }
}
