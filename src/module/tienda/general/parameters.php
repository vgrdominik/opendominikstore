<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 29/04/13
 * Time: 17:59
 * To change this template use File | Settings | File Templates.
 */

if(!empty($_POST['email'])&&!empty($_POST['password']))
{
    $paramsUsuario = Administrador_DB::getInfo(Administrador_Usuario_Usuario::TABLE, '*', array(
        'email = "'.$_POST['email'].'"',
        'password = "'.hash('md5', $_POST['password']).'"'
    ));

    if (!empty($paramsUsuario[0])) {
        $numRand = hash('md5', rand(5000, 9999999999));
        setcookie('email', $_POST['email']);
        setcookie('num_aleatorio', $numRand);

        $modificacionUsuario = Administrador_DB::update(Administrador_Usuario_Usuario::TABLE, array(
            'cookie' => $numRand
        ), array(
            'email = "'.$_POST['email'].'"'
        ));

        $usuario = new Administrador_Usuario_Usuario($paramsUsuario[0], true);
    }
}

if(!empty($_COOKIE['email'])&&isset($_COOKIE['num_aleatorio']))
{
    $paramsUsuario = Administrador_DB::getInfo(Administrador_Usuario_Usuario::TABLE, '*', array(
        'email = "'.$_COOKIE['email'].'"',
        'cookie = "'.$_COOKIE['num_aleatorio'].'"'
    ));

    if (!empty($paramsUsuario[0])) {
        $usuario = new Administrador_Usuario_Usuario($paramsUsuario[0], true);
    }
}

if(!empty($usuario))
{
    $parameters->usuario = $usuario->getObjectAsArray();
}

$descripcioAmigable = explode('/', $_GET['action']);
$parameters->descripcio_amigable = array_pop($descripcioAmigable);
$paginaActual = Administrador_DB::getInfo('Pagina', '*', array('descripcio_amigable = "'.$parameters->descripcio_amigable.'"'));
$keywords = Administrador_DB::getInfo('PaginaKeywords', '*', array('id_pagina = "'.$paginaActual[0]->id_pagina.'"'));
$parameters->id_pagina = $paginaActual[0]->id_pagina;
$parameters->title = $paginaActual[0]->titol;
$parameters->descripcio = $paginaActual[0]->descripcio;
$parameters->keywords = $keywords;
$_GET['action'] = $paginaActual[0]->url;
$parameters->url = $paginaActual[0]->url;

$parameters->path = substr($_SERVER['PHP_SELF'], 0, -10);

session_start();
$idSession = session_id();
$facturesTemp = Administrador_FacturesTemp::getByCodiSessio($idSession);
if($facturesTemp!== null)
{
    $parameters->factura = $facturesTemp->getObjectAsArray($parameters->path);
}

if($_GET['action'] == 'tienda/cerrar')
{
    setcookie('email', '');
    setcookie('num_aleatorio', '');

    Header( "Location: home.html" );
    exit();
}

if($_GET['action'] == 'tienda/articulo')
{
    $article = Administrador_Article::getByIdPagina($parameters->id_pagina);
    $parameters->article = $article->getObjectAsArray();
    if(!empty($parameters->article['id_fabricant']))
    {
        $fabricant = Administrador_DB::getInfo('Fabricant', '*', array('id_fabricant = "'.$parameters->article['id_fabricant'].'"'));
        $fabricant = new Administrador_Fabricant($fabricant[0]);
        $parameters->article['fabricant'] = $fabricant->getObjectAsArray();
    }
    if(empty($parameters->article['imatge']))
    {
        $size = false;
    }else{
        $size = @getimagesize($parameters->article['imatge']);
    }
    if($size === false)
    {
        $parameters->article['imatge'] = $parameters->path.'/src/module/tienda/general/img/imagen_no_disponible.jpg';
    }
}else{
    $article = null;
}

$parameters->params = array();
for($i=0; $i<count($descripcioAmigable); $i+=2)
{
    $parameters->params[$descripcioAmigable[$i]] = $descripcioAmigable[$i+1];
}

$parameters->action = getAction($_GET['action']);

if(!empty($_GET['subaction']))
{
    $parameters->subaction = $_GET['subaction'];
}else{
    $parameters->subaction = '';
}

if(!empty($_GET['group']))
{
    $parameters->group = $_GET['group'];
}else{
    $parameters->group = '';
}

if(!empty($_GET['language']))
{
    $parameters->language = $_GET['language'];
}else{
    $parameters->language = 'es';
}

include_once 'src/module/tienda/general/data/parameters/'.$parameters->language.'/parameters.php';

$parameters->pathModule = $parameters->path . '/src/module/' . $parameters->action->path;

$parameters->menu = Administrador_Familia::getMenu();
foreach($parameters->menu as $key => $family)
{
    if($parameters->menu[$key]['module']['value'] == $parameters->descripcio_amigable||
        ($article !== null&&$article->getIdFamilia()==$family['id_familia']))
    {
        $parameters->menu[$key]['active'] = true;
        $parameters->currentFamily = $family;
        $parameters->currentSubfamily = false;
    }else{
        $parameters->menu[$key]['active'] = false;
    }
    foreach($parameters->menu[$key]['subfamilia'] as $key2 => $subfamily)
    {
        if($subfamily['module']['value'] == $parameters->descripcio_amigable||
            ($article !== null&&$article->getIdFamilia()==$subfamily['id_familia']))
        {
            $parameters->menu[$key]['subfamilia'][$key2]['active'] = true;
            $parameters->menu[$key]['active'] = true;
            $parameters->currentFamily = $family;
            $parameters->currentSubfamily = $subfamily;
        }else{
            $parameters->menu[$key]['subfamilia'][$key2]['active'] = false;
        }
    }

    $arrDescripcion = explode(' ', $family['descripcion']['value']);
    $parameters->menu[$key]['descripcion']['value'] = $arrDescripcion[0];
    unset($arrDescripcion[0]);
    $parameters->menu[$key]['descripcion']['optional'] = join(' ', $arrDescripcion);
}

if(is_file('./src/module/' . $parameters->action->urlParameters))
{
    include ('./src/module/' . $parameters->action->urlParameters);
}
$parameters->menuTemplate = $twig->loadTemplate('src/module/tienda/general/view/base-menu.html.twig');

$parameters->baseTemplate = $twig->loadTemplate('src/module/tienda/general/view/base.html.twig');
if(is_file('./src/module/'.$parameters->action->path.'/view/base-'.$parameters->action->nameFile.'.html.twig'))
{
    $parameters->baseModuleTemplate = $twig->loadTemplate('src/module/'.$parameters->action->path.'/view/base-'.$parameters->action->nameFile.'.html.twig');
}

$fecha = time();
$parameters->version = $fecha;