<?php

/* module/general/view/base-menu.html.twig */
class __TwigTemplate_f6e1aa8584e21b2fd1fe65669dc499de extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'titleMenu' => array($this, 'block_titleMenu'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<ul class=\"nav nav-tabs nav-stacked well\" style=\"max-width: 340px; padding: 8px 0;\">
<li class=\"nav-header\">";
        // line 2
        $this->displayBlock('titleMenu', $context, $blocks);
        echo "</li>
";
        // line 3
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["parameters"]) ? $context["parameters"] : null), "menu"));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 4
            $context["numSubItems"] = twig_length_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : null), "subfamilia"));
            // line 5
            echo "<li ";
            if (((isset($context["numSubItems"]) ? $context["numSubItems"] : null) > 0)) {
                echo "class=\"dropdown\"";
            }
            echo ">
    <a href=\"";
            // line 6
            if (($this->getAttribute($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "module"), "value") != "")) {
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["parameters"]) ? $context["parameters"] : null), "path"), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["parameters"]) ? $context["parameters"] : null), "language"), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "module"), "value"), "html", null, true);
                echo ".html";
            } else {
                echo "#";
            }
            echo "\"";
            if (((isset($context["numSubItems"]) ? $context["numSubItems"] : null) > 0)) {
                echo "class=\"dropdown-toggle\" data-toggle=\"dropdown\"";
            }
            echo ">
        ";
            // line 7
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "descripcion"), "value"), "html", null, true);
            echo " ";
            if (((isset($context["numSubItems"]) ? $context["numSubItems"] : null) > 0)) {
                echo "<b class=\"caret\">";
            }
            echo "</b>
    </a>
    ";
            // line 9
            if (((isset($context["numSubItems"]) ? $context["numSubItems"] : null) > 0)) {
                // line 10
                echo "        ";
                echo twig_escape_filter($this->env, twig_include($this->env, $context, "/module/general/view/base-menu-subfamilies.html.twig", array("item" => (isset($context["item"]) ? $context["item"] : null), "numSubItems" => (isset($context["numSubItems"]) ? $context["numSubItems"] : null))), "html", null, true);
                echo "
    ";
            }
            // line 12
            echo "</li>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 14
        echo "</ul>";
    }

    // line 2
    public function block_titleMenu($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "module/general/view/base-menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 2,  81 => 14,  74 => 12,  68 => 10,  66 => 9,  57 => 7,  40 => 6,  33 => 5,  31 => 4,  27 => 3,  23 => 2,  20 => 1,);
    }
}
