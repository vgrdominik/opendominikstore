<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 3/08/13
 * Time: 10:14
 * To change this template use File | Settings | File Templates.
 */

class TemplateExtension_Project  extends Twig_Extension
{
    public function getName()
    {
        return 'project';
    }

    public function getFunctions()
    {
        return array(
            new Twig_SimpleFunction('debug', 'debug'),
        );
    }
}