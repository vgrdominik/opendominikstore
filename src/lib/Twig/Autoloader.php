<?php

/*
 * This file is part of Twig.
 *
 * (c) 2009 Fabien Potencier
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Autoloads Twig classes.
 *
 * @package twig
 * @author  Fabien Potencier <fabien@symfony.com>
 */
class Twig_Autoloader
{
    /**
     * Registers Twig_Autoloader as an SPL autoloader.
     */
    public static function register()
    {
        ini_set('unserialize_callback_func', 'spl_autoload_call');
        spl_autoload_register(array(new self, 'autoload'));
    }

    /**
     * Handles autoloading of classes.
     *
     * @param string $class A class name.
     */
    public static function autoload($class)
    {
        if (0 !== strpos($class, 'Twig')) {
            $classNameArray = explode('_', strrev($class), 2);
            $classNameArray[0] = strrev($classNameArray[0]);
            if(!empty($classNameArray[1]))
            {
                $classNameArray[1] = strrev($classNameArray[1]);
            }
            if(!empty($classNameArray[1]))
            {
                $fileName = strtolower(str_replace('_', '/', $classNameArray[1])).'/'.$classNameArray[0];
            }else{
                $fileName = $classNameArray[0];
            }
            $extension = '';
            if(is_file('src/lib/' . $fileName . '.class.php'))
            {
                $extension = 'class';
            }elseif(is_file('src/lib/' . $fileName . '.interface.php'))
            {
                $extension = 'interface';
            }

            if(!empty($extension))
            {
                $path = 'src/lib/' . $fileName . '.' . $extension . '.php';
                require_once $path;
            }else{
                $path = 'src/lib/' . $fileName . '.php';
                if(is_file($path))
                {
                    require_once $path;
                }
            }
            return;
        }

        if (is_file($file = dirname(__FILE__).'/../'.str_replace(array('_', "\0"), array('/', ''), $class).'.php')) {
            require $file;
        }
    }
}
