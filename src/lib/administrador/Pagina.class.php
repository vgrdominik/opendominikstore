<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 3/08/13
 * Time: 12:55
 * To change this template use File | Settings | File Templates.
 */

class Administrador_Pagina implements Administrador_SerializableObject {
    const TABLE = 'Pagina';
    const TABLE_KEYWORDS = 'PaginaKeywords';
    private $keywords;
    private $titol;
    private $descripcio;
    private $ultima_modificacio;
    private $descripcio_amigable;
    private $url;
    private $id_pagina;

    public function __construct($parameters, $keywords)
    {
        if(is_array($parameters))
        {
            $newParameters = new stdClass();
            foreach($parameters as $key => $parameter)
            {
                $newParameters->$key = $parameter;
            }
            $parameters = $newParameters;
        }

        if(isset($parameters->titol))
        {
            $this->setTitol($parameters->titol);
        }else{
            $this->setTitol(null);
        }

        if(isset($parameters->id_pagina))
        {
            $this->setIdPagina($parameters->id_pagina);
            if(!empty($keywords))
            {
                $this->setKeywords($keywords);
            }else{
                $this->setKeywords(array());
            }

        }else{
            throw new Exception('Missing parameter: id_pagina');
        }

        if(isset($parameters->descripcio))
        {
            $this->setDescripcio($parameters->descripcio);
        }else{
            throw new Exception('Missing parameter: descripcio');
        }

        if(isset($parameters->ultima_modificacio))
        {
            if(empty($parameters->ultima_modificacio)&&$this->getIdPagina()==-1)
            {
                $this->setUltimaModificacio(date('Y-m-d H:i:s'));
            }else{
                $this->setUltimaModificacio($parameters->ultima_modificacio);
            }
        }else{
            throw new Exception('Missing parameter: alta');
        }

        if(isset($parameters->descripcio_amigable))
        {
            $this->setDescripcioAmigable($parameters->descripcio_amigable);
        }else{
            throw new Exception('Missing parameter: descripcio_amigable');
        }

        if(isset($parameters->url))
        {
            $this->setUrl($parameters->url);
        }else{
            throw new Exception('Missing parameter: url');
        }
    }

    public static function getById($id, $keywords = false)
    {
        $pagina = Administrador_DB::getInfo(Administrador_Pagina::TABLE, '*', array('id_pagina = '.$id));
        if($keywords === false)
        {
            $keywords = array();
        }else{
            $keywords = self::getKeywordsFromDB($id);
        }
        return new Administrador_Pagina($pagina[0], $keywords);
    }

    public static function getDescripcioAmigableById($id)
    {
        $pagina = Administrador_DB::getInfo(Administrador_Pagina::TABLE, '*', array('id_pagina = '.$id));
        return $pagina[0]->descripcio_amigable;
    }

    public static function getKeywordsFromDB($idPagina)
    {
        $keyrowds = Administrador_DB::getInfo(Administrador_Pagina::TABLE_KEYWORDS, '*', array('id_pagina = '.$idPagina));
        return $keyrowds;
    }

    public function getKeywords()
    {
        return $this->keywords;
    }

    public function setKeywords($keywords)
    {
        $arrayKeywords = array();
        $numKeyrowd = 0;
        foreach($keywords as $keyword)
        {
            if(is_array($keyword))
            {
                $newParameters = new stdClass();
                foreach($keyword as $key => $parameter)
                {
                    $newParameters->$key = $parameter;
                }
                $keyword = $newParameters;
            }

            if(empty($keyword->descripcio))
            {
                continue;
            }

            if(!isset($keyword->num_pagina_keywords))
            {
                debug($keyword->num_pagina_keywords);
                throw new Exception('Missing num_pagina_keywords');
            }

            if(empty($keyword->num_pagina_keywords) AND $keyword->num_pagina_keywords !== null)
            {
                throw new Exception('num_pagina_keyword can not be empty');
            }
            if($keyword->num_pagina_keywords == 'NULL')
            {
                $keyword->num_pagina_keywords = null;
                $numKeyrowd++;
            }else{
                $numKeyrowd = $keyword->num_pagina_keywords;
            }

            $arrayKeywords[$numKeyrowd]['descripcio'] = $keyword->descripcio;
            $arrayKeywords[$numKeyrowd]['num_pagina_keywords'] = $keyword->num_pagina_keywords;
            $arrayKeywords[$numKeyrowd]['id_pagina'] = $keyword->id_pagina;
        }
        return $this->keywords = $arrayKeywords;
    }

    public function getTitol()
    {
        return $this->titol;
    }

    public function setTitol($titol)
    {
        return $this->titol = $titol;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        return $this->url = $url;
    }

    public function getIdPagina()
    {
        return $this->id_pagina;
    }

    public function setIdPagina($id_pagina)
    {
        return $this->id_pagina = $id_pagina;
    }

    public function getDescripcio()
    {
        return $this->descripcio;
    }

    public function setDescripcio($descripcio)
    {
        return $this->descripcio = $descripcio;
    }

    public function getUltimaModificacio()
    {
        return $this->ultima_modificacio;
    }

    public function setUltimaModificacio($ultimaModificacio)
    {
        return $this->ultima_modificacio = $ultimaModificacio;
    }

    public function getDescripcioAmigable()
    {
        return $this->descripcio_amigable;
    }

    public function setDescripcioAmigable($descripcioAmigable)
    {
        return $this->descripcio_amigable = $descripcioAmigable;
    }

    public function save()
    {
        if($this->getIdPagina()!= -1)
        {
            $pdo = Administrador_DB::update(Administrador_Pagina::TABLE, $this->getObjectAsArrayOnlyPagina(), array('id_pagina = '.$this->getIdPagina()));
            $keywords = $this->getObjectAsArrayOnlyKeywords();
            $pdo = Administrador_DB::delete(Administrador_Pagina::TABLE_KEYWORDS, array('id_pagina = '.$this->getIdPagina()));
            foreach($keywords['keywords'] as $keyword)
            {
                if($keyword['id_pagina'] == -1)
                {
                    $keyword['id_pagina'] = $this->getIdPagina();
                }
                $pdo = Administrador_DB::insert(Administrador_Pagina::TABLE_KEYWORDS, $keyword);
            }
            return $this;
        }else{
            $pdo = Administrador_DB::insert(Administrador_Pagina::TABLE, $this->getObjectAsArrayOnlyPagina());
            $idPagina = $pdo->lastInsertId();
            $keywords = $this->getObjectAsArrayOnlyKeywords();
            foreach($keywords['keywords'] as $keyword)
            {
                if($keyword['id_pagina'] == -1)
                {
                    $keyword['id_pagina'] = $idPagina;
                }
                $pdo = Administrador_DB::insert(Administrador_Pagina::TABLE_KEYWORDS, $keyword);
            }
            $paramsToPagina = Administrador_DB::getInfo(Administrador_Pagina::TABLE, '*', array('id_pagina = '.$idPagina));
            $keywords = Administrador_DB::getInfo(Administrador_Pagina::TABLE_KEYWORDS, '*', array('id_pagina = '.$idPagina));
            if(!isset($paramsToPagina[0]))
            {
                throw new Exception('Error to create new pagina');
            }
            $newContingut = new Administrador_Pagina($paramsToPagina[0], $keywords);
            $this->setAllParamsFromDB($newContingut);
            return $this;
        }

    }

    public function delete()
    {
        //$pdo = Administrador_DB::delete('PaginaKeywords', array('id_pagina = '.$this->getIdPagina()));
        return Administrador_DB::delete(Administrador_Pagina::TABLE, array('id_pagina = '.$this->getIdPagina()));
    }

    public function setAllParamsFromDB(Administrador_Pagina $newParams)
    {
        $this->setKeywords($newParams->getKeywords());
        $this->setTitol($newParams->getTitol());
        $this->setUrl($newParams->getUrl());
        $this->setIdPagina($newParams->getIdPagina());
        $this->setDescripcio($newParams->getDescripcio());
        $this->setUltimaModificacio($newParams->getUltimaModificacio());
        $this->setDescripcioAmigable($newParams->getDescripcioAmigable());
    }

    public function getObjectAsArray()
    {
        $return = array();
        $return['keywords'] = $this->getKeywords();
        $return['titol'] = $this->getTitol();
        $return['id_pagina'] = ($this->getIdPagina() == -1)? null : $this->getIdPagina();
        $return['descripcio'] = $this->getDescripcio();
        $return['url'] = $this->getUrl();
        $return['ultima_modificacio'] = $this->getUltimaModificacio();
        $return['descripcio_amigable'] = $this->getDescripcioAmigable();

        return $return;
    }

    public function getObjectAsArrayOnlyPagina()
    {
        $return = array();
        $return['titol'] = $this->getTitol();
        $return['id_pagina'] = ($this->getIdPagina() == -1)? null : $this->getIdPagina();
        $return['descripcio'] = $this->getDescripcio();
        $return['url'] = $this->getUrl();
        $return['ultima_modificacio'] = $this->getUltimaModificacio();
        $return['descripcio_amigable'] = $this->getDescripcioAmigable();

        return $return;
    }

    public function getObjectAsArrayOnlyKeywords()
    {
        $return = array();
        $return['keywords'] = $this->getKeywords();

        return $return;
    }
}