<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 3/08/13
 * Time: 12:55
 * To change this template use File | Settings | File Templates.
 */

class Administrador_Article implements Administrador_SerializableObject {
    const TABLE = 'Article';
    private $id_article;
    private $id_familia;
    private $id_nivell;
    private $id_pagina;
    private $id_fabricant;
    private $id_proveidor;
    private $id_iva;
    private $descripcio;
    private $plac;
    private $imatge;
    private $precio;
    private $referencia_propia;
    private $referencia_proveidor;
    private $oferta_desde;
    private $oferta_fins;
    private $preu_oferta;
    private $stock;
    private $alta;
    private $actualitzacio;
    private $informacio;

    public function __construct($parameters)
    {
        if(is_array($parameters))
        {
            $newParameters = new stdClass();
            foreach($parameters as $key => $parameter)
            {
                $newParameters->$key = $parameter;
            }
            $parameters = $newParameters;
        }

        if(isset($parameters->id_article))
        {
            $this->setIdArticle($parameters->id_article);
        }else{
            throw new Exception('Missing parameter: id_article');
        }

        if(isset($parameters->id_familia))
        {
            $this->setIdFamilia($parameters->id_familia);
        }else{
            throw new Exception('Missing parameter: id_familia');
        }

        if(isset($parameters->id_nivell))
        {
            $this->setIdNivell($parameters->id_nivell);
        }else{
            $this->setIdNivell(null);
        }

        if(isset($parameters->id_pagina))
        {
            $this->setIdPagina($parameters->id_pagina);
        }else{
            throw new Exception('Missing parameter: id_pagina');
        }

        if(isset($parameters->id_fabricant))
        {
            $this->setIdFabricant($parameters->id_fabricant);
        }else{
            $this->setIdFabricant(null);
        }

        if(isset($parameters->id_proveidor))
        {
            $this->setIdProveidor($parameters->id_proveidor);
        }else{
            $this->setIdProveidor(null);
        }

        if(isset($parameters->id_iva))
        {
            $this->setIdIVA($parameters->id_iva);
        }else{
            throw new Exception('Missing parameter: id_iva');
        }

        if(isset($parameters->descripcio))
        {
            $this->setDescripcio($parameters->descripcio);
        }else{
            throw new Exception('Missing parameter: descripcio');
        }

        if(isset($parameters->plac))
        {
            $this->setPlac($parameters->plac);
        }else{
            $this->setPlac(null);
        }

        if(isset($parameters->imatge))
        {
            $this->setImatge($parameters->imatge);
        }else{
            throw new Exception('Missing parameter: imatge');
        }

        if(isset($parameters->precio))
        {
            $this->setPrecio($parameters->precio);
        }else{
            throw new Exception('Missing parameter: precio');
        }

        if(isset($parameters->referencia_propia))
        {
            $this->setReferenciaPropia($parameters->referencia_propia);
        }else{
            $this->setReferenciaPropia(null);
        }

        if(isset($parameters->referencia_proveidor))
        {
            $this->setReferenciaProveidor($parameters->referencia_proveidor);
        }else{
            $this->setReferenciaProveidor(null);
        }

        if(isset($parameters->oferta_desde))
        {
            $this->setOfertaDesde($parameters->oferta_desde);
        }else{
            $this->setOfertaDesde(null);
        }

        if(isset($parameters->oferta_fins))
        {
            $this->setOfertaFins($parameters->oferta_fins);
        }else{
            $this->setOfertaFins(null);
        }

        if(isset($parameters->preu_oferta))
        {
            $this->setPreuOferta($parameters->preu_oferta);
        }else{
            $this->setPreuOferta(null);
        }

        if(isset($parameters->stock))
        {
            $this->setStock($parameters->stock);
        }else{
            $this->setPreuOferta(null);
        }

        if(isset($parameters->alta))
        {
            if(empty($parameters->alta)&&$this->getIdArticle()==-1)
            {
                $this->setAlta(date('Y-m-d H:i:s'));
            }else{
                $this->setAlta($parameters->alta);
            }
        }else{
            $this->setAlta(null);
        }

        if(isset($parameters->actualitzacio))
        {
            if(empty($parameters->actualitzacio))
            {
                $this->setActualitzacio(date('Y-m-d H:i:s'));
            }else{
                $this->setActualitzacio($parameters->actualitzacio);
            }
        }else{
            $this->setActualitzacio(null);
        }

        if(isset($parameters->informacio))
        {
            $this->setInformacio($parameters->informacio);
        }else{
            $this->setInformacio(null);
        }
    }

    public static function getByIdPagina($id_pagina)
    {
        $pagina = Administrador_DB::getInfo(self::TABLE, '*', array('id_pagina = '.$id_pagina));

        return new Administrador_Article($pagina[0]);
    }

    public static function getById($id_article)
    {
        $pagina = Administrador_DB::getInfo(self::TABLE, '*', array('id_article = '.$id_article));

        return new Administrador_Article($pagina[0]);
    }

    public function getIdArticle()
    {
        return $this->id_article;
    }

    public function setIdArticle($id_especial)
    {
        return $this->id_article = $id_especial;
    }

    public function getDescripcio()
    {
        return stripslashes($this->descripcio);
    }

    public function setDescripcio($descripcio)
    {
        return $this->descripcio = addslashes($descripcio);
    }

    /**
     * @param mixed $actualitzacio
     */
    public function setActualitzacio($actualitzacio)
    {
        $this->actualitzacio = $actualitzacio;
    }

    /**
     * @return mixed
     */
    public function getActualitzacio()
    {
        return $this->actualitzacio;
    }

    /**
     * @param mixed $alta
     */
    public function setAlta($alta)
    {
        $this->alta = $alta;
    }

    /**
     * @return mixed
     */
    public function getAlta()
    {
        return $this->alta;
    }

    /**
     * @param mixed $id_fabricant
     */
    public function setIdFabricant($id_fabricant)
    {
        $this->id_fabricant = $id_fabricant;
    }

    /**
     * @return mixed
     */
    public function getIdFabricant()
    {
        return $this->id_fabricant;
    }

    public function getFabricant()
    {
        $paramsFabricant = Administrador_DB::getInfo(Administrador_Fabricant::TABLE, '*', array(
            "id_fabricant = ".$this->getIdFabricant()
        ));
        if(empty($paramsFabricant))
        {
            return null;
        }
        $fabricant = new Administrador_Fabricant($paramsFabricant[0]);
        if(empty($fabricant))
        {
            return null;
        }else{
            return $fabricant->getDescripcio();
        }
    }

    /**
     * @param mixed $id_familia
     */
    public function setIdFamilia($id_familia)
    {
        $this->id_familia = $id_familia;
    }

    /**
     * @return mixed
     */
    public function getIdFamilia()
    {
        return $this->id_familia;
    }

    public function getFamilia()
    {
        $paramsFamilia = Administrador_DB::getInfo(Administrador_Familia::TABLE, '*', array(
            "id_familia = ".$this->getIdFamilia()
        ));
        if(empty($paramsFamilia))
        {
            return null;
        }
        $familia = new Administrador_Familia($paramsFamilia[0]);
        if(empty($familia))
        {
            return null;
        }else{
            return $familia->getDescripcio();
        }
    }

    /**
     * @param mixed $id_iva
     */
    public function setIdIva($id_iva)
    {
        $this->id_iva = $id_iva;
    }

    /**
     * @return mixed
     */
    public function getIdIva()
    {
        return $this->id_iva;
    }

    public function getIva()
    {
        $paramsIva = Administrador_DB::getInfo(Administrador_IVA::TABLE, '*', array(
            "id_iva = ".$this->getIdIva()
        ));
        if(empty($paramsIva))
        {
            return null;
        }
        $iva = new Administrador_IVA($paramsIva[0]);
        if(empty($iva))
        {
            return null;
        }else{
            return $iva->getIva();
        }
    }

    /**
     * @param mixed $id_nivell
     */
    public function setIdNivell($id_nivell)
    {
        $this->id_nivell = $id_nivell;
    }

    /**
     * @return mixed
     */
    public function getIdNivell()
    {
        return $this->id_nivell;
    }

    public function getNivell()
    {
        $paramsNivell = Administrador_DB::getInfo(Administrador_Nivell::TABLE, '*', array(
            "id_nivell = ".$this->getIdNivell()
        ));
        if(empty($paramsNivell))
        {
            return null;
        }
        $nivell = new Administrador_Nivell($paramsNivell[0]);
        if(empty($nivell))
        {
            return null;
        }else{
            return $nivell->getDescripcio();
        }
    }

    /**
     * @param mixed $id_pagina
     */
    public function setIdPagina($id_pagina)
    {
        $this->id_pagina = $id_pagina;
    }

    /**
     * @return mixed
     */
    public function getIdPagina()
    {
        return $this->id_pagina;
    }

    public function getPagina()
    {
        $paramsPagina = Administrador_DB::getInfo(Administrador_Pagina::TABLE, '*', array(
            "id_pagina = ".$this->getIdPagina()
        ));
        $paramsPaginaKeywords = Administrador_DB::getInfo(Administrador_Pagina::TABLE_KEYWORDS, '*', array(
            "id_pagina = ".$this->getIdPagina()
        ));
        if(empty($paramsPagina))
        {
            return null;
        }
        $pagina = new Administrador_Pagina($paramsPagina[0], $paramsPaginaKeywords);
        if(empty($pagina))
        {
            return null;
        }else{
            return $pagina->getDescripcioAmigable();
        }
    }

    /**
     * @param mixed $id_proveidor
     */
    public function setIdProveidor($id_proveidor)
    {
        $this->id_proveidor = $id_proveidor;
    }

    /**
     * @return mixed
     */
    public function getIdProveidor()
    {
        return $this->id_proveidor;
    }

    public function getProveidor()
    {
        $paramsProveidor = Administrador_DB::getInfo(Administrador_Proveidor::TABLE, '*', array(
            "id_proveidor = ".$this->getIdProveidor()
        ));
        if(empty($paramsProveidor))
        {
            return null;
        }
        $proveidor = new Administrador_Proveidor($paramsProveidor[0]);
        if(empty($proveidor))
        {
            return null;
        }else{
            return $proveidor->getDescripcio();
        }
    }

    /**
     * @param mixed $imatge
     */
    public function setImatge($imatge)
    {
        $this->imatge = $imatge;
    }

    /**
     * @return mixed
     */
    public function getImatge()
    {
        return $this->imatge;
    }

    /**
     * @param mixed $informacio
     */
    public function setInformacio($informacio)
    {
        $this->informacio = addslashes($informacio);
    }

    /**
     * @return mixed
     */
    public function getInformacio()
    {
        return stripslashes($this->informacio);
    }

    /**
     * @param mixed $oferta_desde
     */
    public function setOfertaDesde($oferta_desde)
    {
        $this->oferta_desde = $oferta_desde;
    }

    /**
     * @return mixed
     */
    public function getOfertaDesde()
    {
        return $this->oferta_desde;
    }

    /**
     * @param mixed $oferta_fins
     */
    public function setOfertaFins($oferta_fins)
    {
        $this->oferta_fins = $oferta_fins;
    }

    /**
     * @return mixed
     */
    public function getOfertaFins()
    {
        return $this->oferta_fins;
    }

    /**
     * @param mixed $plac
     */
    public function setPlac($plac)
    {
        $this->plac = $plac;
    }

    /**
     * @return mixed
     */
    public function getPlac()
    {
        return $this->plac;
    }

    /**
     * @param mixed $precio
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;
    }

    /**
     * @return mixed
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * @param mixed $preu_oferta
     */
    public function setPreuOferta($preu_oferta)
    {
        $this->preu_oferta = $preu_oferta;
    }

    /**
     * @return mixed
     */
    public function getPreuOferta()
    {
        return $this->preu_oferta;
    }

    /**
     * @param mixed $referencia_propia
     */
    public function setReferenciaPropia($referencia_propia)
    {
        $this->referencia_propia = $referencia_propia;
    }

    /**
     * @return mixed
     */
    public function getReferenciaPropia()
    {
        return $this->referencia_propia;
    }

    /**
     * @param mixed $referencia_proveidor
     */
    public function setReferenciaProveidor($referencia_proveidor)
    {
        $this->referencia_proveidor = $referencia_proveidor;
    }

    /**
     * @return mixed
     */
    public function getReferenciaProveidor()
    {
        return $this->referencia_proveidor;
    }

    /**
     * @param mixed $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return mixed
     */
    public function getStock()
    {
        return $this->stock;
    }

    public function save()
    {
        if($this->getIdArticle()!= -1)
        {
            $pdo = Administrador_DB::update(Administrador_Article::TABLE, $this->getObjectAsArray(), array('id_article = '.$this->getIdArticle()));
            return $this;
        }else{
            $pdo = Administrador_DB::insert(Administrador_Article::TABLE, $this->getObjectAsArray());
            $idArticle = $pdo->lastInsertId();
            if(empty($idArticle))
            {
                return false;
            }
            $newArticleParams = Administrador_DB::getInfo(Administrador_Article::TABLE, '*', array('id_article = '.$idArticle));
            $newArticle = new Administrador_Article($newArticleParams[0]);
            $this->setAllParamsFromArticle($newArticle);
            return $this;
        }

    }

    public function delete()
    {
        return Administrador_DB::delete(Administrador_Article::TABLE, array('id_article = '.$this->getIdArticle()));
    }

    public function setAllParamsFromArticle(Administrador_Article $newParams)
    {
        $this->setIdArticle($newParams->getIdArticle());
        $this->setIdFamilia($newParams->getIdFamilia());
        $this->setIdNivell($newParams->getIdNivell());
        $this->setIdPagina($newParams->getIdPagina());
        $this->setIdFabricant($newParams->getIdFabricant());
        $this->setIdProveidor($newParams->getIdProveidor());
        $this->setIdIva($newParams->getIdIva());
        $this->setDescripcio($newParams->getDescripcio());
        $this->setPlac($newParams->getPlac());
        $this->setImatge($newParams->getImatge());
        $this->setPrecio($newParams->getPrecio());
        $this->setReferenciaPropia($newParams->getReferenciaPropia());
        $this->setReferenciaProveidor($newParams->getReferenciaProveidor());
        $this->setOfertaDesde($newParams->getOfertaDesde());
        $this->setOfertaFins($newParams->getOfertaFins());
        $this->setPreuOferta($newParams->getPreuOferta());
        $this->setStock($newParams->getStock());
        $this->setAlta($newParams->getAlta());
        $this->setActualitzacio($newParams->getActualitzacio());
        $this->setInformacio($newParams->getInformacio());
    }

    public function getObjectAsArray()
    {
        $return = array();
        $return['id_article'] = ($this->getIdArticle() == -1)? null : $this->getIdArticle();
        $return['id_familia'] = $this->getIdFamilia();
        $id_nivell = $this->getIdNivell();
        $return['id_nivell'] = (empty($id_nivell))? null : $this->getIdNivell();
        $id_pagina = $this->getIdPagina();
        $return['id_pagina'] = (empty($id_pagina))? null : $this->getIdPagina();
        $id_fabricant = $this->getIdFabricant();
        $return['id_fabricant'] = (empty($id_fabricant))? null : $this->getIdFabricant();
        $id_proveidor = $this->getIdProveidor();
        $return['id_proveidor'] = (empty($id_proveidor))? null : $this->getIdProveidor();
        $return['id_iva'] = $this->getIdIva();
        $return['descripcio'] = $this->getDescripcio();
        $return['plac'] = $this->getPlac();
        $return['imatge'] = $this->getImatge();
        $return['precio'] = $this->getPrecio();
        $return['referencia_propia'] = $this->getReferenciaPropia();
        $return['referencia_proveidor'] = $this->getReferenciaProveidor();
        $return['oferta_desde'] = $this->getOfertaDesde();
        $return['oferta_fins'] = $this->getOfertaFins();
        $return['preu_oferta'] = $this->getPreuOferta();
        $return['stock'] = $this->getStock();
        $return['alta'] = $this->getAlta();
        $return['actualitzacio'] = $this->getActualitzacio();
        $return['informacio'] = $this->getInformacio();

        return $return;
    }
}