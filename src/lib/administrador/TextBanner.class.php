<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 3/08/13
 * Time: 12:55
 * To change this template use File | Settings | File Templates.
 */

class Administrador_TextBanner implements Administrador_SerializableObject {
    const TABLE = 'TextBanner';
    private $id_textbanner;
    private $id_indexpaginaprincipal;
    private $id_promocio;
    private $textBanner;

    public function __construct($parameters)
    {
        if(is_array($parameters))
        {
            $newParameters = new stdClass();
            foreach($parameters as $key => $parameter)
            {
                $newParameters->$key = $parameter;
            }
            $parameters = $newParameters;
        }
        if(isset($parameters->id_textbanner))
        {
            $this->setIdTextBanner($parameters->id_textbanner);
        }else{
            throw new Exception('Missing parameter: id_textbanner');
        }

        if(isset($parameters->id_indexpaginaprincipal))
        {
            $this->setIdIndexPaginaPrincipal($parameters->id_indexpaginaprincipal);
        }else{
            $this->setIdIndexPaginaPrincipal(null);
        }

        if(isset($parameters->id_promocio))
        {
            $this->setIdPromocio($parameters->id_promocio);
        }else{
            $this->setIdPromocio(null);
        }

        if(isset($parameters->textBanner))
        {
            $this->setTextBanner($parameters->textBanner);
        }else{
            throw new Exception('Missing parameter: textBanner');
        }
    }

    public function getIdTextBanner()
    {
        return $this->id_textbanner;
    }

    public function setIdTextBanner($id_textBanner)
    {
        return $this->id_textbanner = $id_textBanner;
    }

    public function getIdPromocio()
    {
        return $this->id_promocio;
    }

    public function setIdPromocio($id_promocio)
    {
        return $this->id_promocio = $id_promocio;
    }

    public function getIdIndexPaginaPrincipal()
    {
        return $this->id_indexpaginaprincipal;
    }

    public function setIdIndexPaginaPrincipal($id_indexpaginaprincipal)
    {
        return $this->id_indexpaginaprincipal = $id_indexpaginaprincipal;
    }

    public function getTextBanner()
    {
        return $this->textBanner;
    }

    public function setTextBanner($textBanner)
    {
        return $this->textBanner = $textBanner;
    }

    public function save()
    {
        if($this->getIdTextBanner()!= -1)
        {
            $pdo = Administrador_DB::update(Administrador_TextBanner::TABLE, $this->getObjectAsArray(), array('id_textbanner = '.$this->getIdTextBanner()));
            return $this;
        }else{
            $pdo = Administrador_DB::insert(Administrador_TextBanner::TABLE, $this->getObjectAsArray());
            $idTextBanner = $pdo->lastInsertId();
            $newTextBannerParams = Administrador_DB::getInfo(Administrador_TextBanner::TABLE, '*', array('id_textbanner = '.$idTextBanner));
            $newTextBanner = new Administrador_TextBanner($newTextBannerParams[0]);
            $this->setAllParamsFromTextBanner($newTextBanner);
            return $this;
        }

    }

    public function delete()
    {
        return Administrador_DB::delete(Administrador_TextBanner::TABLE, array('id_textbanner = '.$this->getIdTextBanner()));
    }

    public function setAllParamsFromTextBanner(Administrador_TextBanner $newParams)
    {
        $this->setIdTextBanner($newParams->getIdTextBanner());
        $this->setIdIndexPaginaPrincipal($newParams->getIdIndexPaginaPrincipal());
        $this->setIdPromocio($newParams->getIdPromocio());
        $this->setTextBanner($newParams->getTextBanner());
    }

    public function getObjectAsArray()
    {
        $return = array();
        $return['id_textbanner'] = ($this->getIdTextBanner() == -1)? null : $this->getIdTextBanner();
        $idPaginaPrincipal = $this->getIdIndexPaginaPrincipal();
        $return['id_indexpaginaprincipal'] = (empty($idPaginaPrincipal))? null : $this->getIdIndexPaginaPrincipal();
        $idPromocio = $this->getIdPromocio();
        $return['id_promocio'] = (empty($idPromocio))? null : $this->getIdPromocio();
        $return['textBanner'] = $this->getTextBanner();

        return $return;
    }
}