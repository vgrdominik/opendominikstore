<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 3/08/13
 * Time: 12:55
 * To change this template use File | Settings | File Templates.
 */

class Administrador_Contingut implements Administrador_SerializableObject {
    const TABLE = 'Contingut';
    private $id_contingut;
    private $titol;
    private $descripcio;
    private $alta;
    private $imatge;
    private $id_pagina;

    public function __construct($parameters)
    {
        if(is_array($parameters))
        {
            $newParameters = new stdClass();
            foreach($parameters as $key => $parameter)
            {
                $newParameters->$key = $parameter;
            }
            $parameters = $newParameters;
        }
        if(isset($parameters->id_contingut))
        {
            $this->setIdContingut($parameters->id_contingut);
        }else{
            throw new Exception('Missing parameter: id_contingut');
        }

        if(isset($parameters->titol))
        {
            $this->setTitol($parameters->titol);
        }else{
            $this->setTitol(null);
        }

        if(isset($parameters->id_pagina))
        {
            $this->setIdPagina($parameters->id_pagina);
        }else{
            throw new Exception('Missing parameter: id_pagina');
        }

        if(isset($parameters->descripcio))
        {
            $this->setDescripcio($parameters->descripcio);
        }else{
            throw new Exception('Missing parameter: descripcio');
        }

        if(isset($parameters->alta))
        {
            if(empty($parameters->alta)&&$this->getIdContingut()==-1)
            {
                $this->setAlta(date('Y-m-d H:i:s'));
            }else{
                $this->setAlta($parameters->alta);
            }
        }else{
            throw new Exception('Missing parameter: alta');
        }

        if(isset($parameters->imatge))
        {
            $this->setImatge($parameters->imatge);
        }else{
            throw new Exception('Missing parameter: imatge');
        }
    }

    public function getIdContingut()
    {
        return $this->id_contingut;
    }

    public function setIdContingut($id_contingut)
    {
        return $this->id_contingut = $id_contingut;
    }

    public function getTitol()
    {
        return $this->titol;
    }

    public function setTitol($titol)
    {
        return $this->titol = $titol;
    }

    public function getIdPagina()
    {
        return $this->id_pagina;
    }

    public function setIdPagina($id_pagina)
    {
        return $this->id_pagina = $id_pagina;
    }

    public function getDescripcio()
    {
        return $this->descripcio;
    }

    public function setDescripcio($descripcio)
    {
        return $this->descripcio = $descripcio;
    }

    public function getAlta()
    {
        return $this->alta;
    }

    public function setAlta($alta)
    {
        return $this->alta = $alta;
    }

    public function getImatge()
    {
        return $this->imatge;
    }

    public function setImatge($imatge)
    {
        return $this->imatge = $imatge;
    }

    public function save()
    {
        if($this->getIdContingut()!= -1)
        {
            $pdo = Administrador_DB::update(Administrador_Contingut::TABLE, $this->getObjectAsArray(), array('id_contingut = '.$this->getIdContingut()));
            return $this;
        }else{
            $pdo = Administrador_DB::insert(Administrador_Contingut::TABLE, $this->getObjectAsArray());
            $idContingut = $pdo->lastInsertId();
            $newContingutParams = Administrador_DB::getInfo(Administrador_Contingut::TABLE, '*', array('id_contingut = '.$idContingut));
            $newContingut = new Administrador_Contingut($newContingutParams[0]);
            $this->setAllParamsFromContingut($newContingut);
            return $this;
        }

    }

    public function delete()
    {
        return Administrador_DB::delete(Administrador_Contingut::TABLE, array('id_contingut = '.$this->getIdContingut()));
    }

    public function setAllParamsFromContingut(Administrador_Contingut $newParams)
    {
        $this->setIdContingut($newParams->getIdContingut());
        $this->setTitol($newParams->getTitol());
        $this->setIdPagina($newParams->getIdPagina());
        $this->setDescripcio($newParams->getDescripcio());
        $this->setAlta($newParams->getAlta());
        $this->setimatge($newParams->getImatge());
    }

    public function getObjectAsArray()
    {
        $return = array();
        $return['id_contingut'] = ($this->getIdContingut() == -1)? null : $this->getIdContingut();
        $return['titol'] = $this->getTitol();
        $idPagina = $this->getIdPagina();
        $return['id_pagina'] = (empty($idPagina))? null : $this->getIdPagina();
        $return['descripcio'] = $this->getDescripcio();
        $return['alta'] = $this->getAlta();
        $return['imatge'] = $this->getImatge();

        return $return;
    }
}