<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 3/08/13
 * Time: 12:55
 * To change this template use File | Settings | File Templates.
 */

class Administrador_Promocio implements Administrador_SerializableObject {
    const TABLE = 'Promocio';
    const TABLE_PAGINA = 'Promocio_Pagina';
    private $id_promocio;
    private $id_tipus;
    private $tipus;
    private $descripcio;
    private $index;

    public function __construct($parameters, $index)
    {
        if(is_array($parameters))
        {
            $newParameters = new stdClass();
            foreach($parameters as $key => $parameter)
            {
                $newParameters->$key = $parameter;
            }
            $parameters = $newParameters;
        }
        if(isset($parameters->id_promocio))
        {
            $this->setIdPromocio($parameters->id_promocio);
            if(!empty($index))
            {
                $this->setIndex($index);
            }else{
                $this->setIndex(array());
            }
        }else{
            throw new Exception('Missing parameter: id_promocio');
        }

        if(isset($parameters->id_tipus))
        {
            $this->setIdTipus($parameters->id_tipus);
        }else{
            throw new Exception('Missing parameter: id_tipus');
        }

        if(isset($parameters->tipus))
        {
            $this->setTipus($parameters->tipus);
        }else{
            throw new Exception('Missing parameter: tipus');
        }

        if(isset($parameters->descripcio))
        {
            $this->setDescripcio($parameters->descripcio);
        }else{
            $this->setDescripcio(null);
        }
    }

    public static function getIndexFromDB($idPromocio)
    {
        $index = Administrador_DB::getInfo(Administrador_Promocio::TABLE_PAGINA, '*', array('id_promocio = '.$idPromocio));
        $return = array();
        foreach($index as $element)
        {
            $return[$element->id_pagina] = $element;
        }
        return $return;
    }

    public function getIndex()
    {
        if(empty($this->index))
        {
            return array();
        }

        return $this->index;
    }

    public function deleteIndexElement($id_pagina)
    {
        unset($this->index[$id_pagina]);
        return true;
    }

    public function setIndex($index)
    {
        $arrayIndex = array();
        foreach($index as $element)
        {
            if(is_array($element))
            {
                $newParameters = new stdClass();
                foreach($element as $key => $parameter)
                {
                    $newParameters->$key = $parameter;
                }
                $element = $newParameters;
            }

            if(empty($element->id_pagina))
            {
                throw new Exception('Missing id_pagina');
            }

            if(!isset($element->id_promocio))
            {
                throw new Exception('Missing id_promocio');
            }

            $arrayIndex[$element->id_pagina]['id_pagina'] = $element->id_pagina;
            $arrayIndex[$element->id_pagina]['id_promocio'] = $element->id_promocio;

        }
        return $this->index = $arrayIndex;
    }

    public function getIdPromocio()
    {
        return $this->id_promocio;
    }

    public function setIdPromocio($id_promocio)
    {
        return $this->id_promocio = $id_promocio;
    }

    public function getIdTipus()
    {
        return $this->id_tipus;
    }

    public function setIdTipus($id_tipus)
    {
        return $this->id_tipus = $id_tipus;
    }

    public function getTipus()
    {
        return $this->tipus;
    }

    public function setTipus($tipus)
    {
        return $this->tipus = $tipus;
    }

    public function getDescripcio()
    {
        return $this->descripcio;
    }

    public function setDescripcio($descripcio)
    {
        return $this->descripcio = $descripcio;
    }

    public function refresh($idPromocio = null)
    {
        $idPromocio = ($idPromocio !== null)? $idPromocio : $this->getIdPromocio();
        $paramsToPromocio = Administrador_DB::getInfo(Administrador_Promocio::TABLE, '*', array('id_promocio = '.$idPromocio));
        $index = Administrador_Promocio::getIndexFromDB($idPromocio);
        if(!isset($paramsToPromocio[0]))
        {
            throw new Exception('Error to refresh promocio');
        }
        $newPromocio = new Administrador_Promocio($paramsToPromocio[0], $index);
        $this->setAllParamsFromDB($newPromocio);
    }

    public static function saveElement($id_promocio, $id_pagina)
    {
        $newElement = Administrador_DB::getInfo(Administrador_Promocio::TABLE_PAGINA, "*", array(
            "id_promocio = $id_promocio",
            "id_pagina = $id_pagina",
        ));

        if(empty($newElement[0]))
        {
            $pdo = Administrador_DB::insert(Administrador_Promocio::TABLE_PAGINA, array(
                "id_promocio" => $id_promocio,
                "id_pagina" => $id_pagina
            ));

            return 'Elemento creado';
        }

        return 'El elemento ya existe';
    }

    public function saveIndex($idPromocio = null)
    {
        $index = $this->getObjectAsArrayOnlyIndex();
        $pdo = Administrador_DB::delete(Administrador_Promocio::TABLE_PAGINA, array('id_promocio = '.$this->getIdPromocio()));
        foreach($index['index'] as $element)
        {
            $element['id_promocio'] = ($idPromocio !== null)? $idPromocio : $this->getIdPromocio();
            $pdo = Administrador_DB::insert(Administrador_Promocio::TABLE_PAGINA, $element);
        }
    }

    public function save()
    {
        if($this->getIdPromocio()!= -1)
        {
            $pdo = Administrador_DB::update(Administrador_Promocio::TABLE, $this->getObjectAsArrayOnlyPromocio(), array('id_promocio = '.$this->getIdPromocio()));
            $this->saveIndex();
            $this->refresh();
            return $this;
        }else{
            $pdo = Administrador_DB::insert(Administrador_Promocio::TABLE, $this->getObjectAsArrayOnlyPromocio());
            $idPromocio = $pdo->lastInsertId();
            $this->saveIndex($idPromocio);
            $this->refresh($idPromocio);
            return $this;
        }

    }

    public function delete()
    {
        return Administrador_DB::delete(Administrador_Promocio::TABLE, array('id_promocio = '.$this->getIdPromocio()));
    }

    public function setAllParamsFromDB(Administrador_Promocio $newParams)
    {
        $this->setIdPromocio($newParams->getIdPromocio());
        $this->setIdTipus($newParams->getIdTipus());
        $this->setTipus($newParams->getTipus());
        $this->setDescripcio($newParams->getDescripcio());
        $this->setIndex($newParams->getIndex());
    }

    public function getObjectAsArray()
    {
        $return = array();
        $return['id_promocio'] = ($this->getIdPromocio() == -1)? null : $this->getIdPromocio();
        $return['id_tipus'] = $this->getIdTipus();
        $return['tipus'] = $this->getTipus();
        $descripcio = $this->getDescripcio();
        $return['descripcio'] = (empty($descripcio))? null : $descripcio;
        $return['index'] = $this->getIndex();

        return $return;
    }

    public function getObjectAsArrayOnlyPromocio()
    {
        $return = array();
        $return['id_promocio'] = ($this->getIdPromocio() == -1)? null : $this->getIdPromocio();
        $return['id_tipus'] = $this->getIdTipus();
        $return['tipus'] = $this->getTipus();
        $descripcio = $this->getDescripcio();
        $return['descripcio'] = (empty($descripcio))? null : $descripcio;

        return $return;
    }

    public function getObjectAsArrayOnlyIndex()
    {
        $return = array();
        $return['index'] = $this->getIndex();

        return $return;
    }
}