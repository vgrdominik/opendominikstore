<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 3/08/13
 * Time: 12:55
 * To change this template use File | Settings | File Templates.
 */

class Administrador_FitxaTecnicaDescripcio implements Administrador_SerializableObject {
    const TABLE = 'FitxaTecnicaDescripcio';
    private $id_fitxatecnicadescripcio;
    private $id_fitxatecnicatitol;
    private $descripcio;

    public function __construct($parameters)
    {
        if(is_array($parameters))
        {
            $newParameters = new stdClass();
            foreach($parameters as $key => $parameter)
            {
                $newParameters->$key = $parameter;
            }
            $parameters = $newParameters;
        }
        if(isset($parameters->id_fitxatecnicadescripcio))
        {
            $this->setIdFitxaTecnicaDescripcio($parameters->id_fitxatecnicadescripcio);
        }else{
            throw new Exception('Missing parameter: id_fitxatecnicadescripcio');
        }

        if(isset($parameters->id_fitxatecnicatitol))
        {
            $this->setIdFitxaTecnicaTitol($parameters->id_fitxatecnicatitol);
        }else{
            throw new Exception('Missing parameter: id_fitxatecnicatitol');
        }

        if(isset($parameters->descripcio))
        {
            $this->setDescripcio($parameters->descripcio);
        }else{
            throw new Exception('Missing parameter: descripcio');
        }
    }

    public function getIdFitxaTecnicaDescripcio()
    {
        return $this->id_fitxatecnicadescripcio;
    }

    public function setIdFitxaTecnicaDescripcio($id_fitxatecnicadescripcio)
    {
        return $this->id_fitxatecnicadescripcio = $id_fitxatecnicadescripcio;
    }

    public function getIdFitxaTecnicaTitol()
    {
        return $this->id_fitxatecnicatitol;
    }

    public function setIdFitxaTecnicaTitol($id_fitxatecnicatitol)
    {
        return $this->id_fitxatecnicatitol = $id_fitxatecnicatitol;
    }

    public function getDescripcio()
    {
        return $this->descripcio;
    }

    public function setDescripcio($descripcio)
    {
        return $this->descripcio = $descripcio;
    }

    public function getTitol()
    {
        $paramsTitol = Administrador_DB::getInfo(Administrador_FitxaTecnicaTitol::TABLE, '*', array(
            "id_fitxatecnicatitol = ".$this->getIdFitxaTecnicaTitol()
        ));
        if(empty($paramsTitol))
        {
            return null;
        }
        $titol = new Administrador_FitxaTecnicaTitol($paramsTitol[0]);
        if(empty($titol))
        {
            return null;
        }else{
            return $titol->getDescripcio();
        }
    }

    public function save()
    {
        if($this->getIdFitxaTecnicaDescripcio()!= -1)
        {
            $pdo = Administrador_DB::update(Administrador_FitxaTecnicaDescripcio::TABLE, $this->getObjectAsArray(), array('id_fitxatecnicadescripcio = '.$this->getIdFitxaTecnicaDescripcio(), 'id_fitxatecnicatitol = '.$this->getIdFitxaTecnicaTitol()));
            return $this;
        }else{
            $newIdToFitxaTecnicaDescripcio = Administrador_DB::getInfo(
                self::TABLE,
                array('id_fitxatecnicadescripcio'),
                null,
                0,
                1,
                array('id_fitxatecnicadescripcio' => 'DESC')
            );
            if(empty($newIdToFitxaTecnicaDescripcio))
            {
                $newIdToFitxaTecnicaDescripcio = 0;
            }else{
                $newIdToFitxaTecnicaDescripcio = $newIdToFitxaTecnicaDescripcio[0]->id_fitxatecnicadescripcio;
            }
            $this->setIdFitxaTecnicaDescripcio($newIdToFitxaTecnicaDescripcio + 1);
            $pdo = Administrador_DB::insert(Administrador_FitxaTecnicaDescripcio::TABLE, $this->getObjectAsArray());
            $newFitxaTecnicaDescripcioParams = Administrador_DB::getInfo(Administrador_FitxaTecnicaDescripcio::TABLE, '*', array('id_fitxatecnicadescripcio = '.$this->getIdFitxaTecnicaDescripcio(), 'id_fitxatecnicatitol = '.$this->getIdFitxaTecnicaTitol()));
            $newFitxaTecnicaDescripcio = new Administrador_FitxaTecnicaDescripcio($newFitxaTecnicaDescripcioParams[0]);
            $this->setAllParamsFromFitxaTecnicaDescripcio($newFitxaTecnicaDescripcio);
            return $this;
        }

    }

    public function delete()
    {
        return Administrador_DB::delete(Administrador_FitxaTecnicaDescripcio::TABLE, array('id_fitxatecnicadescripcio = '.$this->getIdFitxaTecnicaDescripcio(), 'id_fitxatecnicatitol = '.$this->getIdFitxaTecnicaTitol()));
    }

    public function setAllParamsFromFitxaTecnicaDescripcio(Administrador_FitxaTecnicaDescripcio $newParams)
    {
        $this->setIdFitxaTecnicaDescripcio($newParams->getIdFitxaTecnicaDescripcio());
        $this->setDescripcio($newParams->getDescripcio());
    }

    public function getObjectAsArray()
    {
        $return = array();
        $return['id_fitxatecnicadescripcio'] = $this->getIdFitxaTecnicaDescripcio();
        $return['id_fitxatecnicatitol'] = $this->getIdFitxaTecnicaTitol();
        $return['descripcio'] = $this->getDescripcio();

        return $return;
    }
}