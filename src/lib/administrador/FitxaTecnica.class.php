<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 3/08/13
 * Time: 12:55
 * To change this template use File | Settings | File Templates.
 */

class Administrador_FitxaTecnica implements Administrador_SerializableObject {
    const TABLE = 'FitxaTecnica';
    private $id_fitxatecnica;
    private $id_article;
    private $index;

    public function __construct($parameters, $index)
    {
        if(is_array($parameters))
        {
            $newParameters = new stdClass();
            foreach($parameters as $key => $parameter)
            {
                $newParameters->$key = $parameter;
            }
            $parameters = $newParameters;
        }
        if(isset($parameters->id_fitxatecnica))
        {
            $this->setIdFitxaTecnica($parameters->id_fitxatecnica);
        }else{
            throw new Exception('Missing parameter: id_fitxatecnica');
        }

        if(isset($parameters->id_article))
        {
            $this->setIdArticle($parameters->id_article);
        }else{
            throw new Exception('Missing parameter: id_article');
        }

        if(!empty($index))
        {
            $this->setIndex($index);
        }else{
            $this->setIndex(array());
        }
    }

    public static function getIndexFromDB($idFitxaTecnica)
    {
        $index = Administrador_DB::getInfo(Administrador_FitxaTecnicaFitxaTecnicaDescripcio::TABLE, '*', array('id_promocio = '.$idFitxaTecnica));
        $return = array();
        foreach($index as $element)
        {
            $return[$element->id_fitxatecnicatitol] = $element->id_fitxatecnicadescripcio;
        }
        return $return;
    }

    public function getIndex()
    {
        if(empty($this->index))
        {
            return array();
        }

        return $this->index;
    }

    public function deleteIndexElement($id_fitxatecnicatitol, $id_fitxatecnicadescripcio)
    {
        unset($this->index[$id_fitxatecnicatitol][$id_fitxatecnicadescripcio]);
        return true;
    }

    public function setIndex($index)
    {
        $arrayIndex = array();
        foreach($index as $idTitol)
        {
            foreach($idTitol as $idDescripcio)
            {

                if(empty($idDescripcio))
                {
                    throw new Exception('Missing id descripció');
                }

                if(empty($idTitol))
                {
                    throw new Exception('Missing id títol');
                }

                $arrayIndex[$idTitol][$idDescripcio] = true;
            }

        }
        return $this->index = $arrayIndex;
    }

    public function getIdFitxaTecnica()
    {
        return $this->id_fitxatecnica;
    }

    public function setIdFitxaTecnica($id_fitxatecnica)
    {
        return $this->id_fitxatecnica = $id_fitxatecnica;
    }

    public function getIdArticle()
    {
        return $this->id_article;
    }

    public function setIdArticle($id_article)
    {
        return $this->id_article = $id_article;
    }

    public function refresh($idFitxaTecnica = null)
    {
        $idFitxaTecnica = ($idFitxaTecnica !== null)? $idFitxaTecnica : $this->getIdFitxaTecnica();
        $paramsToFitxaTecnica = Administrador_DB::getInfo(Administrador_FitxaTecnica::TABLE, '*', array('id_fitxatecnica = '.$idFitxaTecnica));
        $index = Administrador_FitxaTecnica::getIndexFromDB($idFitxaTecnica);
        if(!isset($paramsToFitxaTecnica[0]))
        {
            throw new Exception('Error to refresh promocio');
        }
        $newPromocio = new Administrador_FitxaTecnica($paramsToFitxaTecnica[0], $index);
        $this->setAllParamsFromFitxaTecnica($newPromocio);
    }

    public static function saveElement($idFitxaTecnica, $idFitxaTecnicaDescripcio, $idFitxaTecnicaTitol)
    {
        $newElement = Administrador_DB::getInfo(Administrador_FitxaTecnicaFitxaTecnicaDescripcio::TABLE, "*", array(
            "id_fitxatecnica = $idFitxaTecnica",
            "id_fitxatecnicadescripcio = $idFitxaTecnicaDescripcio",
            "id_fitxatecnicatitol = $idFitxaTecnicaTitol",
        ));

        if(empty($newElement[0]))
        {
            $newElement = new Administrador_FitxaTecnicaFitxaTecnicaDescripcio(array(
                "id_fitxatecnica = $idFitxaTecnica",
                "id_fitxatecnicadescripcio = $idFitxaTecnicaDescripcio",
                "id_fitxatecnicatitol = $idFitxaTecnicaTitol",
            ));
            $newElement->save();

            return 'Elemento creado';
        }

        return 'El elemento ya existe';
    }

    public function saveIndex($idFitxaTecnica = null)
    {
        $index = $this->getObjectAsArrayOnlyIndex();
        $pdo = Administrador_DB::delete(Administrador_FitxaTecnicaFitxaTecnicaDescripcio::TABLE, array('id_fitxatecnica = '.$this->getIdFitxaTecnica()));
        foreach($index['index'] as $element)
        {
            $element['id_fitxatecnica'] = ($idFitxaTecnica !== null)? $idFitxaTecnica : $this->getIdPromocio();
            $pdo = Administrador_DB::insert(Administrador_FitxaTecnicaFitxaTecnicaDescripcio::TABLE, $element);
        }
    }

    public function save()
    {
        if($this->getIdFitxaTecnica()!= -1)
        {
            $pdo = Administrador_DB::update(Administrador_FitxaTecnica::TABLE, $this->getObjectAsArrayOnlyFitxaTecnica(), array('id_fitxatecnica = '.$this->getIdFitxaTecnica()));
            return $this;
        }else{
            $pdo = Administrador_DB::insert(Administrador_FitxaTecnica::TABLE, $this->getObjectAsArrayOnlyFitxaTecnica());
            $idFitxaTecnica = $pdo->lastInsertId();
            $newFitxaTecnicaParams = Administrador_DB::getInfo(Administrador_FitxaTecnica::TABLE, '*', array('id_fitxatecnica = '.$idFitxaTecnica));
            $newFitxaTecnica = new Administrador_TextBanner($newFitxaTecnicaParams[0]);
            $this->setAllParamsFromFitxaTecnica($newFitxaTecnica);
            return $this;
        }

    }

    public function delete()
    {
        return Administrador_DB::delete(Administrador_FitxaTecnica::TABLE, array('id_fitxatecnica = '.$this->getIdFitxaTecnica()));
    }

    public function setAllParamsFromFitxaTecnica(Administrador_FitxaTecnica $newParams)
    {
        $this->setIdFitxaTecnica($newParams->getIdFitxaTecnica());
        $this->setIndex($newParams->getIndex());
    }

    public function getObjectAsArray()
    {
        $return = array();
        $return['id_fitxatecnica'] = ($this->getIdFitxaTecnica() == -1)? null : $this->getIdFitxaTecnica();
        $return['id_article'] = $this->getIdArticle();
        $return['index'] = $this->getIndex();

        return $return;
    }

    public function getObjectAsArrayOnlyFitxaTecnica()
    {
        $return = array();
        $return['id_fitxatecnica'] = ($this->getIdFitxaTecnica() == -1)? null : $this->getIdFitxaTecnica();
        $return['id_article'] = $this->getIdArticle();

        return $return;
    }

    public function getObjectAsArrayOnlyIndex()
    {
        $return = array();
        $return['index'] = $this->getIndex();

        return $return;
    }
}