<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 25/08/13
 * Time: 12:35
 * To change this template use File | Settings | File Templates.
 */

class Administrador_Widget {
    const WIDGET_IMAGEN = 'Imatge';
    const WIDGET_IMAGEN_MODULE = 'administrador/imagen';
    const WIDGET_IMAGEN_MODULE_VIEW = 'administrador/imagenView';
    const WIDGET_IMAGEN_MODULE_VIEW_INDEX = 'id_imatge';
    const WIDGET_TEXTO = 'TextBanner';
    const WIDGET_TEXTO_MODULE = 'administrador/textbanner';
    const WIDGET_TEXTO_MODULE_VIEW = 'administrador/textbannerView';
    const WIDGET_TEXTO_MODULE_VIEW_INDEX = 'id_textbanner';
    const WIDGET_SLIDE = 'Slide';
    const WIDGET_SLIDE_MODULE = 'administrador/slide';
    const WIDGET_SLIDE_MODULE_VIEW = 'administrador/slideImatge';
    const WIDGET_SLIDE_MODULE_VIEW_INDEX = 'id_slide';

    public static function getWidgetsName()
    {
        $return = array();
        $return[0] = new stdClass();
        $return[0]->name = self::WIDGET_IMAGEN;
        $return[0]->module = self::getModuleFromWidgetName(self::WIDGET_IMAGEN);
        $return[0]->moduleView = self::getModuleViewFromWidgetName(self::WIDGET_IMAGEN);
        $return[0]->moduleViewIndex = self::getModuleViewIndexFromWidgetName(self::WIDGET_IMAGEN);
        $return[1] = new stdClass();
        $return[1]->name = self::WIDGET_TEXTO;
        $return[1]->module = self::getModuleFromWidgetName(self::WIDGET_TEXTO);
        $return[1]->moduleView = self::getModuleViewFromWidgetName(self::WIDGET_TEXTO);
        $return[1]->moduleViewIndex = self::getModuleViewIndexFromWidgetName(self::WIDGET_TEXTO);
        $return[2] = new stdClass();
        $return[2]->name = self::WIDGET_SLIDE;
        $return[2]->module = self::getModuleFromWidgetName(self::WIDGET_SLIDE);
        $return[2]->moduleView = self::getModuleViewFromWidgetName(self::WIDGET_SLIDE);
        $return[2]->moduleViewIndex = self::getModuleViewIndexFromWidgetName(self::WIDGET_SLIDE);

        return $return;
    }

    public static function getModuleFromWidgetName($widget)
    {
        switch($widget)
        {
            case self::WIDGET_IMAGEN:
                $return = self::WIDGET_IMAGEN_MODULE;
                break;
            case self::WIDGET_TEXTO:
                $return = self::WIDGET_TEXTO_MODULE;
                break;
            case self::WIDGET_SLIDE:
                $return = self::WIDGET_SLIDE_MODULE;
                break;
            default;
                $return = false;
                break;
        }

        return $return;
    }

    public static function getModuleViewFromWidgetName($widget)
    {
        switch($widget)
        {
            case self::WIDGET_IMAGEN:
                $return = self::WIDGET_IMAGEN_MODULE_VIEW;
                break;
            case self::WIDGET_TEXTO:
                $return = self::WIDGET_TEXTO_MODULE_VIEW;
                break;
            case self::WIDGET_SLIDE:
                $return = self::WIDGET_SLIDE_MODULE_VIEW;
                break;
            default;
                $return = false;
                break;
        }

        return $return;
    }

    public static function getModuleViewIndexFromWidgetName($widget)
    {
        switch($widget)
        {
            case self::WIDGET_IMAGEN:
                $return = self::WIDGET_IMAGEN_MODULE_VIEW_INDEX;
                break;
            case self::WIDGET_TEXTO:
                $return = self::WIDGET_TEXTO_MODULE_VIEW_INDEX;
                break;
            case self::WIDGET_SLIDE:
                $return = self::WIDGET_SLIDE_MODULE_VIEW_INDEX;
                break;
            default;
                $return = false;
                break;
        }

        return $return;
    }
}