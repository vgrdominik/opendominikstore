<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 3/08/13
 * Time: 12:55
 * To change this template use File | Settings | File Templates.
 */

class Administrador_IndexPaginaPrincipal implements Administrador_SerializableObject {
    const TABLE = 'IndexPaginaPrincipal';
    const TABLE_TIPUS = 'IndexPaginaPrincipal_Tipus';
    private $id_indexpaginaprincipal;
    private $id_pagina;
    private $descripcio;
    private $index;

    public function __construct($parameters, $index)
    {
        if(is_array($parameters))
        {
            $newParameters = new stdClass();
            foreach($parameters as $key => $parameter)
            {
                $newParameters->$key = $parameter;
            }
            $parameters = $newParameters;
        }
        if(isset($parameters->id_indexpaginaprincipal))
        {
            $this->setIdIndexPaginaPrincipal($parameters->id_indexpaginaprincipal);
            if(!empty($index))
            {
                $this->setIndex($index);
            }else{
                $this->setIndex(array());
            }
        }else{
            throw new Exception('Missing parameter: id_indexpaginaprincipal');
        }

        if(isset($parameters->id_pagina))
        {
            $this->setIdPagina($parameters->id_pagina);
        }else{
            $this->setIdPagina(null);
        }

        if(isset($parameters->descripcio))
        {
            $this->setDescripcio($parameters->descripcio);
        }else{
            $this->setDescripcio(null);
        }
    }

    public static function getByIdPagina($id_pagina)
    {
        $pagina = Administrador_DB::getInfo(Administrador_IndexPaginaPrincipal::TABLE, '*', array('id_pagina = '.$id_pagina));
        $index = Administrador_IndexPaginaPrincipal::getIndexFromDB($pagina[0]->id_indexpaginaprincipal);

        return new Administrador_IndexPaginaPrincipal($pagina[0], $index);
    }

    public static function getIndexFromDB($idIndexPaginaPrincipal)
    {
        $index = Administrador_DB::getInfo(Administrador_IndexPaginaPrincipal::TABLE_TIPUS, '*', array('id_indexpaginaprincipal = '.$idIndexPaginaPrincipal));
        $return = array();
        foreach($index as $element)
        {
            $return[$element->tipus][$element->id_tipus] = $element;
        }
        return $return;
    }

    public function getIndex()
    {
        if(empty($this->index))
        {
            return array();
        }

        foreach($this->index as $keyTipus => $tipus)
        {
            foreach($tipus as $keyElement => $element)
            {
                $this->index[$keyTipus][$keyElement]['ordre'] = stripslashes($this->index[$keyTipus][$keyElement]['ordre']);
            }
        }
        return $this->index;
    }

    private function cmpIndex($a, $b) {
        if ($a['ordre'] == $b['ordre']) {
            return 0;
        }
        return ($a['ordre'] < $b['ordre']) ? -1 : 1;
    }

    public function getIndexOrdered()
    {
        if(empty($this->index))
        {
            return array();
        }

        $return = array();
        $count = 0;
        foreach($this->index as $keyTipus => $tipus)
        {
            foreach($tipus as $keyElement => $element)
            {
                $this->index[$keyTipus][$keyElement]['ordre'] = stripslashes($this->index[$keyTipus][$keyElement]['ordre']);
                $return[$count] = $this->index[$keyTipus][$keyElement];
                $count++;
            }
        }
        uasort($return, array($this, 'cmpIndex'));

        return $return;
    }

    public function deleteIndexElement($tipus, $id_tipus)
    {
        unset($this->index[$tipus][$id_tipus]);
        return true;
    }

    public function setIndex($index)
    {
        $arrayIndex = array();
        foreach($index as $tipus)
        {
            foreach($tipus as $element)
            {
                if(is_array($element))
                {
                    $newParameters = new stdClass();
                    foreach($element as $key => $parameter)
                    {
                        $newParameters->$key = $parameter;
                    }
                    $element = $newParameters;
                }

                if(empty($element->width))
                {
                    throw new Exception('Missing width');
                }

                if(empty($element->height))
                {
                    throw new Exception('Missing height');
                }

                if(empty($element->ordre))
                {
                    throw new Exception('Missing ordre');
                }

                if(empty($element->tipus))
                {
                    throw new Exception('Missing tipus');
                }

                if(empty($element->params))
                {
                    $element->params = serialize(null);
                }

                if(!isset($element->id_tipus))
                {
                    throw new Exception('Missing id_tipus');
                }

                $arrayIndex[$element->tipus][$element->id_tipus]['width'] = $element->width;
                $arrayIndex[$element->tipus][$element->id_tipus]['height'] = $element->height;
                $arrayIndex[$element->tipus][$element->id_tipus]['ordre'] = addslashes($element->ordre);
                $arrayIndex[$element->tipus][$element->id_tipus]['params'] = unserialize($element->params);
                $arrayIndex[$element->tipus][$element->id_tipus]['tipus'] = $element->tipus;
                $arrayIndex[$element->tipus][$element->id_tipus]['id_tipus'] = $element->id_tipus;
            }

        }
        return $this->index = $arrayIndex;
    }

    public function getIdIndexPaginaPrincipal()
    {
        return $this->id_indexpaginaprincipal;
    }

    public function setIdIndexPaginaPrincipal($id_indexpaginaprincipal)
    {
        return $this->id_indexpaginaprincipal = $id_indexpaginaprincipal;
    }

    public function getIdPagina()
    {
        return $this->id_pagina;
    }

    public function setIdPagina($id_pagina)
    {
        return $this->id_pagina = $id_pagina;
    }

    public function getDescripcio()
    {
        return $this->descripcio;
    }

    public function setDescripcio($descripcio)
    {
        return $this->descripcio = $descripcio;
    }

    public function refresh($idIndexPaginaPrincipal = null)
    {
        $idIndexPaginaPrincipal = ($idIndexPaginaPrincipal !== null)? $idIndexPaginaPrincipal : $this->getIdIndexPaginaPrincipal();
        $paramsToPaginaPrincipal = Administrador_DB::getInfo(Administrador_IndexPaginaPrincipal::TABLE, '*', array('id_indexpaginaprincipal = '.$idIndexPaginaPrincipal));
        $index = Administrador_IndexPaginaPrincipal::getIndexFromDB($idIndexPaginaPrincipal);
        if(!isset($paramsToPaginaPrincipal[0]))
        {
            throw new Exception('Error to refresh pagina principal');
        }
        $newPaginaPrincipal = new Administrador_IndexPaginaPrincipal($paramsToPaginaPrincipal[0], $index);
        $this->setAllParamsFromDB($newPaginaPrincipal);
    }

    public static function saveElement($id_indexpaginaprincipal, $tipus, $id_tipus, $width, $height, $ordre)
    {
        $newElement = Administrador_DB::getInfo(Administrador_IndexPaginaPrincipal::TABLE_TIPUS, "*", array(
            "id_indexpaginaprincipal = $id_indexpaginaprincipal",
            "tipus = '$tipus'",
            "id_tipus = $id_tipus"
        ));

        if(!empty($newElement[0]))
        {
            $pdo = Administrador_DB::update(Administrador_IndexPaginaPrincipal::TABLE_TIPUS, array(
                    "width" => $width,
                    "height" => $height,
                    "ordre" => $ordre
                ),
                array(
                    "id_indexpaginaprincipal = '$id_indexpaginaprincipal'",
                    "tipus = '$tipus'",
                    "id_tipus = '$id_tipus'",
                ));

            return 'El elemento ya existe';
        }else{
            $pdo = Administrador_DB::insert(Administrador_IndexPaginaPrincipal::TABLE_TIPUS, array(
                "id_indexpaginaprincipal" => $id_indexpaginaprincipal,
                "tipus" => $tipus,
                "id_tipus" => $id_tipus,
                "width" => $width,
                "height" => $height,
                "ordre" => $ordre
            ));
        }

        return 'Elemento creado';
    }

    public function saveIndex($idIndexPaginaPrincipal = null)
    {
        $index = $this->getObjectAsArrayOnlyIndex();
        $pdo = Administrador_DB::delete(Administrador_IndexPaginaPrincipal::TABLE_TIPUS, array('id_indexpaginaprincipal = '.$this->getIdIndexPaginaPrincipal()));
        foreach($index['index'] as $tipus)
        {
            foreach($tipus as $element)
            {
                $element['id_indexpaginaprincipal'] = ($idIndexPaginaPrincipal !== null)? $idIndexPaginaPrincipal : $this->getIdIndexPaginaPrincipal();
                $element['ordre'] = addslashes($element['ordre']);
                switch($element['tipus'])
                {
                    case 'Imatge':
                        $elementParams = Administrador_DB::getInfo(Administrador_Imatge::TABLE, '*', array('id_imatge = '.$element['id_tipus']));
                        $imatge = new Administrador_Imatge($elementParams[0]);
                        $element['params'] = serialize($imatge->getObjectAsArray());
                        break;
                    case 'TextBanner':
                        $elementParams = Administrador_DB::getInfo(Administrador_TextBanner::TABLE, '*', array('id_textbanner = '.$element['id_tipus']));
                        $textBanner = new Administrador_TextBanner($elementParams[0]);
                        $element['params'] = serialize($textBanner->getObjectAsArray());
                        break;
                    case 'Slide':
                        $elementParams = Administrador_DB::getInfo(Administrador_Slide::TABLE, '*', array('id_slide = '.$element['id_tipus']));
                        $elementImatgesParams = Administrador_DB::getInfo(Administrador_Slide::TABLE_IMATGE, '*', array('id_slide = '.$element['id_tipus']));
                        $slide = new Administrador_Slide($elementParams[0], $elementImatgesParams);
                        $element['params'] = serialize($slide->getObjectAsArray());
                        break;
                }
                $pdo = Administrador_DB::insert('IndexPaginaPrincipal_Tipus', $element);
            }
        }
    }

    public function save()
    {
        if($this->getIdIndexPaginaPrincipal()!= -1)
        {
            $pdo = Administrador_DB::update(Administrador_IndexPaginaPrincipal::TABLE, $this->getObjectAsArrayOnlyIndexPaginaPrincipal(), array('id_indexpaginaprincipal = '.$this->getIdIndexPaginaPrincipal()));
            $this->saveIndex();
            $this->refresh();
            return $this;
        }else{
            $pdo = Administrador_DB::insert(Administrador_IndexPaginaPrincipal::TABLE, $this->getObjectAsArrayOnlyIndexPaginaPrincipal());
            $idIndexPaginaPrincipal = $pdo->lastInsertId();
            $this->saveIndex($idIndexPaginaPrincipal);
            $this->refresh($idIndexPaginaPrincipal);
            return $this;
        }

    }

    public function delete()
    {
        return Administrador_DB::delete(Administrador_IndexPaginaPrincipal::TABLE, array('id_indexpaginaprincipal = '.$this->getIdIndexPaginaPrincipal()));
    }

    public function setAllParamsFromDB(Administrador_IndexPaginaPrincipal $newParams)
    {
        $this->setIdIndexPaginaPrincipal($newParams->getIdIndexPaginaPrincipal());
        $this->setIdPagina($newParams->getIdPagina());
        $this->getDescripcio($newParams->getDescripcio());
        $this->setIndex($newParams->getIndex());
    }

    public function getObjectAsArray()
    {
        $return = array();
        $return['id_indexpaginaprincipal'] = ($this->getIdIndexPaginaPrincipal() == -1)? null : $this->getIdIndexPaginaPrincipal();
        $idPagina = $this->getIdPagina();
        $return['id_pagina'] = (empty($idPagina))? null : $idPagina;
        $descripcio = $this->getDescripcio();
        $return['descripcio'] = (empty($descripcio))? null : $descripcio;
        $return['index'] = $this->getIndex();

        return $return;
    }

    public function getObjectAsArrayOnlyIndexPaginaPrincipal()
    {
        $return = array();
        $return['id_indexpaginaprincipal'] = ($this->getIdIndexPaginaPrincipal() == -1)? null : $this->getIdIndexPaginaPrincipal();
        $idPagina = $this->getIdPagina();
        $return['id_pagina'] = (empty($idPagina))? null : $idPagina;
        $descripcio = $this->getDescripcio();
        $return['descripcio'] = (empty($descripcio))? null : $descripcio;

        return $return;
    }

    public function getObjectAsArrayOnlyIndex()
    {
        $return = array();
        $return['index'] = $this->getIndex();

        return $return;
    }
}