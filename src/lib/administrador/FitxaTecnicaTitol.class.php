<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 3/08/13
 * Time: 12:55
 * To change this template use File | Settings | File Templates.
 */

class Administrador_FitxaTecnicaTitol implements Administrador_SerializableObject {
    const TABLE = 'FitxaTecnicaTitol';
    private $id_fitxatecnicatitol;
    private $descripcio;

    public function __construct($parameters)
    {
        if(is_array($parameters))
        {
            $newParameters = new stdClass();
            foreach($parameters as $key => $parameter)
            {
                $newParameters->$key = $parameter;
            }
            $parameters = $newParameters;
        }
        if(isset($parameters->id_fitxatecnicatitol))
        {
            $this->setIdFitxaTecnicaTitol($parameters->id_fitxatecnicatitol);
        }else{
            throw new Exception('Missing parameter: id_fitxatecnicatitol');
        }

        if(isset($parameters->descripcio))
        {
            $this->setDescripcio($parameters->descripcio);
        }else{
            throw new Exception('Missing parameter: descripcio');
        }
    }

    public function getIdFitxaTecnicaTitol()
    {
        return $this->id_fitxatecnicatitol;
    }

    public function setIdFitxaTecnicaTitol($id_fitxatecnicatitol)
    {
        return $this->id_fitxatecnicatitol = $id_fitxatecnicatitol;
    }

    public function getDescripcio()
    {
        return stripslashes($this->descripcio);
    }

    public function setDescripcio($descripcio)
    {
        return $this->descripcio = addslashes($descripcio);
    }

    public function save()
    {
        if($this->getIdFitxaTecnicaTitol()!= -1)
        {
            $pdo = Administrador_DB::update(Administrador_FitxaTecnicaTitol::TABLE, $this->getObjectAsArray(), array('id_fitxatecnicatitol = '.$this->getIdFitxaTecnicaTitol()));
            return $this;
        }else{
            $pdo = Administrador_DB::insert(Administrador_FitxaTecnicaTitol::TABLE, $this->getObjectAsArray());
            $idFitxaTecnicaTitol = $pdo->lastInsertId();
            $newFitxaTecnicaTitolParams = Administrador_DB::getInfo(Administrador_FitxaTecnicaTitol::TABLE, '*', array('id_fitxatecnicatitol = '.$idFitxaTecnicaTitol));
            $newFitxaTecnicaTitol = new Administrador_FitxaTecnicaTitol($newFitxaTecnicaTitolParams[0]);
            $this->setAllParamsFromFitxaTecnicaTitol($newFitxaTecnicaTitol);
            return $this;
        }

    }

    public function delete()
    {
        return Administrador_DB::delete(Administrador_FitxaTecnicaTitol::TABLE, array('id_fitxatecnicatitol = '.$this->getIdFitxaTecnicaTitol()));
    }

    public function setAllParamsFromFitxaTecnicaTitol(Administrador_FitxaTecnicaTitol $newParams)
    {
        $this->setIdFitxaTecnicaTitol($newParams->getIdFitxaTecnicaTitol());
        $this->setDescripcio($newParams->getDescripcio());
    }

    public function getObjectAsArray()
    {
        $return = array();
        $return['id_fitxatecnicatitol'] = ($this->getIdFitxaTecnicaTitol() == -1)? null : $this->getIdFitxaTecnicaTitol();
        $return['descripcio'] = $this->getDescripcio();

        return $return;
    }
}