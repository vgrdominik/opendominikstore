<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 3/08/13
 * Time: 12:55
 * To change this template use File | Settings | File Templates.
 */

class Administrador_FitxaTecnicaFitxaTecnicaDescripcio implements Administrador_SerializableObject {
    const TABLE = 'FitxaTecnica_FitxaTecnicaDescripcio';
    private $id_fitxatecnica;
    private $id_fitxatecnicadescripcio;
    private $id_fitxatecnicatitol;

    public function __construct($parameters)
    {
        if(is_array($parameters))
        {
            $newParameters = new stdClass();
            foreach($parameters as $key => $parameter)
            {
                $newParameters->$key = $parameter;
            }
            $parameters = $newParameters;
        }

        if(isset($parameters->id_fitxatecnica))
        {
            $this->setIdFitxaTecnica($parameters->id_fitxatecnica);
        }else{
            throw new Exception('Missing parameter: id_fitxatecnica');
        }

        if(isset($parameters->id_fitxatecnicadescripcio))
        {
            $this->setIdFitxaTecnicaDescripcio($parameters->id_fitxatecnicadescripcio);
        }else{
            throw new Exception('Missing parameter: id_fitxatecnicadescripcio');
        }

        if(isset($parameters->id_fitxatecnicatitol))
        {
            $this->setIdFitxaTecnicaTitol($parameters->id_fitxatecnicatitol);
        }else{
            throw new Exception('Missing parameter: id_fitxatecnicatitol');
        }
    }

    public function getIdFitxaTecnica()
    {
        return $this->id_fitxatecnica;
    }

    public function setIdFitxaTecnica($id_fitxatecnica)
    {
        return $this->id_fitxatecnica = $id_fitxatecnica;
    }

    public function getIdFitxatecnicaDescripcio()
    {
        return $this->id_fitxatecnicadescripcio;
    }

    public function setIdFitxaTecnicaDescripcio($id_fitxatecnicadescripcio)
    {
        return $this->id_fitxatecnicadescripcio = $id_fitxatecnicadescripcio;
    }

    public function getIdFitxaTecnicaTitol()
    {
        return $this->id_fitxatecnicatitol;
    }

    public function setIdFitxaTecnicaTitol($id_fitxatecnicatitol)
    {
        return $this->id_fitxatecnicatitol = $id_fitxatecnicatitol;
    }

    public function save()
    {
        if($this->getIdFitxaTecnica()!= -1)
        {
            $pdo = Administrador_DB::update(Administrador_FitxaTecnicaFitxaTecnicaDescripcio::TABLE, $this->getObjectAsArray(), array('id_fitxatecnica = '.$this->getIdFitxaTecnica(), 'id_fitxatecnicatitol = '.$this->getIdFitxaTecnicaTitol(), 'id_fitxatecnicadescripcio = '.$this->getIdFitxatecnicaDescripcio()));
            return $this;
        }else{
            $pdo = Administrador_DB::insert(Administrador_FitxaTecnicaFitxaTecnicaDescripcio::TABLE, $this->getObjectAsArray());
            $newFitxaTecnicaParams = Administrador_DB::getInfo(Administrador_FitxaTecnicaFitxaTecnicaDescripcio::TABLE, '*', array('id_fitxatecnica = '.$this->getIdFitxaTecnica(), 'id_fitxatecnicatitol = '.$this->getIdFitxaTecnicaTitol(), 'id_fitxatecnicadescripcio = '.$this->getIdFitxatecnicaDescripcio()));
            $newFitxaTecnica = new Administrador_FitxaTecnicaFitxaTecnicaDescripcio($newFitxaTecnicaParams[0]);
            $this->setAllParamsFromFitxaTecnica($newFitxaTecnica);
            return $this;
        }

    }

    public function delete()
    {
        return Administrador_DB::delete(Administrador_FitxaTecnicaFitxaTecnicaDescripcio::TABLE, array('id_fitxatecnica = '.$this->getIdFitxaTecnica(), 'id_fitxatecnicatitol = '.$this->getIdFitxaTecnicaTitol(), 'id_fitxatecnicadescripcio = '.$this->getIdFitxatecnicaDescripcio()));
    }

    public function setAllParamsFromFitxaTecnica(Administrador_FitxaTecnicaFitxaTecnicaDescripcio $newParams)
    {
        $this->setIdFitxaTecnica($newParams->getIdFitxaTecnica());
        $this->setIdFitxaTecnicaDescripcio($newParams->getIdFitxatecnicaDescripcio());
        $this->setIdFitxaTecnicaTitol($newParams->getIdFitxaTecnicaTitol());
    }

    public function getObjectAsArray()
    {
        $return = array();
        $return['id_fitxatecnica'] = $this->getIdFitxaTecnica();
        $return['id_fitxatecnicatitol'] = $this->getIdFitxaTecnicaTitol();
        $return['id_fitxatecnicadescripcio'] = $this->getIdFitxatecnicaDescripcio();

        return $return;
    }
}