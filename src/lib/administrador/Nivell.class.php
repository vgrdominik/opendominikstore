<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 3/08/13
 * Time: 12:55
 * To change this template use File | Settings | File Templates.
 */

class Administrador_Nivell implements Administrador_SerializableObject {
    const TABLE = 'Nivell';
    private $id_nivell;
    private $descripcio;
    private $tipus;
    private $adicional;

    public function __construct($parameters)
    {
        if(is_array($parameters))
        {
            $newParameters = new stdClass();
            foreach($parameters as $key => $parameter)
            {
                $newParameters->$key = $parameter;
            }
            $parameters = $newParameters;
        }
        if(isset($parameters->id_nivell))
        {
            $this->setIdNivell($parameters->id_nivell);
        }else{
            throw new Exception('Missing parameter: id_nivell');
        }

        if(isset($parameters->descripcio))
        {
            $this->setDescripcio($parameters->descripcio);
        }else{
            throw new Exception('Missing parameter: descripcio');
        }

        if(isset($parameters->tipus))
        {
            $this->setTipus($parameters->tipus);
        }else{
            throw new Exception('Missing parameter: tipus');
        }

        if(isset($parameters->adicional))
        {
            $this->setAdicional($parameters->adicional);
        }else{
            $this->setAdicional(null);
        }
    }

    public function getIdNivell()
    {
        return $this->id_nivell;
    }

    public function setIdNivell($id_nivell)
    {
        return $this->id_nivell = $id_nivell;
    }

    public function getDescripcio()
    {
        return stripslashes($this->descripcio);
    }

    public function setDescripcio($descripcio)
    {
        return $this->descripcio = addslashes($descripcio);
    }

    public function getTipus()
    {
        return $this->tipus;
    }

    public function setTipus($tipus)
    {
        return $this->tipus = $tipus;
    }

    public function getAdicional()
    {
        return $this->adicional;
    }

    public function setAdicional($adicional)
    {
        return $this->adicional = $adicional;
    }

    public function save()
    {
        if($this->getIdNivell()!= -1)
        {
            $pdo = Administrador_DB::update(Administrador_Nivell::TABLE, $this->getObjectAsArray(), array('id_nivell = '.$this->getIdNivell()));
            return $this;
        }else{
            $pdo = Administrador_DB::insert(Administrador_Nivell::TABLE, $this->getObjectAsArray());
            $idNivell = $pdo->lastInsertId();
            $newNivellParams = Administrador_DB::getInfo(Administrador_Nivell::TABLE, '*', array('id_nivell = '.$idNivell));
            $newNivell = new Administrador_Nivell($newNivellParams[0]);
            $this->setAllParamsFromNivell($newNivell);
            return $this;
        }

    }

    public function delete()
    {
        return Administrador_DB::delete(Administrador_Nivell::TABLE, array('id_nivell = '.$this->getIdNivell()));
    }

    public function setAllParamsFromNivell(Administrador_Nivell $newParams)
    {
        $this->setIdNivell($newParams->getIdNivell());
        $this->setDescripcio($newParams->getDescripcio());
        $this->setTipus($newParams->getTipus());
        $this->setAdicional($newParams->getAdicional());
    }

    public function getObjectAsArray()
    {
        $return = array();
        $return['id_nivell'] = ($this->getIdNivell() == -1)? null : $this->getIdNivell();
        $return['descripcio'] = $this->getDescripcio();
        $return['tipus'] = $this->getTipus();
        $return['adicional'] = $this->getAdicional();

        return $return;
    }
}