<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 3/08/13
 * Time: 12:55
 * To change this template use File | Settings | File Templates.
 */

class Administrador_Familia implements Administrador_SerializableObject {
    const TABLE = 'Familia';
    private $id_familia;
    private $de_familia;
    private $descripcio;
    private $ordre;
    private $id_pagina;

    private static function buildFinalFamilyToMenu($family, $subfamilies = null)
    {
        return array(
            'id_familia' => $family->id_familia,
            'subfamilia' => $subfamilies,
            'module' => array(
                'value' => $family->descripcio_amigable
            ),
            'descripcion' => array(
                'value' => $family->descripcio
            )
        );
    }

    private static function buildFamiliesToMenu($family, $subfamilies = null)
    {
        $arraySubfamilies = array();
        foreach($subfamilies as $subfamily)
        {
            $arraySubfamilies[$subfamily->descripcio] = self::buildFinalFamilyToMenu($subfamily);
        }
        return self::buildFinalFamilyToMenu($family, $arraySubfamilies);
    }

    public function __construct($parameters)
    {
        if(is_array($parameters))
        {
            $newParameters = new stdClass();
            foreach($parameters as $key => $parameter)
            {
                $newParameters->$key = $parameter;
            }
            $parameters = $newParameters;
        }
        if(isset($parameters->id_familia))
        {
            $this->setIdFamilia($parameters->id_familia);
        }else{
            throw new Exception('Missing parameter: id_familia');
        }

        if(isset($parameters->de_familia))
        {
            if(empty($parameters->de_familia)||$parameters->de_familia=='NULL')
            {
                $this->setDeFamilia(null);
            }else{
                $this->setDeFamilia($parameters->de_familia);
            }
        }else{
            $this->setDeFamilia(null);
        }

        if(isset($parameters->id_pagina))
        {
            $this->setIdPagina($parameters->id_pagina);
        }else{
            throw new Exception('Missing parameter: id_pagina');
        }

        if(isset($parameters->descripcio))
        {
            $this->setDescripcio($parameters->descripcio);
        }else{
            throw new Exception('Missing parameter: descripcio');
        }

        if(isset($parameters->ordre))
        {
            $this->setOrdre($parameters->ordre);
        }else{
            $this->setOrdre(null);
        }
    }

    public static function getByIdPagina($id_pagina)
    {
        $pagina = Administrador_DB::getInfo(self::TABLE, '*', array('id_pagina = '.$id_pagina));

        return new Administrador_Familia($pagina[0]);
    }

    public function getIndexArticles($generalPath, $page, $limit, $order, $maxHeightArticle, $widthArticle, $nuevo = 0, $actualizado = 0, $precioRango = array())
    {
        $limitStart = $page * $limit - $limit;
        $limitEnd = $limitStart + $limit;
        $fechaActualMenos15Dias = new DateTime();
        $fechaActualMenos15Dias->modify('-15 day');
        $de_familia = $this->getDeFamilia();
        $extraTable = Administrador_familia::TABLE.' f INNER JOIN '.Administrador_Article::TABLE.' a ON f.id_familia = a.id_familia';
        if($de_familia == 0)
        {
            $extraTableCondition = Administrador_familia::TABLE.' f INNER JOIN '.Administrador_Article::TABLE.' a ON f.id_familia = a.id_familia';
            $condition = '(f.id_familia = '.$this->getIdFamilia().' OR f.de_familia = '.$this->getIdFamilia().')';
        }else{
            $extraTableCondition = Administrador_familia::TABLE.' f INNER JOIN '.Administrador_Article::TABLE.' a ON f.id_familia = a.id_familia';
            $condition = '(f.id_familia = '.$this->getDeFamilia().' OR f.de_familia = '.$this->getDeFamilia().')';
        }
        $articlesInfo = Administrador_DB::getInfo(
            $extraTable.' INNER JOIN '.Administrador_Pagina::TABLE.' p ON a.id_pagina = p.id_pagina',
            array(
                'a.*',
                'p.descripcio_amigable AS descripcio_amigable'
            ),
            array($condition),
            null,
            null,
            $order
        );

        $articlesArr = array();
        $filtresArr = array();
        $filtresArr['familia'] = array();
        $filtresArr['familia']['familiaMadre'] = count($articlesInfo);
        $filtresArr['minPrecio'] = 0;
        $filtresArr['maxPrecio'] = 0;
        $filtresArr['precio'] = array();
        $filtresArr['precioNovedad'] = array();
        $filtresArr['precioActualizacion'] = array();
        $filtresArr['novedad'] = 0;
        $filtresArr['actualizacion'] = 0;
        $countArticulosCatalogo = 0;
        $countArticulosCatalogoToFilters = 0;
        $count=0;
        $imageDefault = $generalPath.'/src/module/tienda/general/img/imagen_no_disponible.jpg';
        $sizeDefault = getimagesize('src/module/tienda/general/img/imagen_no_disponible.jpg');
        foreach($articlesInfo as $articleInfo)
        {
            // Preparación filtros de la familia madre
            $article = new Administrador_Article($articleInfo);
            if(empty($filtresArr['familia'][$articleInfo->id_familia]))
            {
                $filtresArr['familia'][$articleInfo->id_familia] = 1;
            }else{
                $filtresArr['familia'][$articleInfo->id_familia]++;
            }

            // Fin preparación filtros de la familia madre
            // Continuar solo si pertenece al catálogo actual
            if(!($articleInfo->id_familia == $this->getIdFamilia()||$de_familia==0))
            {
                continue;
            }
            $countArticulosCatalogoToFilters++;
            // Fin preparación filtros de la familia madre
            $alta = new DateTime($articleInfo->alta);
            $actualizacion = new DateTime($articleInfo->actualitzacio);
            // Preparación filtros de la familia actual
            if(empty($filtresArr['precio'][$articleInfo->precio]))
            {
                $filtresArr['minPrecio'] = ($filtresArr['minPrecio']>$articleInfo->precio||$countArticulosCatalogoToFilters==1)? (int) $articleInfo->precio : (int) $filtresArr['minPrecio'];
                $filtresArr['maxPrecio'] = ($filtresArr['maxPrecio']<$articleInfo->precio)? (int) $articleInfo->precio + 1 : (int) $filtresArr['maxPrecio'];
                $filtresArr['precio'][$articleInfo->precio]['precio'] = $articleInfo->precio;
                $filtresArr['precio'][$articleInfo->precio]['count'] = 1;
            }else{
                $filtresArr['precio'][$articleInfo->precio]['count']++;
            }
            if($alta > $fechaActualMenos15Dias)
            {
                if(empty($filtresArr['precioNovedad'][$articleInfo->precio]))
                {
                    $filtresArr['precioNovedad'][$articleInfo->precio]['precio'] = $articleInfo->precio;
                    $filtresArr['precioNovedad'][$articleInfo->precio]['count'] = 1;
                }else{
                    $filtresArr['precioNovedad'][$articleInfo->precio]['count']++;
                }
            }
            if($actualizacion > $fechaActualMenos15Dias)
            {
                if(empty($filtresArr['precioActualizacion'][$articleInfo->precio]))
                {
                    $filtresArr['precioActualizacion'][$articleInfo->precio]['precio'] = $articleInfo->precio;
                    $filtresArr['precioActualizacion'][$articleInfo->precio]['count'] = 1;
                }else{
                    $filtresArr['precioActualizacion'][$articleInfo->precio]['count']++;
                }
            }
            // Comprobamos las condiciones adicionales antes para no tener en cuenta los limites al sumar el total de artículos actuales
            if(!(empty($nuevo)||(!empty($nuevo)&&$alta>$fechaActualMenos15Dias))||
                !(empty($actualizado)||(!empty($actualizado)&&$actualizacion>$fechaActualMenos15Dias))||
                !(empty($precioRango)||(!empty($precioRango)&&$precioRango[0]<=$articleInfo->precio&&$precioRango[1]>=$articleInfo->precio))
            )
            {
                $aditionalConditions = true;
            }else{
                $countArticulosCatalogo++;
                $aditionalConditions = false;
                // Preparación filtros de la familia actual
                if($alta > $fechaActualMenos15Dias)
                {
                    $filtresArr['novedad']++;
                }
                if($actualizacion > $fechaActualMenos15Dias)
                {
                    $filtresArr['actualizacion']++;
                }
            }
            // Añadir al array de artículos solo si entra dentro de los límetes de la página a mostrar
            if(!($limitStart < $countArticulosCatalogo&&$countArticulosCatalogo <= $limitEnd)||
                $aditionalConditions
            )
            {
                continue;
            }
            $articlesArr[$count] = (array) $articleInfo;
            $articlesArr[$count]['informacio'] = substr(strip_tags($articlesArr[$count]['informacio']), 1, 150).'...';
            if(empty($articlesArr[$count]['imatge']))
            {
                $size = false;
            }else{
                $size = @getimagesize($articlesArr[$count]['imatge']);
            }
            if($size === false)
            {
                $size = $sizeDefault;
                $articlesArr[$count]['imatge'] = $imageDefault;
            }
            if($size[0]>$widthArticle)
            {
                $multiplicador = $widthArticle/$size[0];
            }else{
                $multiplicador = 1;
            }
            $height = $size[1] * $multiplicador;
            $heightSobrante = $maxHeightArticle - $height;
            $marginTop = round($heightSobrante/2);
            $articlesArr[$count]['imatge_marginTop'] = $marginTop;
            $articlesArr[$count]['url_article'] = $articleInfo->descripcio_amigable;
            $count++;
        }

        $return = new stdClass();
        $return->countArticles = $countArticulosCatalogo;
        $return->articles = $articlesArr;
        $return->filtres = $filtresArr;
        return $return;
    }

    public static function getMenu()
    {
        $familias = Administrador_DB::getInfo(
            self::TABLE.' f INNER JOIN '.Administrador_Pagina::TABLE.' p ON f.id_pagina = p.id_pagina',
            array(
                'f.id_familia AS id_familia',
                'f.de_familia AS de_familia',
                'f.descripcio AS descripcio',
                'p.descripcio_amigable AS descripcio_amigable'
            )
        );
        $menu = array();
        foreach($familias as $familia)
        {
            if($familia->de_familia === null)
            {
                $subfamilias = array();
                foreach($familias as $familia2)
                {
                    if($familia2->de_familia === $familia->id_familia)
                    {
                        $subfamilias[] = $familia2;
                    }
                }
                $menu[$familia->descripcio] = self::buildFamiliesToMenu($familia, $subfamilias);
            }
        }
        return $menu;
    }

    public function getIdFamilia()
    {
        return $this->id_familia;
    }

    public function setIdFamilia($id_familia)
    {
        return $this->id_familia = $id_familia;
    }

    public function getDeFamilia()
    {
        return $this->de_familia;
    }

    public function setDeFamilia($de_familia)
    {
        return $this->de_familia = $de_familia;
    }

    public function getIdPagina()
    {
        return $this->id_pagina;
    }

    public function setIdPagina($id_pagina)
    {
        return $this->id_pagina = $id_pagina;
    }

    public function getDescripcio()
    {
        return $this->descripcio;
    }

    public function setDescripcio($descripcio)
    {
        return $this->descripcio = $descripcio;
    }

    public function getOrdre()
    {
        return $this->ordre;
    }

    public function setOrdre($ordre)
    {
        return $this->ordre = $ordre;
    }

    public function save()
    {
        if($this->getIdFamilia()!= -1)
        {
            $pdo = Administrador_DB::update(Administrador_Familia::TABLE, $this->getObjectAsArray(), array('id_familia = '.$this->getIdFamilia()));
            return $this;
        }else{
            $pdo = Administrador_DB::insert(Administrador_Familia::TABLE, $this->getObjectAsArray());
            $idFamily = $pdo->lastInsertId();
            $newFamilyParams = Administrador_DB::getInfo(Administrador_Familia::TABLE, '*', array('id_familia = '.$idFamily));
            $newFamily = new Administrador_Familia($newFamilyParams[0]);
            $this->setAllParamsFromFamily($newFamily);
            return $this;
        }

    }

    public function delete()
    {
        return Administrador_DB::delete(Administrador_Familia::TABLE, array('id_familia = '.$this->getIdFamilia()));
    }

    public function setAllParamsFromFamily(Administrador_Familia $newParams)
    {
        $this->setIdFamilia($newParams->getIdFamilia());
        $this->setDeFamilia($newParams->getDeFamilia());
        $this->setIdPagina($newParams->getIdPagina());
        $this->setDescripcio($newParams->getDescripcio());
        $this->setOrdre($newParams->getOrdre());
    }

    public function getObjectAsArray()
    {
        $return = array();
        $return['id_familia'] = ($this->getIdFamilia() == -1)? null : $this->getIdFamilia();
        $return['de_familia'] = $this->getDeFamilia();
        $idPagina = $this->getIdPagina();
        $return['id_pagina'] = (empty($idPagina))? null : $this->getIdPagina();;
        $return['descripcio'] = $this->getDescripcio();
        $return['ordre'] = $this->getOrdre();

        return $return;
    }
}