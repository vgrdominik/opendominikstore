<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 3/08/13
 * Time: 12:55
 * To change this template use File | Settings | File Templates.
 */

class Administrador_Fabricant implements Administrador_SerializableObject {
    const TABLE = 'Fabricant';
    private $id_fabricant;
    private $descripcio;
    private $imatge;

    public function __construct($parameters)
    {
        if(is_array($parameters))
        {
            $newParameters = new stdClass();
            foreach($parameters as $key => $parameter)
            {
                $newParameters->$key = $parameter;
            }
            $parameters = $newParameters;
        }
        if(isset($parameters->id_fabricant))
        {
            $this->setIdFabricant($parameters->id_fabricant);
        }else{
            throw new Exception('Missing parameter: id_fabricant');
        }

        if(isset($parameters->descripcio))
        {
            $this->setDescripcio($parameters->descripcio);
        }else{
            throw new Exception('Missing parameter: descripcio');
        }

        if(isset($parameters->imatge))
        {
            $this->setImatge($parameters->imatge);
        }else{
            throw new Exception('Missing parameter: imatge');
        }
    }

    public function getIdFabricant()
    {
        return $this->id_fabricant;
    }

    public function setIdFabricant($id_fabricant)
    {
        return $this->id_fabricant = $id_fabricant;
    }

    public function getDescripcio()
    {
        return stripslashes($this->descripcio);
    }

    public function setDescripcio($descripcio)
    {
        return $this->descripcio = addslashes($descripcio);
    }

    public function getImatge()
    {
        return $this->imatge;
    }

    public function setImatge($imatge)
    {
        return $this->imatge = $imatge;
    }

    public function save()
    {
        if($this->getIdFabricant()!= -1)
        {
            $pdo = Administrador_DB::update(Administrador_Fabricant::TABLE, $this->getObjectAsArray(), array('id_fabricant = '.$this->getIdFabricant()));
            return $this;
        }else{
            $pdo = Administrador_DB::insert(Administrador_Fabricant::TABLE, $this->getObjectAsArray());
            $idFabricant = $pdo->lastInsertId();
            $newFabricantParams = Administrador_DB::getInfo(Administrador_Fabricant::TABLE, '*', array('id_fabricant = '.$idFabricant));
            $newFabricant = new Administrador_Fabricant($newFabricantParams[0]);
            $this->setAllParamsFromFabricant($newFabricant);
            return $this;
        }

    }

    public function delete()
    {
        return Administrador_DB::delete(Administrador_Fabricant::TABLE, array('id_fabricant = '.$this->getIdFabricant()));
    }

    public function setAllParamsFromFabricant(Administrador_Fabricant $newParams)
    {
        $this->setIdFabricant($newParams->getIdFabricant());
        $this->setDescripcio($newParams->getDescripcio());
        $this->setImatge($newParams->getImatge());
    }

    public function getObjectAsArray()
    {
        $return = array();
        $return['id_fabricant'] = ($this->getIdFabricant() == -1)? null : $this->getIdFabricant();
        $return['descripcio'] = $this->getDescripcio();
        $return['imatge'] = $this->getImatge();

        return $return;
    }
}