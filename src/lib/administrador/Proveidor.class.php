<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 3/08/13
 * Time: 12:55
 * To change this template use File | Settings | File Templates.
 */

class Administrador_Proveidor implements Administrador_SerializableObject {
    const TABLE = 'Proveidor';
    private $id_proveidor;
    private $descripcio;
    private $plac;

    public function __construct($parameters)
    {
        if(is_array($parameters))
        {
            $newParameters = new stdClass();
            foreach($parameters as $key => $parameter)
            {
                $newParameters->$key = $parameter;
            }
            $parameters = $newParameters;
        }
        if(isset($parameters->id_proveidor))
        {
            $this->setIdProveidor($parameters->id_proveidor);
        }else{
            throw new Exception('Missing parameter: id_proveidor');
        }

        if(isset($parameters->descripcio))
        {
            $this->setDescripcio($parameters->descripcio);
        }else{
            throw new Exception('Missing parameter: descripcio');
        }

        if(isset($parameters->plac))
        {
            $this->setPlac($parameters->plac);
        }else{
            throw new Exception('Missing parameter: plac');
        }
    }

    public function getIdProveidor()
    {
        return $this->id_proveidor;
    }

    public function setIdProveidor($id_proveidor)
    {
        return $this->id_proveidor = $id_proveidor;
    }

    public function getDescripcio()
    {
        return stripslashes($this->descripcio);
    }

    public function setDescripcio($descripcio)
    {
        return $this->descripcio = addslashes($descripcio);
    }

    public function getPlac()
    {
        return $this->plac;
    }

    public function setPlac($plac)
    {
        return $this->plac = $plac;
    }

    public function save()
    {
        if($this->getIdProveidor()!= -1)
        {
            $pdo = Administrador_DB::update(Administrador_Proveidor::TABLE, $this->getObjectAsArray(), array('id_proveidor = '.$this->getIdProveidor()));
            return $this;
        }else{
            $pdo = Administrador_DB::insert(Administrador_Proveidor::TABLE, $this->getObjectAsArray());
            $idProveidor = $pdo->lastInsertId();
            $newProveidorParams = Administrador_DB::getInfo(Administrador_Proveidor::TABLE, '*', array('id_proveidor = '.$idProveidor));
            $newProveidor = new Administrador_Proveidor($newProveidorParams[0]);
            $this->setAllParamsFromProveidor($newProveidor);
            return $this;
        }

    }

    public function delete()
    {
        return Administrador_DB::delete(Administrador_Proveidor::TABLE, array('id_proveidor = '.$this->getIdProveidor()));
    }

    public function setAllParamsFromProveidor(Administrador_Proveidor $newParams)
    {
        $this->setIdProveidor($newParams->getIdProveidor());
        $this->setDescripcio($newParams->getDescripcio());
        $this->setPlac($newParams->getPlac());
    }

    public function getObjectAsArray()
    {
        $return = array();
        $return['id_proveidor'] = ($this->getIdProveidor() == -1)? null : $this->getIdProveidor();
        $return['descripcio'] = $this->getDescripcio();
        $return['plac'] = $this->getPlac();

        return $return;
    }
}