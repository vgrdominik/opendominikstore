<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 3/08/13
 * Time: 12:55
 * To change this template use File | Settings | File Templates.
 */

class Administrador_Imatge implements Administrador_SerializableObject {
    const TABLE = 'Imatge';
    private $id_imatge;
    private $id_indexpaginaprincipal;
    private $id_promocio;
    private $url;

    public function __construct($parameters)
    {
        if(is_array($parameters))
        {
            $newParameters = new stdClass();
            foreach($parameters as $key => $parameter)
            {
                $newParameters->$key = $parameter;
            }
            $parameters = $newParameters;
        }
        if(isset($parameters->id_imatge))
        {
            $this->setIdImatge($parameters->id_imatge);
        }else{
            throw new Exception('Missing parameter: id_imatge');
        }

        if(isset($parameters->id_indexpaginaprincipal))
        {
            $this->setIdIndexPaginaPrincipal($parameters->id_indexpaginaprincipal);
        }else{
            $this->setIdIndexPaginaPrincipal(null);
        }

        if(isset($parameters->id_promocio))
        {
            $this->setIdPromocio($parameters->id_promocio);
        }else{
            $this->setIdPromocio(null);
        }

        if(isset($parameters->url))
        {
            $this->setUrl($parameters->url);
        }else{
            throw new Exception('Missing parameter: url');
        }
    }

    public function getIdImatge()
    {
        return $this->id_imatge;
    }

    public function setIdImatge($id_imatge)
    {
        return $this->id_imatge = $id_imatge;
    }

    public function getIdPromocio()
    {
        return $this->id_promocio;
    }

    public function setIdPromocio($id_promocio)
    {
        return $this->id_promocio = $id_promocio;
    }

    public function getIdIndexPaginaPrincipal()
    {
        return $this->id_indexpaginaprincipal;
    }

    public function setIdIndexPaginaPrincipal($id_indexpaginaprincipal)
    {
        return $this->id_indexpaginaprincipal = $id_indexpaginaprincipal;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        return $this->url = $url;
    }

    public function save()
    {
        if($this->getIdImatge()!= -1)
        {
            $pdo = Administrador_DB::update(Administrador_Imatge::TABLE, $this->getObjectAsArray(), array('id_imatge = '.$this->getIdImatge()));
            return $this;
        }else{
            $pdo = Administrador_DB::insert(Administrador_Imatge::TABLE, $this->getObjectAsArray());
            $idImatge = $pdo->lastInsertId();
            $newImatgeParams = Administrador_DB::getInfo(Administrador_Imatge::TABLE, '*', array('id_imatge = '.$idImatge));
            $newImatge = new Administrador_Imatge($newImatgeParams[0]);
            $this->setAllParamsFromImatge($newImatge);
            return $this;
        }

    }

    public function delete()
    {
        return Administrador_DB::delete(Administrador_Imatge::TABLE, array('id_imatge = '.$this->getIdImatge()));
    }

    public function setAllParamsFromImatge(Administrador_Imatge $newParams)
    {
        $this->setIdImatge($newParams->getIdImatge());
        $this->setIdIndexPaginaPrincipal($newParams->getIdIndexPaginaPrincipal());
        $this->getIdPromocio($newParams->getIdPromocio());
        $this->setUrl($newParams->getUrl());
    }

    public function getObjectAsArray()
    {
        $return = array();
        $return['id_imatge'] = ($this->getIdImatge() == -1)? null : $this->getIdImatge();
        $idPaginaPrincipal = $this->getIdIndexPaginaPrincipal();
        $return['id_indexpaginaprincipal'] = (empty($idPaginaPrincipal))? null : $this->getIdIndexPaginaPrincipal();
        $idPromocio = $this->getIdPromocio();
        $return['id_promocio'] = (empty($idPromocio))? null : $this->getIdPromocio();
        $return['url'] = $this->getUrl();

        return $return;
    }
}