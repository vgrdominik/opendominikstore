<?php
class Administrador_Module implements Administrador_SerializableObject{
	const TABLE = "Module";
	private $id_module;
	private $id_rol;
	private $id_usuario;
	private $rol;
	private $usuario;
	private $module;
	private $permissions;

	public function __construct($parameters, $rol = null, $usuario = null)
	{

		if(is_array($parameters))
        {
            $newParameters = new stdClass();
            foreach($parameters as $key => $parameter)
            {
                $newParameters->$key = $parameter;
            }
            $parameters = $newParameters;
        }

        $this->setUsuario($usuario);
        $this->setRol($rol);
        if(!empty($parameters->id_module))
        {
            $this->setIdModule($parameters->id_module);
        }else{
            throw new Exception('Missing parameter: id_module');
        }
        if(!empty($parameters->id_rol))
        {
            $this->setIdRol($parameters->id_rol);
        }else{
            throw new Exception('Missing parameter: id_rol');
        }
        if(!empty($parameters->id_usuario))
        {
            $this->setIdUsuario($parameters->id_usuario);
        }else{
            throw new Exception('Missing parameter: id_usuario');
        }
        if(!empty($parameters->module))
        {
            $this->setModule($parameters->module);
        }else{
            throw new Exception('Missing parameter: module');
        }
        if(!empty($parameters->permissions))
        {
            $this->setPermissions($parameters->permissions);
        }else{
            throw new Exception('Missing parameter: permissions');
        }
	}


	public function getIdModule()
    {
        return $this->id_module;
    }
	public function setIdModule($id_module)
    {
        return $this->id_module = $id_module;
    }
	public function getIdRol()
    {
        return $this->id_rol;
    }
	public function setIdRol($id_rol)
    {
        return $this->id_rol = $id_rol;
    }
	public function getIdUsuario()
    {
        return $this->id_usuario;
    }
	public function setIdUsuario($id_usuario)
    {
        return $this->id_usuario = $id_usuario;
    }
    public function getRol()
    {
        return $this->rol;
    }
    public function setRol($rol)
    {
        return $this->rol = $rol;
    }
    public function getUsuario()
    {
        return $this->usuario;
    }
    public function setUsuario($usuario)
    {
        return $this->usuario = $usuario;
    }
	public function getModule()
    {
        return $this->module;
    }
	public function setModule($rol)
    {
        return $this->module = $rol;
    }
    public function getPermissions()
    {
        return $this->permissions;
    }
	public function setPermissions($permissions)
    {
        return $this->permissions = $permissions;
    }

    public function getObjectAsArray()
    {
    	$return = array();
    	$return['id_module'] = ($this->getIdModule() == -1)? null : $this->getIdModule();
    	$return['id_rol'] = $this->getIdRol();
    	$return['id_usuario'] = $this->getIdUsuario();
    	$return['rol'] = $this->getRol();
    	$return['usuario'] = $this->getUsuario();
    	$return['module'] = $this->getModule();
    	$return['permissions'] = $this->getPermissions();

    	return $return;
    }

    public function getObjectAsArrayOnlyMainParameters()
    {
    	$return = array();
    	$return['id_module'] = ($this->getIdModule() == -1)? null : $this->getIdModule();
    	$return['id_rol'] = $this->getIdRol();
    	$return['id_usuario'] = $this->getIdUsuario();
    	$return['module'] = $this->getModule();
    	$return['permissions'] = $this->getPermissions();

    	return $return;
    }

    private function setAllParametersFromModule(Administrador_Module $newParameters)
    {
    	$this->setIdModule($newParameters->getIdModule());
    	$this->setIdRol($newParameters->getIdRol());
    	$this->setIdUsuario($newParameters->getIdusuario());
    	$this->setModule($newParameters->getModule());
    	$this->setPermissions($newParameters->getPermissions());
    }

    public function save()
    {
        if ($this->getIdModule() != -1)
        {
            $pdo = Administrador_DB::update(Administrador_Module::TABLE,$this->getObjectAsArrayOnlyMainParameters(),Array('id_module = '.$this->getIdModule()));
        }else{
            $pdo = Administrador_DB::insert(Administrador_Module::TABLE,$this->getObjectAsArrayOnlyMainParameters());
            $idModule = $pdo->lastInsertId();
            $newModuleParams = Administrador_DB::getInfo(Administrador_Module::TABLE,'*',Array('id_module = '.$idModule));
            if(empty($newModuleParams[0])){
                throw new Exception("Module already exist");
            }

            $newModule = new Administrador_Module($newModuleParams[0]);
            $this->setAllParametersFromModule($newModule);
        }
        return $this;
    }

    public function delete()
    {
        return Administrador_DB::delete(Administrador_Module::TABLE,Array('id_module = '.$this->getIdModule()));
    }


}





 ?>