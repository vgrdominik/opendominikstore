<?php
/**
 * User: vgrdominik
 * Date: 31/12/12
 * Time: 13:27
 */

interface Administrador_SerializableObject
{
    public function getObjectAsArray();
}