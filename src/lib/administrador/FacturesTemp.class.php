<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 3/08/13
 * Time: 12:55
 * To change this template use File | Settings | File Templates.
 */

class Administrador_FacturesTemp implements Administrador_SerializableObject {
    const TABLE = 'FacturesTemp';
    const TABLE_ARTICLES = 'FacturesTempArticle';
    private $id_facturestemp;
    private $codi_sessio;
    private $data;
    private $forma_pagament;
    private $total;
    private $index;

    public function __construct($parameters, $index)
    {
        if(is_array($parameters))
        {
            $newParameters = new stdClass();
            foreach($parameters as $key => $parameter)
            {
                $newParameters->$key = $parameter;
            }
            $parameters = $newParameters;
        }
        if(isset($parameters->id_facturestemp))
        {
            $this->setIdFacturesTemp($parameters->id_facturestemp);
            if(!empty($index))
            {
                $this->setIndex($index);
            }else{
                $this->setIndex(array());
            }
        }else{
            throw new Exception('Missing parameter: id_facturestemp');
        }

        if(isset($parameters->codi_sessio))
        {
            $this->setCodiSessio($parameters->codi_sessio);
        }else{
            throw new Exception('Missing parameter: codi_sessio');
        }

        if(isset($parameters->data))
        {
            $this->setData($parameters->data);
        }else{
            throw new Exception('Missing parameter: data');
        }

        if(isset($parameters->forma_pagament))
        {
            $this->setFormaPagament($parameters->forma_pagament);
        }else{
            $this->setFormaPagament(null);
        }

        if(isset($parameters->total))
        {
            $this->setTotal($parameters->total);
        }else{
            throw new Exception('Missing parameter: total');
        }
    }

    public static function getByCodiSessio($codi_sessio)
    {
        $facturesTemp = Administrador_DB::getInfo(self::TABLE, '*', array('codi_sessio = "'.$codi_sessio.'"'));
        if(!empty($facturesTemp))
        {
            $facturesTempArticle = Administrador_DB::getInfo(self::TABLE_ARTICLES, '*', array('id_facturestemp = '.$facturesTemp[0]->id_facturestemp));

            return new Administrador_FacturesTemp($facturesTemp[0], $facturesTempArticle);
        }else{
            return null;
        }
    }

    public static function getIndexFromDB($idFacturesTemp)
    {
        $index = Administrador_DB::getInfo(Administrador_FacturesTemp::TABLE_ARTICLES, '*', array('id_facturestemp = '.$idFacturesTemp));
        $return = array();
        foreach($index as $element)
        {
            $return[$element->id_facturestemparticle] = $element;
        }
        return $return;
    }

    public function getIndex()
    {
        if(empty($this->index))
        {
            return array();
        }

        return $this->index;
    }

    public function deleteIndexElement($id_facturestemparticle)
    {
        unset($this->index[$id_facturestemparticle]);
        return true;
    }

    public function setIndex($index)
    {
        $arrayIndex = array();
        foreach($index as $element)
        {
            if(is_array($element))
            {
                $newParameters = new stdClass();
                foreach($element as $key => $parameter)
                {
                    $newParameters->$key = $parameter;
                }
                $element = $newParameters;
            }

            if(empty($element->id_facturestemparticle))
            {
                $element->id_facturestemparticle = null;
            }

            if(empty($element->id_facturestemp))
            {
                throw new Exception('Missing id_facturestemp');
            }

            if(!isset($element->descripcio))
            {
                throw new Exception('Missing descripcio');
            }

            if(!isset($element->iva))
            {
                throw new Exception('Missing iva');
            }

            if(!isset($element->nivell))
            {
                $element->nivell = null;
            }

            if(!isset($element->id_article))
            {
                throw new Exception('Missing id_article');
            }

            if(!isset($element->quantitat))
            {
                throw new Exception('Missing quantitat');
            }

            if(!isset($element->preu))
            {
                throw new Exception('Missing preu');
            }

            if(!isset($element->descompte))
            {
                $element->descompte = null;
            }

            if(!isset($element->referencia_propia))
            {
                $element->referencia_propia = null;
            }

            if(!isset($element->referencia_proveidor))
            {
                $element->referencia_proveidor = null;
            }

            if(!isset($element->data))
            {
                throw new Exception('Missing data');
            }

            $article = Administrador_Article::getById($element->id_article);
            $arrayIndex[$element->id_facturestemparticle]['imatge'] = $article->getImatge();
            $arrayIndex[$element->id_facturestemparticle]['url_article'] = $article->getPagina();

            $arrayIndex[$element->id_facturestemparticle]['id_facturestemparticle'] = $element->id_facturestemparticle;
            $arrayIndex[$element->id_facturestemparticle]['id_facturestemp'] = $element->id_facturestemp;
            $arrayIndex[$element->id_facturestemparticle]['id_article'] = $element->id_article;
            $arrayIndex[$element->id_facturestemparticle]['descripcio'] = $element->descripcio;
            $arrayIndex[$element->id_facturestemparticle]['iva'] = $element->iva;
            $arrayIndex[$element->id_facturestemparticle]['nivell'] = $element->nivell;
            $arrayIndex[$element->id_facturestemparticle]['quantitat'] = $element->quantitat;
            $arrayIndex[$element->id_facturestemparticle]['preu'] = $element->preu;
            $arrayIndex[$element->id_facturestemparticle]['descompte'] = $element->descompte;
            $arrayIndex[$element->id_facturestemparticle]['referencia_propia'] = $element->referencia_propia;
            $arrayIndex[$element->id_facturestemparticle]['referencia_proveidor'] = $element->referencia_proveidor;
            $arrayIndex[$element->id_facturestemparticle]['data'] = $element->data;

        }
        return $this->index = $arrayIndex;
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function setTotal($total)
    {
        return $this->total = $total;
    }

    public function getIdFacturesTemp()
    {
        return $this->id_facturestemp;
    }

    public function setIdFacturesTemp($id_facturestemp)
    {
        return $this->id_facturestemp = $id_facturestemp;
    }

    public function getCodiSessio()
    {
        return $this->codi_sessio;
    }

    public function setCodiSessio($codi_sessio)
    {
        return $this->codi_sessio = $codi_sessio;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data)
    {
        return $this->data = $data;
    }

    public function getFormaPagament()
    {
        return $this->forma_pagament;
    }

    public function setFormaPagament($formaPagament)
    {
        return $this->forma_pagament = $formaPagament;
    }

    public function refresh($idFacturesTemp = null)
    {
        $idFacturesTemp = ($idFacturesTemp !== null)? $idFacturesTemp : $this->getIdFacturesTemp();
        $paramsToFacturesTemp = Administrador_DB::getInfo(Administrador_FacturesTemp::TABLE, '*', array('id_facturestemp = '.$idFacturesTemp));
        $index = Administrador_FacturesTemp::getIndexFromDB($idFacturesTemp);
        if(!isset($paramsToFacturesTemp[0]))
        {
            throw new Exception('Error to refresh factures temp');
        }
        $newFacturesTemp = new Administrador_FacturesTemp($paramsToFacturesTemp[0], $index);
        $this->setAllParamsFromDB($newFacturesTemp);
    }

    public static function saveElement($id_facturestemp, $id_facturestemparticle, $quantitat, $id_article = null)
    {
        if($id_facturestemparticle === null)
        {
            $id_facturestemparticleToSelect = '';
        }else{
            $id_facturestemparticleToSelect = $id_facturestemparticle;
        }
        $newElement = Administrador_DB::getInfo(Administrador_FacturesTemp::TABLE_ARTICLES, "*", array(
            "id_facturestemp = $id_facturestemp",
            "id_facturestemparticle = $id_facturestemparticleToSelect",
        ));

        if(empty($newElement[0]))
        {
            if($id_article === null)
            {
                throw new Exception('Missing parameter id_article');
            }
            $article = Administrador_Article::getById($id_article);

            $pdo = Administrador_DB::insert(Administrador_FacturesTemp::TABLE_ARTICLES, array(
                "id_facturestemp" => $id_facturestemp,
                "id_facturestemparticle" => $id_facturestemparticle,
                "descripcio" => $article->getDescripcio(),
                "nivell" => $article->getNivell(),
                "preu" => $article->getPrecio(),
                "descompte" => $article->getPreuOferta(),
                "referencia_propia" => $article->getReferenciaPropia(),
                "referencia_proveidor" => $article->getReferenciaProveidor(),
                "iva" => $article->getIva(),
                "id_article" => $article->getIdArticle(),
                "data" => date('Y-m-d H:i:s'),
                "quantitat" => $quantitat
            ));

            $return = 'Elemento creado';
        }else{
            $pdo = Administrador_DB::update(Administrador_FacturesTemp::TABLE_ARTICLES, array(
                    "quantitat" => $quantitat
                ),
                array(
                    "id_facturestemp = ".$id_facturestemp,
                    "id_facturestemparticle = ".$id_facturestemparticle
                )
            );

            $return = 'Elemento modificado';
        }

        $allElements = Administrador_DB::getInfo(Administrador_FacturesTemp::TABLE_ARTICLES, "*", array(
            "id_facturestemp = $id_facturestemp"
        ));
        $total = 0;
        foreach($allElements as $element)
        {
            $total += $element->preu * $element->quantitat;
        }
        $pdo = Administrador_DB::update(Administrador_FacturesTemp::TABLE, array(
                "total" => $total
            ),
            array(
                "id_facturestemp = ".$id_facturestemp
            )
        );

        return $return;
    }

    public function saveIndex($idFacturesTemp = null)
    {
        $index = $this->getObjectAsArrayOnlyIndexToDB();
        $pdo = Administrador_DB::delete(Administrador_FacturesTemp::TABLE_ARTICLES, array('id_facturestemp = '.$this->getIdFacturesTemp()));
        $total = 0;
        foreach($index['index'] as $element)
        {
            $total += $element['preu'] * $element['quantitat'];
            $element['id_facturestemp'] = ($idFacturesTemp !== null)? $idFacturesTemp : $this->getIdFacturesTemp();
            $pdo = Administrador_DB::insert(Administrador_FacturesTemp::TABLE_ARTICLES, $element);
        }

        $pdo = Administrador_DB::update(Administrador_FacturesTemp::TABLE, array(
                "total" => $total
            ),
            array(
                "id_facturestemp = ".$this->getIdFacturesTemp()
            )
        );
    }

    public function save()
    {
        if($this->getIdFacturesTemp()!= -1)
        {
            $pdo = Administrador_DB::update(Administrador_FacturesTemp::TABLE, $this->getObjectAsArrayOnlyFacturesTemp(), array('id_facturestemp = '.$this->getIdFacturesTemp()));
            $this->saveIndex();
            $this->refresh();
            return $this;
        }else{
            $pdo = Administrador_DB::insert(Administrador_FacturesTemp::TABLE, $this->getObjectAsArrayOnlyFacturesTemp());
            $idFacturesTemp = $pdo->lastInsertId();
            $this->saveIndex($idFacturesTemp);
            $this->refresh($idFacturesTemp);
            return $this;
        }
    }

    public function delete()
    {
        return Administrador_DB::delete(Administrador_FacturesTemp::TABLE, array('id_facturestemp = '.$this->getIdFacturesTemp()));
    }

    public function setAllParamsFromDB(Administrador_FacturesTemp $newParams)
    {
        $this->setIdFacturesTemp($newParams->getIdFacturesTemp());
        $this->setCodiSessio($newParams->getCodiSessio());
        $this->setData($newParams->getData());
        $this->setFormaPagament($newParams->getFormaPagament());
        $this->setTotal($newParams->getTotal());
        $this->setIndex($newParams->getIndex());
    }

    public function getObjectAsArray($generalPath = null)
    {
        $return = array();
        $return['id_facturestemp'] = ($this->getIdFacturesTemp() == -1)? null : $this->getIdFacturesTemp();
        $return['codi_sessio'] = $this->getCodiSessio();
        $return['data'] = $this->getData();
        $return['forma_pagament'] = $this->getFormaPagament();
        $return['total'] = $this->getTotal();
        $return['index'] = $this->getIndex();
        if($generalPath !== null)
        {
            foreach($return['index'] as $key => $article)
            {
                $return['index'][$key]['subtotal'] = number_format($article['quantitat'] * $article['preu'], 2, '.', '');
                if(empty($article['imatge']))
                {
                    $size = false;
                }else{
                    $size = @getimagesize($article['imatge']);
                }
                if($size === false)
                {
                    $return['index'][$key]['imatge'] = $generalPath.'/src/module/tienda/general/img/imagen_no_disponible.jpg';
                }
            }
        }

        return $return;
    }

    public function getObjectAsArrayOnlyFacturesTemp()
    {
        $return = array();
        $return['id_facturestemp'] = ($this->getIdFacturesTemp() == -1)? null : $this->getIdFacturesTemp();
        $return['codi_sessio'] = $this->getCodiSessio();
        $return['data'] = $this->getData();
        $return['forma_pagament'] = $this->getFormaPagament();
        $return['total'] = $this->getTotal();

        return $return;
    }

    public function getObjectAsArrayOnlyIndex($generalPath)
    {
        $return = array();
        $return['index'] = $this->getIndex();

        foreach($return['index'] as $key => $article)
        {
            $return['index'][$key]['subtotal'] = number_format($article['quantitat'] * $article['preu'], 2, '.', '');
            if(empty($article['imatge']))
            {
                $size = false;
            }else{
                $size = @getimagesize($article['imatge']);
            }
            if($size === false)
            {
                $return['index'][$key]['imatge'] = $generalPath.'/src/module/tienda/general/img/imagen_no_disponible.jpg';
            }
        }

        return $return;
    }

    public function getObjectAsArrayOnlyIndexToDB()
    {
        $return = array();
        $index = $this->getIndex();
        $return['index'] = array();
        foreach($index as $element)
        {
            $return['index'][$element['id_facturestemparticle']]['id_facturestemparticle'] = $element['id_facturestemparticle'];
            $return['index'][$element['id_facturestemparticle']]['id_facturestemp'] = $element['id_facturestemp'];
            $return['index'][$element['id_facturestemparticle']]['id_article'] = $element['id_article'];
            $return['index'][$element['id_facturestemparticle']]['descripcio'] = $element['descripcio'];
            $return['index'][$element['id_facturestemparticle']]['iva'] = $element['iva'];
            $return['index'][$element['id_facturestemparticle']]['nivell'] = $element['nivell'];
            $return['index'][$element['id_facturestemparticle']]['quantitat'] = $element['quantitat'];
            $return['index'][$element['id_facturestemparticle']]['preu'] = $element['preu'];
            $return['index'][$element['id_facturestemparticle']]['descompte'] = $element['descompte'];
            $return['index'][$element['id_facturestemparticle']]['referencia_propia'] = $element['referencia_propia'];
            $return['index'][$element['id_facturestemparticle']]['referencia_proveidor'] = $element['referencia_proveidor'];
            $return['index'][$element['id_facturestemparticle']]['data'] = $element['data'];
        }

        return $return;
    }
}