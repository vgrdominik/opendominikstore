<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 3/08/13
 * Time: 12:55
 * To change this template use File | Settings | File Templates.
 */

class Administrador_Especial implements Administrador_SerializableObject {
    const TABLE = 'Especial';
    private $id_especial;
    private $descripcio;

    public function __construct($parameters)
    {
        if(is_array($parameters))
        {
            $newParameters = new stdClass();
            foreach($parameters as $key => $parameter)
            {
                $newParameters->$key = $parameter;
            }
            $parameters = $newParameters;
        }
        if(isset($parameters->id_especial))
        {
            $this->setIdEspecial($parameters->id_especial);
        }else{
            throw new Exception('Missing parameter: id_especial');
        }

        if(isset($parameters->descripcio))
        {
            $this->setDescripcio($parameters->descripcio);
        }else{
            throw new Exception('Missing parameter: descripcio');
        }
    }

    public function getIdEspecial()
    {
        return $this->id_especial;
    }

    public function setIdEspecial($id_especial)
    {
        return $this->id_especial = $id_especial;
    }

    public function getDescripcio()
    {
        return stripslashes($this->descripcio);
    }

    public function setDescripcio($descripcio)
    {
        return $this->descripcio = addslashes($descripcio);
    }

    public function save()
    {
        if($this->getIdEspecial()!= -1)
        {
            $pdo = Administrador_DB::update(Administrador_Especial::TABLE, $this->getObjectAsArray(), array('id_especial = '.$this->getIdEspecial()));
            return $this;
        }else{
            $pdo = Administrador_DB::insert(Administrador_Especial::TABLE, $this->getObjectAsArray());
            $idEspecial = $pdo->lastInsertId();
            $newEspecialParams = Administrador_DB::getInfo(Administrador_Especial::TABLE, '*', array('id_especial = '.$idEspecial));
            $newEspecial = new Administrador_Especial($newEspecialParams[0]);
            $this->setAllParamsFromEspecial($newEspecial);
            return $this;
        }

    }

    public function delete()
    {
        return Administrador_DB::delete(Administrador_Especial::TABLE, array('id_especial = '.$this->getIdEspecial()));
    }

    public function setAllParamsFromEspecial(Administrador_Especial $newParams)
    {
        $this->setIdEspecial($newParams->getIdEspecial());
        $this->setDescripcio($newParams->getDescripcio());
    }

    public function getObjectAsArray()
    {
        $return = array();
        $return['id_especial'] = ($this->getIdEspecial() == -1)? null : $this->getIdEspecial();
        $return['descripcio'] = $this->getDescripcio();

        return $return;
    }
}