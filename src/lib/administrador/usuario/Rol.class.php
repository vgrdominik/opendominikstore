<?php
class Administrador_Usuario_Rol implements Administrador_SerializableObject{
	const TABLE = "Rol";
    private $id_rol;
	private $rol;
    private $usuarios = array();

	public function __construct($parameters)
	{

		if(is_array($parameters))
        {
            $newParameters = new stdClass();
            foreach($parameters as $key => $parameter)
            {
                $newParameters->$key = $parameter;
            }
            $parameters = $newParameters;
        }

        if(!empty($parameters->id_rol))
        {
            $this->setIdRol($parameters->id_rol);
        }else{
            throw new Exception('Missing parameter: id_rol');
        }
        if(!empty($parameters->rol))
        {
            $this->setRol($parameters->rol);
        }else{
            throw new Exception('Missing parameter: rol');
        }
        if(!empty($parameters->usuarios))
        {
            $this->setUsuarios($parameters->usuarios);
        }
	}


	public function getIdRol()
    {
        return $this->id_rol;
    }
	public function setIdRol($id_rol)
    {
        return $this->id_rol = $id_rol;
    }
	public function getRol()
    {
        return $this->rol;
    }
	public function setRol($rol)
    {
        return $this->rol = $rol;
    }
    public function getUsuarios()
    {
        $usuariosAsArray = array();
        foreach($this->usuarios as $usuario)
        {
            $usuariosAsArray[] = $usuario->getObjectAsArray();
        }
        return $usuariosAsArray;
    }
    public function setUsuarios($usuarios)
    {
        $this->usuarios = array();
        foreach ($usuarios as $usuario) {
            if(is_array($usuario))
            {
                $newParameters = new stdClass();
                foreach($usuario as $key => $parameter)
                {
                    $newParameters->$key = $parameter;
                }
                $usuario = $newParameters;
            }

            if(get_class($usuario) === 'Administrador_Usuario_Usuario')
            {
                $this->usuarios[$usuario->getIdUsuario()] = $usuario;
            }else{
                $this->usuarios[$usuario->id_usuario] = new Administrador_Usuario_Usuario($usuario, true);
            }
        }
        return $this->usuarios;
    }

    public function getObjectAsArray()
    {
    	$return = array();
    	$return['id_rol'] = ($this->getIdRol() == -1)? null : $this->getIdRol();
    	$return['rol'] = $this->getRol();
        $return['usuarios'] = $this->getUsuarios();

    	return $return;
    }

    public function getObjectAsArrayOnlyMainParameters()
    {
        $return = array();
        $return['id_rol'] = ($this->getIdRol() == -1)? null : $this->getIdRol();
        $return['rol'] = $this->getRol();

        return $return;
    }

    private function setAllParametersFromRol(Administrador_Usuario_Rol $newParameters)
    {
    	$this->setIdRol($newParameters->getIdRol());
    	$this->setRol($newParameters->getRol());
        $this->setUsuarios($newParameters->getUsuarios());
    }

    public function save()
    {
        if ($this->getIdRol() != -1)
        {
            $pdo = Administrador_DB::update(Administrador_Usuario_Rol::TABLE,$this->getObjectAsArrayOnlyMainParameters(),Array('id_rol = '.$this->getIdRol()));
        }else{
            $pdo = Administrador_DB::insert(Administrador_Usuario_Rol::TABLE,$this->getObjectAsArrayOnlyMainParameters());
            $idrol = $pdo->lastInsertId();
            $newRolParams = Administrador_DB::getInfo(Administrador_Usuario_Rol::TABLE,'*',Array('id_rol = '.$idrol));
            if(empty($newRolParams[0])){
                throw new Exception("Rol already exist");
            }

            $newRol = new Administrador_Usuario_Rol($newRolParams[0]);
            $this->setAllParametersFromRol($newRol);
        }
        return $this;
    }

    public function delete()
    {
        return Administrador_DB::delete(Administrador_Usuario_Rol::TABLE,Array('id_rol = '.$this->getIdRol()));
    }


}





 ?>