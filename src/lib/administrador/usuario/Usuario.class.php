<?php
class Administrador_Usuario_Usuario implements Administrador_SerializableObject{
	const TABLE = "Usuario";
	private $id_usuario;
	private $id_rol;
	private $usuario;
	private $apellidos;
	private $email;
    private $password;
	private $cookie;
    private $activo;
	private $rol;
    /**
     * If true roles disabled
     * @var bool
     */
    private $withoutRoles;
    private $roles = array();

	public function __construct($parameters, $withoutRoles = false, $rol = null)
	{
		if(is_array($parameters))
        {
            $newParameters = new stdClass();
            foreach($parameters as $key => $parameter)
            {
                $newParameters->$key = $parameter;
            }
            $parameters = $newParameters;
        }
        if(!is_bool($withoutRoles))
        {
            throw new Exception('withoutRoles must be boolean');
        }

        if(isset($parameters->id_usuario))
        {
            $this->setIdUsuario($parameters->id_usuario);
        }else{
            throw new Exception('Missing parameter: id_usuario');
        }
        if(isset($parameters->id_rol))
        {
            $this->setIdRol($parameters->id_rol);
        }else{
            throw new Exception('Missing parameter: id_rol');
        }
        $this->setRol($rol);
        if(!empty($parameters->usuario))
        {
            $this->setUsuario($parameters->usuario);
        }else{
            throw new Exception('Missing parameter: usuario');
        }
        if(!empty($parameters->apellidos))
        {
            $this->setApellidos($parameters->apellidos);
        }else{
            throw new Exception('Missing parameter: apellidos');
        }
        if(!empty($parameters->email))
        {
            $this->setEmail($parameters->email);
        }else{
            throw new Exception('Missing parameter: email');
        }
        if(!empty($parameters->password))
        {
            $this->setPassword($parameters->password);
        }
        if(isset($parameters->cookie))
        {
            $this->setCookie($parameters->cookie);
        }else{
            $this->setCookie(null);
        }
        if(isset($parameters->activo))
        {
            $this->setActivo((!empty($parameters->activo))? 1 : 0);
        }else{
            $this->setActivo(0);
        }
        $this->setWithoutRoles($withoutRoles);
        if(!empty($parameters->roles))
        {
            $this->setRoles($parameters->roles);
        }else{
            if($this->getWithoutRoles() === false)
            {
                throw new Exception('Missing parameter: roles');
            }
        }
	}


	public function getIdUsuario()
    {
        return $this->id_usuario;
    }
	public function setIdUsuario($id_usuario)
    {
        return $this->id_usuario = $id_usuario;
    }
    public function getIdRol()
    {
        return $this->id_rol;
    }
    public function setIdRol($id_rol)
    {
        return $this->id_rol = $id_rol;
    }
    public function setRol($rol)
    {
        return $this->rol = $rol;
    }
    public function getRol()
    {
        return $this->rol;
    }
	public function getUsuario()
    {
        return $this->usuario;
    }
	public function setUsuario($usuario)
    {
        return $this->usuario = $usuario;
    }
    public function getApellidos()
    {
        return $this->apellidos;
    }
    public function setApellidos($apellidos)
    {
        return $this->apellidos = $apellidos;
    }
    public function getEmail()
    {
        return $this->email;
    }
    public function setEmail($email)
    {
        return $this->email = $email;
    }
    public function getPassword()
    {
        return $this->password;
    }
    public function setPassword($password)
    {
        return $this->password = $password;
    }
    public function getCookie()
    {
        return $this->cookie;
    }
	public function setCookie($cookie)
    {
        return $this->cookie = $cookie;
    }
    public function getActivo()
    {
        return $this->activo;
    }
    public function setActivo($activo)
    {
        return $this->activo = $activo;
    }
    public function getWithoutRoles()
    {
        return $this->withoutRoles;
    }
    public function setWithoutRoles($withoutRoles)
    {
        return $this->withoutRoles = $withoutRoles;
    }
    public function getRolesAsObject()
    {
        return $this->roles;
    }
    public function getRoles()
    {
        $rolesAsArray = array();
        foreach($this->roles as $rol)
        {
            $rolesAsArray[] = $rol->getObjectAsArray();
        }
        return $rolesAsArray;
    }
    public function setRoles($roles)
    {
        $this->roles = array();
        foreach ($roles as $rol) {
            if(is_array($rol))
            {
                $newParameters = new stdClass();
                foreach($rol as $key => $parameter)
                {
                    $newParameters->$key = $parameter;
                }
                $rol = $newParameters;
            }

            if(get_class($rol) === 'Administrador_Usuario_Rol')
            {
                $this->roles[$rol->getIdRol()] = $rol;
            }else{
                $this->roles[$rol->id_rol] = new Administrador_Usuario_Rol($rol);
            }
        }
        return $this->roles;
    }



    public function getObjectAsArray()
    {
    	$return = array();
    	$return['id_usuario'] = ($this->getIdUsuario() == -1)? null : $this->getIdUsuario();
    	$return['id_rol'] = $this->getIdRol();
    	$return['rol'] = $this->getRol();
    	$return['usuario'] = $this->getUsuario();
    	$return['apellidos'] = $this->getApellidos();
    	$return['email'] = $this->getEmail();
        $password = $this->getPassword();
        if(!empty($password))
        {
            $return['password'] = $password;
        }
    	$return['cookie'] = $this->getCookie();
    	$return['activo'] = $this->getActivo();
        $return['roles'] = $this->getRoles();
    	return $return;
    }

      public function getObjectAsArrayOnlyMainParameters()
    {
        $return = array();
        $return['id_usuario'] = ($this->getIdUsuario() == -1)? null : $this->getIdUsuario();
        $return['id_rol'] = $this->getIdRol();
        $return['usuario'] = $this->getUsuario();
        $return['apellidos'] = $this->getApellidos();
        $return['email'] = $this->getEmail();
        $password = $this->getPassword();
        if(!empty($password))
        {
            $return['password'] = $password;
        }
        $return['cookie'] = $this->getCookie();
        $return['activo'] = $this->getActivo();
        return $return;
    }

    private function setAllParametersFromUsuario(Administrador_Usuario_Usuario $newParameters)
    {
    	$this->setIdUsuario($newParameters->getIdUsuario());
    	$this->setIdRol($newParameters->getIdRol());
    	$this->setUsuario($newParameters->getUsuario());
    	$this->setApellidos($newParameters->getApellidos());
    	$this->setEmail($newParameters->getEmail());
    	$this->setPassword($newParameters->getPassword());
    	$this->setCookie($newParameters->getCookie());
    	$this->setActivo($newParameters->getActivo());
        $this->setRoles($newParameters->getRoles());
    }

    private function deleteRoles()
    {
        if($this->getIdUsuario() != -1)
        {
            return Administrador_DB::delete(Administrador_Usuario_UsuarioRol::TABLE, array('id_usuario = '.$this->getIdUsuario()));
        }
    }

    public function save()
    {
        if($this->getWithoutRoles() === true)
        {
            throw new Exception('To save User, withoutRoles must be false');
        }

        if ($this->getIdUsuario() != -1)
        {
            $pdo = Administrador_DB::update(Administrador_Usuario_Usuario::TABLE,$this->getObjectAsArrayOnlyMainParameters(),Array('id_usuario = '.$this->getIdUsuario()));

            $newUsuarioParams = Administrador_DB::getInfo(Administrador_Usuario_Usuario::TABLE,'*',Array('id_usuario = '.$this->getIdUsuario()));

            $this->deleteRoles();
            $errors = Administrador_Usuario_UsuarioRol::saveUsuarioRoles($this);
            foreach($errors as $error)
            {
                throw new Exception($error);
            }

            $newUsuarioRolesParams = Administrador_DB::getInfo(Administrador_Usuario_UsuarioRol::TABLE,'*',Array('id_usuario = '.$this->getIdUsuario()));
            $newUsuarioParams[0]->roles = array();
            foreach($newUsuarioRolesParams as $newUsuarioRolParams)
            {
                $newRolParams = Administrador_DB::getInfo(Administrador_Usuario_Rol::TABLE, '*', array('id_rol = '.$newUsuarioRolParams->id_rol));
                $newUsuarioParams[0]->roles[] = $newRolParams[0];
            }

            $newUsuario = new Administrador_Usuario_Usuario($newUsuarioParams[0]);
            $this->setAllParametersFromUsuario($newUsuario);
        }else{
            $pdo = Administrador_DB::insert(Administrador_Usuario_Usuario::TABLE,$this->getObjectAsArrayOnlyMainParameters());
            $idUsuario = $pdo->lastInsertId();
            $newUsuarioParams = Administrador_DB::getInfo(Administrador_Usuario_Usuario::TABLE,'*',Array('id_usuario = '.$idUsuario));
            if(empty($newUsuarioParams[0])){
                throw new Exception("User already exist");
            }
            $this->setIdUsuario($idUsuario);

            $errors = Administrador_Usuario_UsuarioRol::saveUsuarioRoles($this);
            foreach($errors as $error)
            {
                throw new Exception($error);
            }

            $newUsuarioRolesParams = Administrador_DB::getInfo(Administrador_Usuario_UsuarioRol::TABLE,'*',Array('id_usuario = '.$idUsuario));
            $newUsuarioParams[0]->roles = array();
            foreach($newUsuarioRolesParams as $newUsuarioRolParams)
            {
                $newRolParams = Administrador_DB::getInfo(Administrador_Usuario_Rol::TABLE, '*', array('id_rol = '.$newUsuarioRolParams->id_rol));
                $newUsuarioParams[0]->roles[] = $newRolParams[0];
            }

            $newUsuario = new Administrador_Usuario_Usuario($newUsuarioParams[0]);
            $this->setAllParametersFromUsuario($newUsuario);
        }

        return $this;
    }

    public function delete()
    {
        return Administrador_DB::delete(Administrador_Usuario_Usuario::TABLE,Array('id_usuario = '.$this->getIdUsuario()));
    }


}





 ?>