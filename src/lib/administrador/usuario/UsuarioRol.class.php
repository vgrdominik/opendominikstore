<?php
class Administrador_Usuario_UsuarioRol
{
    const TABLE = 'Usuario_Rol';

    private static function testExistUsuarioRol(Administrador_Usuario_Rol $rol, Administrador_Usuario_Usuario $usuario)
    {
        $test = Administrador_DB::getInfo(
                Administrador_Usuario_UsuarioRol::TABLE,
                '*',
                array('id_usuario = '.$usuario->getIdUsuario(),'id_rol = '.$rol->getIdRol()));
        return (empty($test[0]))? false : true;
    }

    private static function saveUsuarioRol(Administrador_Usuario_Rol $rol, Administrador_Usuario_Usuario $usuario)
    {
        $error = null;

        if(self::testExistUsuarioRol($rol, $usuario) === true)
        {
            $error = 'Found duplicated row';
        }else{
            $pdo = Administrador_DB::insert(Administrador_Usuario_UsuarioRol::TABLE,
                Array('id_usuario_rol' => null, 'id_usuario' => $usuario->getIdUsuario(), 'id_rol' => $rol->getIdRol()));
            $idUsuarioRol = $pdo->lastInsertId();
            $newUsuarioRolParams = Administrador_DB::getInfo(Administrador_Usuario_UsuarioRol::TABLE,'*',Array('id_usuario_rol = '.$idUsuarioRol));
            if(empty($newUsuarioRolParams[0])){
                throw new Exception("Error to insert");
            }
        }

        return $error;
    }

    public static function saveUsuarioRoles(Administrador_Usuario_Usuario $usuario)
    {
        $roles = $usuario->getRolesAsObject();
        $error = array();
        foreach ($roles as $rol) {
            $message = self::saveUsuarioRol($rol, $usuario);
            if($message !== null)
            {
                $error[] = $message;
            }
        }

        return $error;
    }
}





 ?>