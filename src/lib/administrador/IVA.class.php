<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 3/08/13
 * Time: 12:55
 * To change this template use File | Settings | File Templates.
 */

class Administrador_IVA implements Administrador_SerializableObject {
    const TABLE = 'IVA';
    private $id_iva;
    private $iva;

    public function __construct($parameters)
    {
        if(is_array($parameters))
        {
            $newParameters = new stdClass();
            foreach($parameters as $key => $parameter)
            {
                $newParameters->$key = $parameter;
            }
            $parameters = $newParameters;
        }
        if(isset($parameters->id_iva))
        {
            $this->setIdIva($parameters->id_iva);
        }else{
            throw new Exception('Missing parameter: id_iva');
        }

        if(isset($parameters->iva))
        {
            $this->setIva($parameters->iva);
        }else{
            throw new Exception('Missing parameter: iva');
        }
    }

    public function getIdIva()
    {
        return $this->id_iva;
    }

    public function setIdIva($id_iva)
    {
        return $this->id_iva = $id_iva;
    }

    public function getIva()
    {
        return $this->iva;
    }

    public function setIva($iva)
    {
        return $this->iva = $iva;
    }

    public function save()
    {
        if($this->getIdIva()!= -1)
        {
            $pdo = Administrador_DB::update(Administrador_IVA::TABLE, $this->getObjectAsArray(), array('id_iva = '.$this->getIdIva()));
            return $this;
        }else{
            $pdo = Administrador_DB::insert(Administrador_IVA::TABLE, $this->getObjectAsArray());
            $idIVA = $pdo->lastInsertId();
            $newIVAParams = Administrador_DB::getInfo(Administrador_IVA::TABLE, '*', array('id_iva = '.$idIVA));
            $newIVA = new Administrador_IVA($newIVAParams[0]);
            $this->setAllParamsFromIVA($newIVA);
            return $this;
        }

    }

    public function delete()
    {
        return Administrador_DB::delete(Administrador_IVA::TABLE, array('id_iva = '.$this->getIdIva()));
    }

    public function setAllParamsFromIVA(Administrador_IVA $newParams)
    {
        $this->setIdIva($newParams->getIdIva());
        $this->setIva($newParams->getIva());
    }

    public function getObjectAsArray()
    {
        $return = array();
        $return['id_iva'] = ($this->getIdIva() == -1)? null : $this->getIdIva();
        $return['iva'] = $this->getIva();

        return $return;
    }
}