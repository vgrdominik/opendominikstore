<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vgrdominik
 * Date: 3/08/13
 * Time: 12:55
 * To change this template use File | Settings | File Templates.
 */

class Administrador_Slide implements Administrador_SerializableObject {
    const TABLE = 'Slide';
    const TABLE_IMATGE = 'SlideImatge';
    private $imatges;
    private $id_indexpaginaprincipal;
    private $id_promocio;
    private $id_slide;

    public function __construct($parameters, $imatges)
    {
        if(is_array($parameters))
        {
            $newParameters = new stdClass();
            foreach($parameters as $key => $parameter)
            {
                $newParameters->$key = $parameter;
            }
            $parameters = $newParameters;
        }


        if(isset($parameters->id_slide))
        {
            $this->setIdSlide($parameters->id_slide);
            if(!empty($imatges))
            {
                $this->setImatges($imatges);
            }else{
                $this->setImatges(array());
            }

        }else{
            throw new Exception('Missing parameter: id_slide');
        }

        if(isset($parameters->id_indexpaginaprincipal))
        {
            $this->setIdIndexPaginaPrincipal($parameters->id_indexpaginaprincipal);
        }else{
            $this->setIdIndexPaginaPrincipal(null);
        }

        if(isset($parameters->id_promocio))
        {
            $this->setIdPromocio($parameters->id_promocio);
        }else{
            $this->setIdPromocio(null);
        }
    }

    public static function getImatgesFromDB($idSlide)
    {
        $imatges = Administrador_DB::getInfo(Administrador_Slide::TABLE_IMATGE, '*', array('id_slide = '.$idSlide));
        return $imatges;
    }

    public function getImatges()
    {
        foreach($this->imatges as $key => $imatge)
        {
            $this->imatges[$key]['descripcio'] = stripslashes($this->imatges[$key]['descripcio']);
        }
        return $this->imatges;
    }

    public function deleteImatge($num_slideimatge)
    {
        unset($this->imatges[$num_slideimatge]);
        return true;
    }

    public function setImatges($imatges)
    {
        $arrayImatges = array();
        $numImatge = 0;
        foreach($imatges as $imatge)
        {
            if(is_array($imatge))
            {
                $newParameters = new stdClass();
                foreach($imatge as $key => $parameter)
                {
                    $newParameters->$key = $parameter;
                }
                $imatge = $newParameters;
            }

            if(empty($imatge->descripcio))
            {
                throw new Exception('Missing descripcio');
            }

            if(empty($imatge->id_slide))
            {
                throw new Exception('Missing id_slide');
            }

            if(empty($imatge->link))
            {
                $imatge->link = null;
            }

            if(empty($imatge->url))
            {
                throw new Exception('Missing url');
            }

            if(!isset($imatge->num_slideimatge))
            {
                throw new Exception('Missing num_slideimatge');
            }

            if(empty($imatge->num_slideimatge) AND $imatge->num_slideimatge !== null)
            {
                throw new Exception('num_slideimatge can not be empty');
            }
            if($imatge->num_slideimatge == 'NULL')
            {
                $imatge->num_slideimatge = null;
                $numImatge++;
            }else{
                $numImatge = $imatge->num_slideimatge;
            }

            $arrayImatges[$numImatge]['descripcio'] = addslashes($imatge->descripcio);
            $arrayImatges[$numImatge]['url'] = $imatge->url;
            $arrayImatges[$numImatge]['link'] = $imatge->link;
            $arrayImatges[$numImatge]['num_slideimatge'] = $imatge->num_slideimatge;
            $arrayImatges[$numImatge]['id_slide'] = $imatge->id_slide;
        }
        return $this->imatges = $arrayImatges;
    }

    public function getIdSlide()
    {
        return $this->id_slide;
    }

    public function setIdSlide($id_slide)
    {
        return $this->id_slide = $id_slide;
    }

    public function getIdPromocio()
    {
        return $this->id_promocio;
    }

    public function setIdPromocio($id_promocio)
    {
        return $this->id_promocio = $id_promocio;
    }

    public function getIdIndexPaginaPrincipal()
    {
        return $this->id_indexpaginaprincipal;
    }

    public function setIdIndexPaginaPrincipal($id_indexpaginaprincipal)
    {
        return $this->id_indexpaginaprincipal = $id_indexpaginaprincipal;
    }

    public function refresh($idSlide = null)
    {
        $idSlide = ($idSlide !== null)? $idSlide : $this->getIdSlide();
        $paramsToSlide = Administrador_DB::getInfo(Administrador_Slide::TABLE, '*', array('id_slide = '.$idSlide));
        $imatges = Administrador_DB::getInfo(Administrador_Slide::TABLE_IMATGE, '*', array('id_slide = '.$idSlide));
        if(!isset($paramsToSlide[0]))
        {
            throw new Exception('Error to refresh slide');
        }
        $newSlide = new Administrador_Slide($paramsToSlide[0], $imatges);
        $this->setAllParamsFromDB($newSlide);
    }

    public function saveImatges($idSlide = null)
    {
        $imatges = $this->getObjectAsArrayOnlyImatges();
        $pdo = Administrador_DB::delete(Administrador_Slide::TABLE_IMATGE, array('id_slide = '.$this->getIdSlide()));
        foreach($imatges['imatges'] as $imatge)
        {
            if($imatge['id_slide'] == -1)
            {
                $imatge['id_slide'] = ($idSlide !== null)? $idSlide : $this->getIdSlide();
            }
            $imatge['descripcio'] = addslashes($imatge['descripcio']);
            $pdo = Administrador_DB::insert('SlideImatge', $imatge);
        }
    }

    public function save()
    {
        if($this->getIdSlide()!= -1)
        {
            $pdo = Administrador_DB::update(Administrador_Slide::TABLE, $this->getObjectAsArrayOnlySlide(), array('id_slide = '.$this->getIdSlide()));
            $this->saveImatges();
            $this->refresh();
            return $this;
        }else{
            $pdo = Administrador_DB::insert(Administrador_Slide::TABLE, $this->getObjectAsArrayOnlySlide());
            $idSlide = $pdo->lastInsertId();
            $this->saveImatges($idSlide);
            $this->refresh($idSlide);
            return $this;
        }

    }

    public function delete()
    {
        return Administrador_DB::delete(Administrador_Slide::TABLE, array('id_slide = '.$this->getIdSlide()));
    }

    public function setAllParamsFromDB(Administrador_Slide $newParams)
    {
        $this->setImatges($newParams->getImatges());
        $this->setIdSlide($newParams->getIdSlide());
        $this->setIdIndexPaginaPrincipal($newParams->getIdIndexPaginaPrincipal());
        $this->setIdPromocio($newParams->getIdPromocio());
    }

    public function getObjectAsArray()
    {
        $return = array();
        $return['imatges'] = $this->getImatges();
        $return['id_slide'] = ($this->getIdSlide() == -1)? null : $this->getIdSlide();
        $return['id_indexpaginaprincipal'] = $this->getIdIndexPaginaPrincipal();
        $return['id_promocio'] = $this->getIdPromocio();

        return $return;
    }

    public function getObjectAsArrayOnlySlide()
    {
        $return = array();
        $return['id_slide'] = ($this->getIdSlide() == -1)? null : $this->getIdSlide();
        $return['id_indexpaginaprincipal'] = $this->getIdIndexPaginaPrincipal();
        $return['id_promocio'] = $this->getIdPromocio();

        return $return;
    }

    public function getObjectAsArrayOnlyImatges()
    {
        $return = array();
        $return['imatges'] = $this->getImatges();

        return $return;
    }
}