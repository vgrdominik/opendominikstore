<?php
/**
 * User: vgrdominik
 * Date: 22/09/12
 * Time: 10:55
 */

class Administrador_DB
{
    public $pdo;

    function __construct($dsn, $user, $password) {
        $this->pdo = new PDO($dsn, $user, $password);
        $this->pdo->exec('SET names UTF8');
    }

    private static function get( $db = Administrador_Core::GENERAL_DB )
    {
        $dsn = 'mysql:dbname='.$db.';host=localhost';
        $user = 'admin';
        $password = 'test';
        return new Administrador_DB($dsn, $user, $password);
    }

    public static function exec ($q, $db = Administrador_Core::GENERAL_DB) {
        $pdo = self::get($db)->pdo;
        $pdo->query( $q );
        return $pdo;
    }

    public static function query ( $q, $useIdAsKey = false, $db = Administrador_Core::GENERAL_DB ) {
        $list = array();

        $result = self::get($db)->pdo->query($q);

        if ( !empty($result) )
        {
            while ( $row = $result->fetchObject() ) {
                /*
                 * El parametre $useIdAsKey no funciona ja que no totes les taules tenen un id (la majoria es diuen id_nom_taula)
                if ( $useIdAsKey == false ) {
                    $list[] = $row;
                } else {
                    $list[$row->id] = $row;
                }
                */
                $list[] = $row;
            }
        }

        return $list;
    }

    public static function getInfo ( $table, $fields = '*', array $conditions = null, $limit_start = null, $limit_rows = null, array $order = null, $db = Administrador_Core::GENERAL_DB )
    {
        if ( $fields != '*' ) {
            //$fields_query = join('`, `', $fields);
            //$fields_query = sprintf('`%s`', $fields_query);
            $fields_query = join(', ', $fields);
            $fields_query = sprintf('%s', $fields_query);
        } else {
            $fields_query = $fields;
        }

        if ( !empty($conditions) ) {
            $conditions_query = 'WHERE '.join(' AND ', $conditions);
        } else {
            $conditions_query = null;
        }

        if ( $limit_start !== null )
        {
            if ( !empty($limit_rows) ) {
                $limit_query = sprintf('LIMIT %s, %s', $limit_start, $limit_rows);
            } else {
                $limit_query = sprintf('LIMIT %s', $limit_start);
            }
        } else {
            $limit_query = null;
        }

        if ( !empty($order) ) {
            $order_query = 'ORDER BY ';
            foreach ( $order as $field => $value ) {
                $order_query .= sprintf('%s %s,', $field, $value);
            }
            $order_query = trim($order_query, ',');
        } else {
            $order_query = null;
        }

        $q = sprintf('SELECT %s FROM %s %s %s %s', $fields_query, $table, $conditions_query, $order_query, $limit_query);
//debug($q);
        return self::query( $q, false, $db);
    }

    static function update( $table, array $fields, array $conditions = null, $limit = null, $db = Administrador_Core::GENERAL_DB )
    {
        $str = array();
        foreach($fields as $key => $value) {
            if($value === null)
            {
                $str[] = "`$key` = NULL";
            }else{
                if($value == 'NULL')
                {
                    $str[] = "`$key` = NULL";
                }else{
                    $str[] = "`$key` = '$value'";
                }
            }
        }
        $fields_query = join(', ', $str);

        if ( !empty($conditions) ) {
            $conditions_query = join(' AND ', $conditions);
            $conditions_query = 'WHERE '.$conditions_query;
        } else {
            $conditions_query = null;
        }

        if ( !empty($limit) ) {
            $limit_query = ' LIMIT '.$limit;
        } else {
            $limit_query = null;
        }

        $sql = sprintf("UPDATE `%s` SET %s %s %s;", $table, $fields_query, $conditions_query, $limit_query);

        return Administrador_DB::exec( $sql, $db );
    }

    static function delete( $table, array $conditions = null, $limit = null, $db = Administrador_Core::GENERAL_DB )
    {

        if ( !empty($conditions) ) {
            $conditions_query = join(' AND ', $conditions);
            $conditions_query = 'WHERE '.$conditions_query;
        } else {
            $conditions_query = null;
        }

        if ( !empty($limit) ) {
            $limit_query = ' LIMIT '.$limit;
        } else {
            $limit_query = null;
        }

        $sql = sprintf("DELETE FROM `%s` %s %s;", $table, $conditions_query, $limit_query);

        return Administrador_DB::exec( $sql, $db );
    }

    static function insert( $table, array $fields, $db = Administrador_Core::GENERAL_DB )
    {
        $str = array();
        foreach($fields as $key => $value) {
            if($value === null)
            {
                if($key == 'id_pagina')
                {
                    $str[] = "`$key` = 0";
                }else{
                    $str[] = "`$key` = NULL";
                }
            }else{
                $str[] = "`$key` = '$value'";
            }
        }
        $fields_query = join(', ', $str);

        $sql = sprintf("INSERT INTO  `%s` SET %s;", $table, $fields_query);
//print($sql);
        return Administrador_DB::exec( $sql, $db );
    }
}
